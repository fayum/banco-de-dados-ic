/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package views;

import model.bean.Coleta;
import common.ConversorData;
import common.GraficoXY;
import common.TableCellListener;
import common.ponto;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import model.bean.Espectro;
import model.dao.AlteracaoDAO;
import model.dao.ColetaDAO;
import model.dao.EspectroDAO;
import org.jfree.chart.ChartPanel;
import javafx.util.Pair;

/**
 *
 * @author Valdete
 */
public class BuscaIndivCol extends javax.swing.JFrame {

    private Coleta coletaOriginal;
    private List<String> alterados; //campos que foram alterados
    private List<ponto> curvaExc;
    private List<ponto> curvaAdd;

    public List<ponto> getCurvaExc() {
        return curvaExc;
    }

    public void setCurvaExc(List<ponto> curvaExc) {
        this.curvaExc = curvaExc;
    }

    public List<ponto> getCurvaAdd() {
        return curvaAdd;
    }

    public void setCurvaAdd(List<ponto> curvaAdd) {
        this.curvaAdd = curvaAdd;
    }

    public Coleta getColetaOriginal() {
        return coletaOriginal;
    }

    public void setColetaOriginal(Coleta coletaOriginal) {
        this.coletaOriginal = coletaOriginal;
    }

    public List<String> getAlterados() {
        return alterados;
    }

    public void setAlterados(List<String> alterados) {
        this.alterados = alterados;
    }

    public void marcarAlterado(String campo, JTextField txt, List<String> alteracoes)
    {
        if(alteracoes.contains(campo))
        {
            txt.setBorder(BorderFactory.createLineBorder(Color.RED));
            txt.setToolTipText("Valor Original: ");
        }
    }
    
    public void carregaGrafico()
    {
        GraficoXY g = new GraficoXY();
        jPainelGrafico.removeAll();
        List<ponto> data = getTableData(tabEspec);
        jPainelGrafico.add(g.criar(data));
        jPainelGrafico.revalidate();
        jPainelGrafico.repaint();
    }
    
    public void carregaColeta(Coleta coleta)
    {
        ConversorData dtconv = new ConversorData();
                
        //Sinaliza alterações
        if(alterados.contains("DataHora_col"))
        {
            txtDt.setBorder(BorderFactory.createLineBorder(Color.RED));
            txtHr.setBorder(BorderFactory.createLineBorder(Color.RED));
        }
        
        if(alterados.contains("Validacao"))
            chkBValidacao.setBorder(BorderFactory.createLineBorder(Color.RED));
        
        marcarAlterado("IrradGlobHorizontal", txtIrradGlobHorizontal, alterados);
        marcarAlterado("DesvGlobHorizontal", txtDesvGlobHorizontal, alterados);
        marcarAlterado("IrradDirNormal", txtIrradDirNormal, alterados);
        marcarAlterado("DesvDirNormal", txtDesvDirNormal, alterados);
        marcarAlterado("RadiacaoDifusa", txtRadDif, alterados);
        marcarAlterado("DesvDifusa", txtDesvDifusa, alterados);
        
        marcarAlterado("TempAr", txtTempAr, alterados);
        marcarAlterado("UmidAr", txtUmidAr, alterados);        
        marcarAlterado("PressAr", txtPressAr, alterados);
        
        marcarAlterado("CorrCC_aSi", txtCCaSi, alterados);
        marcarAlterado("CorrCC_micSi", txtCCmicSi, alterados);
        marcarAlterado("CorrCC_pSi", txtCCpSi, alterados);
        marcarAlterado("CorrCC_moSi", txtCCmoSi, alterados);
        marcarAlterado("CorrCC_GaAs", txtCCGaAs, alterados);
        marcarAlterado("CorrCC_TJ", txtCCTJ, alterados);
        
        marcarAlterado("DesvCorrCC_aSi", txtDesvCCaSi, alterados);
        marcarAlterado("DesvCorrCC_micSi", txtDesvCCmicSi, alterados);
        marcarAlterado("DesvCorrCC_pSi", txtDesvCCpSi, alterados);
        marcarAlterado("DesvCorrCC_moSi", txtDesvCCmoSi, alterados);
        marcarAlterado("DesvCorrCC_GaAs", txtDesvCCGaAs, alterados);
        marcarAlterado("DesvCorrCC_TJ", txtDesvCCTJ, alterados);
        
        marcarAlterado("TempMod_aSi", txtTempaSi, alterados);
        marcarAlterado("TempMod_micSi", txtTempmicSi, alterados);
        marcarAlterado("TempMod_pSi", txtTemppSi, alterados);
        marcarAlterado("TempMod_moSi", txtTempmoSi, alterados);
        marcarAlterado("TempMod_GaAs", txtTempGaAs, alterados);
        marcarAlterado("TempMod_TJ", txtTempTJ, alterados);
        
        marcarAlterado("DesvTempMod_aSi", txtDesvTempaSi, alterados);
        marcarAlterado("DesvTempMod_micSi", txtDesvTempmicSi, alterados);
        marcarAlterado("DesvTempMod_pSi", txtDesvTemppSi, alterados);
        marcarAlterado("DesvTempMod_moSi", txtDesvTempmoSi, alterados);
        marcarAlterado("DesvTempMod_GaAs", txtDesvTempGaAs, alterados);
        marcarAlterado("DesvTempMod_TJ", txtDesvTempTJ, alterados);
        
        marcarAlterado("IrrPiranômetro", txtIrradPir, alterados);
        marcarAlterado("DesvIrrPiranômetro", txtDesvIrradPir, alterados);
        marcarAlterado("IrrFotodiodo", txtIrradFoto, alterados);
        marcarAlterado("DesvIrrFotodiodo", txtDesvIrradFoto, alterados);
        
        //Carrega valores        
        txtDt.setText(dtconv.FormatarTimeStampEmDataConvencional(coleta.getDataHora_col()));
        txtHr.setText(dtconv.FormatarTimeStampEmHora(coleta.getDataHora_col()));
        txtCidade.setText(coleta.getCidade());
        //lblKT.setText(coleta.getIrradGlobHorizontal()/coleta.getIrradDirNormal());
        
        txtIrradGlobHorizontal.setText(Double.toString(coleta.getIrradGlobHorizontal()));
        txtDesvGlobHorizontal.setText(Double.toString(coleta.getDesvGlobHorizontal()));
        txtIrradDirNormal.setText(Double.toString(coleta.getIrradDirNormal()));
        txtDesvDirNormal.setText(Double.toString(coleta.getDesvDirNormal()));
        txtRadDif.setText(Double.toString(coleta.getRadiacaoDifusa()));
        txtDesvDifusa.setText(Double.toString(coleta.getDesvDifusa()));
        
        txtUmidAr.setText(Double.toString(coleta.getUmidAr()));
        txtTempAr.setText(Double.toString(coleta.getTempAr()));
        txtPressAr.setText(Double.toString(coleta.getPressAr()));
        
        txtCCGaAs.setText(Double.toString(coleta.getCorrCC_GaAs()));
        txtCCTJ.setText(Double.toString(coleta.getCorrCC_TJ()));
        txtCCaSi.setText(Double.toString(coleta.getCorrCC_aSi()));
        txtCCmicSi.setText(Double.toString(coleta.getCorrCC_micSi()));
        txtCCmoSi.setText(Double.toString(coleta.getCorrCC_moSi()));
        txtCCpSi.setText(Double.toString(coleta.getCorrCC_pSi()));
        
        txtDesvCCGaAs.setText(Double.toString(coleta.getDesvCorrCC_GaAs()));
        txtDesvCCTJ.setText(Double.toString(coleta.getDesvCorrCC_TJ()));
        txtDesvCCaSi.setText(Double.toString(coleta.getDesvCorrCC_aSi()));
        txtDesvCCmicSi.setText(Double.toString(coleta.getDesvCorrCC_micSi()));
        txtDesvCCmoSi.setText(Double.toString(coleta.getDesvCorrCC_moSi()));
        txtDesvCCpSi.setText(Double.toString(coleta.getDesvCorrCC_pSi()));
        
        txtTempGaAs.setText(Double.toString(coleta.getTempMod_GaAs()));
        txtTempTJ.setText(Double.toString(coleta.getTempMod_TJ()));
        txtTempaSi.setText(Double.toString(coleta.getTempMod_aSi()));
        txtTempmicSi.setText(Double.toString(coleta.getTempMod_micSi()));
        txtTempmoSi.setText(Double.toString(coleta.getTempMod_moSi()));
        txtTemppSi.setText(Double.toString(coleta.getTempMod_pSi()));
        
        txtDesvTempGaAs.setText(Double.toString(coleta.getDesvTempMod_GaAs()));
        txtDesvTempTJ.setText(Double.toString(coleta.getDesvTempMod_TJ()));
        txtDesvTempaSi.setText(Double.toString(coleta.getDesvTempMod_aSi()));
        txtDesvTempmicSi.setText(Double.toString(coleta.getDesvTempMod_micSi()));
        txtDesvTempmoSi.setText(Double.toString(coleta.getDesvTempMod_moSi()));
        txtDesvTemppSi.setText(Double.toString(coleta.getDesvTempMod_pSi()));
        
        txtIrradPir.setText(Double.toString(coleta.getIrrPiranômetro()));
        txtDesvIrradPir.setText(Double.toString(coleta.getDesvIrrPiranômetro()));
        txtIrradFoto.setText(Double.toString(coleta.getIrrFotodiodo()));
        txtDesvIrradFoto.setText(Double.toString(coleta.getDesvIrrFotodiodo()));
        
        System.out.println("Tá válidade?" + coleta.isValidacao());
        chkBValidacao.setSelected(coleta.isValidacao());
    }
    
    public List<ponto> getTableData (JTable table) {
        DefaultTableModel dtm = (DefaultTableModel) table.getModel();
        int nRow = dtm.getRowCount(), nCol = dtm.getColumnCount();
        List<ponto> tableData = new ArrayList<ponto>();
        for (int i = 0 ; i < nRow ; i++)
        {
            if((dtm.getValueAt(i,0) instanceof Double)&&(dtm.getValueAt(i,1) instanceof Double))
            tableData.add(new ponto((Double) dtm.getValueAt(i,0), (Double) dtm.getValueAt(i,1)));
        }
        return tableData;
    }
    
    public static boolean pontoExistente(List<ponto> lista, Double value)
    {
        for(ponto p:lista)
            if(p.getX()==value)
                return true;
        return false;
    }
    
    public static List<ponto> retiraPonto(List<ponto> lista, ponto parametro)
    {
        ponto pan;
        for (Iterator<ponto> p = lista.iterator(); p.hasNext();) {
          pan = p.next();
          if (pan.getX() == parametro.getX()) {
            p.remove();
          }
        }
        return lista;
    }
        
    /** Creates new form CadIndivEspec */
    @SuppressWarnings("empty-statement")
    public BuscaIndivCol(Coleta coleta) {
        
        this.setColetaOriginal(coleta);
        ConversorData dtconv = new ConversorData();
        
        initComponents();
        setTitle("Banco de dados - "+ dtconv.FormatarTimeStampEmDataConvencional(coleta.getDataHora_col())+" "+dtconv.FormatarTimeStampEmHora(coleta.getDataHora_col())+" - "+ coleta.getCidade());
        setLocationRelativeTo(null);
        
        //Busca quais são os campos alterados
        AlteracaoDAO dao = new AlteracaoDAO();
        setAlterados(dao.buscaAlteracoes(coleta.getDataHora_col()));
        
        //Busca Espectro
        EspectroDAO daoEspec = new EspectroDAO();
        Espectro espectro = daoEspec.select(coleta.getDataHora_col());
        System.out.println("Espectro é null? "+(espectro==null));
        
        //Tabela Espectro
        if(espectro==null)
            espectro = new Espectro();
        loadTableEspectro(espectro.getCurva());
        System.out.println("Carregou Tabela");
        
        //Grafico
        GraficoXY g = new GraficoXY();
        ChartPanel chartP = g.criar(espectro.getCurva());
        System.out.println("Criou gráfico");
        if(chartP != null)
        {
            this.jPainelGrafico.setLayout(new BorderLayout());
            this.jPainelGrafico.add(chartP);
        }
        System.out.println("Antes do pack");
        pack();
        System.out.println("Construiu gráfico");
        
        this.curvaExc = new ArrayList<ponto>();
        this.curvaAdd = new ArrayList<ponto>();
        Action action = new AbstractAction()
        {
            @Override
            public void actionPerformed(ActionEvent e){
                TableCellListener tcl = (TableCellListener)e.getSource();
                System.out.println("Alterou");
                ponto newP, oldP;
                if(tcl.getColumn()==0)
                {
                    oldP = new ponto((Double)tcl.getOldValue(), (Double) tabEspec.getModel().getValueAt(tcl.getRow(), 1));
                    newP = new ponto((Double)tcl.getNewValue(), (Double) tabEspec.getModel().getValueAt(tcl.getRow(), 1));
                }
                else
                {
                    oldP = new ponto((Double) tabEspec.getModel().getValueAt(tcl.getRow(),0),(Double)tcl.getOldValue());
                    newP = new ponto((Double) tabEspec.getModel().getValueAt(tcl.getRow(),0),(Double)tcl.getNewValue());
                }
                                
                retiraPonto(curvaAdd, oldP);
                curvaExc.add(oldP);
                curvaAdd.add(newP);
                tabEspec.setRowSorter(new TableRowSorter(tabEspec.getModel()));    
                                
                carregaGrafico();
            }
                        
        };
        TableCellListener tcl = new TableCellListener(tabEspec, action);
                
        //Carrega coleta        
        System.out.println("Antes de carregar a coleta");
        carregaColeta(coleta);
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        BuscaIndivCol = new javax.swing.JPanel();
        txtHr = new javax.swing.JFormattedTextField();
        txtIrradGlobHorizontal = new javax.swing.JTextField();
        txtDesvGlobHorizontal = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtDesvDirNormal = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtIrradDirNormal = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtRadDif = new javax.swing.JTextField();
        txtTempAr = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtUmidAr = new javax.swing.JTextField();
        txtPressAr = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtCCaSi = new javax.swing.JTextField();
        txtDesvCCaSi = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txtDt = new javax.swing.JFormattedTextField();
        jLabel13 = new javax.swing.JLabel();
        txtCCmicSi = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        txtDesvCCmicSi = new javax.swing.JTextField();
        txtCCpSi = new javax.swing.JTextField();
        txtDesvCCpSi = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        txtCCmoSi = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        txtDesvCCmoSi = new javax.swing.JTextField();
        txtDesvCCGaAs = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        txtCCGaAs = new javax.swing.JTextField();
        txtCCTJ = new javax.swing.JTextField();
        txtDesvCCTJ = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        txtTempaSi = new javax.swing.JTextField();
        txtTempmicSi = new javax.swing.JTextField();
        txtTemppSi = new javax.swing.JTextField();
        txtTempmoSi = new javax.swing.JTextField();
        txtTempGaAs = new javax.swing.JTextField();
        txtTempTJ = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        jLabel37 = new javax.swing.JLabel();
        txtDesvTempaSi = new javax.swing.JTextField();
        txtDesvTempmicSi = new javax.swing.JTextField();
        txtDesvTemppSi = new javax.swing.JTextField();
        txtDesvTempmoSi = new javax.swing.JTextField();
        txtDesvTempGaAs = new javax.swing.JTextField();
        txtDesvTempTJ = new javax.swing.JTextField();
        txtIrradPir = new javax.swing.JTextField();
        jLabel38 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        txtDesvIrradPir = new javax.swing.JTextField();
        txtDesvIrradFoto = new javax.swing.JTextField();
        jLabel40 = new javax.swing.JLabel();
        txtIrradFoto = new javax.swing.JTextField();
        jLabel41 = new javax.swing.JLabel();
        chkBValidacao = new javax.swing.JCheckBox();
        btnSalvarAlteracoes = new javax.swing.JButton();
        txtDesvDifusa = new javax.swing.JTextField();
        jLabel42 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        jPainelGrafico = new javax.swing.JPanel();
        txtCidade = new javax.swing.JTextField();
        jLabel44 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tabEspec = new javax.swing.JTable();
        txtAddCompOnda = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        txtAddIrrad = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        btnExcluir = new javax.swing.JButton();
        btnSalvarEspec = new javax.swing.JButton();
        btnProcurarEspec = new javax.swing.JButton();
        txtEspectro = new javax.swing.JTextField();
        jLabel46 = new javax.swing.JLabel();
        lblKT = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(51, 51, 51));

        BuscaIndivCol.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        try {
            txtHr.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##:##:##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtHr.setText("00:00:00");
        txtHr.setToolTipText("");
        BuscaIndivCol.add(txtHr, new org.netbeans.lib.awtextra.AbsoluteConstraints(543, 129, 100, -1));
        BuscaIndivCol.add(txtIrradGlobHorizontal, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 167, 100, -1));
        BuscaIndivCol.add(txtDesvGlobHorizontal, new org.netbeans.lib.awtextra.AbsoluteConstraints(543, 167, 100, -1));

        jLabel3.setText("Data:");
        BuscaIndivCol.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(125, 132, -1, -1));

        jLabel4.setText("Hora:");
        BuscaIndivCol.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(498, 132, -1, -1));

        jLabel1.setText("Irradiância Global Horizontal:");
        BuscaIndivCol.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(13, 170, -1, -1));

        jLabel5.setText("Desvio Padrão Irradiância Global Horizontal:");
        BuscaIndivCol.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 170, -1, -1));
        BuscaIndivCol.add(txtDesvDirNormal, new org.netbeans.lib.awtextra.AbsoluteConstraints(543, 193, 100, -1));

        jLabel6.setText("Desvio Padrão Irradiância Direta:");
        BuscaIndivCol.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 196, -1, -1));
        BuscaIndivCol.add(txtIrradDirNormal, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 193, 100, -1));

        jLabel7.setText("Irradiância Direta:");
        BuscaIndivCol.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(63, 196, -1, -1));
        BuscaIndivCol.add(txtRadDif, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 219, 100, -1));
        BuscaIndivCol.add(txtTempAr, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 270, 100, -1));

        jLabel8.setText("Radiação Difusa:");
        BuscaIndivCol.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 222, -1, -1));

        jLabel9.setText("Temperatura Ar:");
        BuscaIndivCol.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 270, -1, -1));
        BuscaIndivCol.add(txtUmidAr, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 245, 100, -1));
        BuscaIndivCol.add(txtPressAr, new org.netbeans.lib.awtextra.AbsoluteConstraints(543, 245, 100, -1));

        jLabel10.setText("Umidade Ar:");
        BuscaIndivCol.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(92, 248, -1, -1));

        jLabel11.setText("Pressão Ar:");
        BuscaIndivCol.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 248, -1, -1));
        BuscaIndivCol.add(txtCCaSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 360, 100, -1));
        BuscaIndivCol.add(txtDesvCCaSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 360, 100, -1));

        jLabel12.setText("aSi:");
        BuscaIndivCol.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 363, -1, -1));

        try {
            txtDt.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtDt.setText("00/00/0000");
        BuscaIndivCol.add(txtDt, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 129, 100, -1));

        jLabel13.setText("Desvio Padrão aSi:");
        BuscaIndivCol.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 363, -1, -1));
        BuscaIndivCol.add(txtCCmicSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 385, 100, -1));

        jLabel14.setText("micSi:");
        BuscaIndivCol.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 388, -1, -1));

        jLabel15.setText("Desvio Padrão micSi:");
        BuscaIndivCol.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 388, -1, -1));
        BuscaIndivCol.add(txtDesvCCmicSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 385, 100, -1));
        BuscaIndivCol.add(txtCCpSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 410, 100, -1));
        BuscaIndivCol.add(txtDesvCCpSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 410, 100, -1));

        jLabel16.setText("pSi");
        BuscaIndivCol.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 413, -1, -1));

        jLabel17.setText("Desvio Padrão pSi:");
        BuscaIndivCol.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 413, -1, -1));

        jLabel18.setText("Desvio Padrão moSi:");
        BuscaIndivCol.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 438, -1, -1));
        BuscaIndivCol.add(txtCCmoSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 435, 100, -1));

        jLabel19.setText("moSi:");
        BuscaIndivCol.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 438, -1, -1));
        BuscaIndivCol.add(txtDesvCCmoSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 435, 100, -1));
        BuscaIndivCol.add(txtDesvCCGaAs, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 460, 100, -1));

        jLabel20.setText("GaAs:");
        BuscaIndivCol.add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 463, -1, -1));

        jLabel21.setText("Desvio Padrão GaAsi:");
        BuscaIndivCol.add(jLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 463, -1, -1));
        BuscaIndivCol.add(txtCCGaAs, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 460, 100, -1));
        BuscaIndivCol.add(txtCCTJ, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 485, 100, -1));
        BuscaIndivCol.add(txtDesvCCTJ, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 485, 100, -1));

        jLabel22.setText("TJ:");
        BuscaIndivCol.add(jLabel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 488, -1, -1));

        jLabel23.setText("Desvio Padrão TJi:");
        BuscaIndivCol.add(jLabel23, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 488, -1, -1));

        jLabel24.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel24.setText("Coleta");
        BuscaIndivCol.add(jLabel24, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 40, -1, -1));

        jLabel25.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel25.setText("Temperatura Módulo Solar");
        BuscaIndivCol.add(jLabel25, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 540, -1, -1));

        jLabel26.setText("aSi:");
        BuscaIndivCol.add(jLabel26, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 573, -1, -1));

        jLabel27.setText("micSi:");
        BuscaIndivCol.add(jLabel27, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 598, -1, -1));

        jLabel28.setText("pSi");
        BuscaIndivCol.add(jLabel28, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 623, -1, -1));

        jLabel29.setText("moSi:");
        BuscaIndivCol.add(jLabel29, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 648, -1, -1));

        jLabel30.setText("GaAs:");
        BuscaIndivCol.add(jLabel30, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 673, -1, -1));

        jLabel31.setText("TJ:");
        BuscaIndivCol.add(jLabel31, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 698, -1, -1));
        BuscaIndivCol.add(txtTempaSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 570, 100, -1));
        BuscaIndivCol.add(txtTempmicSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 595, 100, -1));
        BuscaIndivCol.add(txtTemppSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 620, 100, -1));
        BuscaIndivCol.add(txtTempmoSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 645, 100, -1));
        BuscaIndivCol.add(txtTempGaAs, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 670, 100, -1));
        BuscaIndivCol.add(txtTempTJ, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 695, 100, -1));

        jLabel32.setText("Desvio Padrão aSi:");
        BuscaIndivCol.add(jLabel32, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 573, -1, -1));

        jLabel33.setText("Desvio Padrão micSi:");
        BuscaIndivCol.add(jLabel33, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 598, -1, -1));

        jLabel34.setText("Desvio Padrão moSi:");
        BuscaIndivCol.add(jLabel34, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 648, -1, -1));

        jLabel35.setText("Desvio Padrão pSi:");
        BuscaIndivCol.add(jLabel35, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 623, -1, -1));

        jLabel36.setText("Desvio Padrão GaAsi:");
        BuscaIndivCol.add(jLabel36, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 673, -1, -1));

        jLabel37.setText("Desvio Padrão TJi:");
        BuscaIndivCol.add(jLabel37, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 698, -1, -1));
        BuscaIndivCol.add(txtDesvTempaSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 570, 100, -1));
        BuscaIndivCol.add(txtDesvTempmicSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 595, 100, -1));
        BuscaIndivCol.add(txtDesvTemppSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 620, 100, -1));
        BuscaIndivCol.add(txtDesvTempmoSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 645, 100, -1));
        BuscaIndivCol.add(txtDesvTempGaAs, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 670, 100, -1));
        BuscaIndivCol.add(txtDesvTempTJ, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 695, 100, -1));
        BuscaIndivCol.add(txtIrradPir, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 770, 100, -1));

        jLabel38.setText("Irradiância Piranômetro:");
        BuscaIndivCol.add(jLabel38, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 780, -1, -1));

        jLabel39.setText("Desvio Padrão Irradiância Piranômetro:");
        BuscaIndivCol.add(jLabel39, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 780, -1, -1));
        BuscaIndivCol.add(txtDesvIrradPir, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 770, 100, -1));
        BuscaIndivCol.add(txtDesvIrradFoto, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 800, 100, -1));

        jLabel40.setText("Desvio Padrão Irradiância Fotodiodo:");
        BuscaIndivCol.add(jLabel40, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 800, -1, -1));
        BuscaIndivCol.add(txtIrradFoto, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 800, 100, -1));

        jLabel41.setText("Irradiância Fotodiodo:");
        BuscaIndivCol.add(jLabel41, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 800, -1, -1));

        chkBValidacao.setText("Validação");
        BuscaIndivCol.add(chkBValidacao, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 830, -1, -1));

        btnSalvarAlteracoes.setText("Salvar Alterações");
        btnSalvarAlteracoes.setToolTipText("");
        btnSalvarAlteracoes.setActionCommand("");
        btnSalvarAlteracoes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarAlteracoesActionPerformed(evt);
            }
        });
        BuscaIndivCol.add(btnSalvarAlteracoes, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 840, -1, -1));
        BuscaIndivCol.add(txtDesvDifusa, new org.netbeans.lib.awtextra.AbsoluteConstraints(543, 220, 100, -1));

        jLabel42.setText("Desvio Padrão Radiação Difusa:");
        BuscaIndivCol.add(jLabel42, new org.netbeans.lib.awtextra.AbsoluteConstraints(363, 223, -1, -1));

        jLabel43.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel43.setText("Corrente de Curto Circuito");
        BuscaIndivCol.add(jLabel43, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 330, -1, -1));

        javax.swing.GroupLayout jPainelGraficoLayout = new javax.swing.GroupLayout(jPainelGrafico);
        jPainelGrafico.setLayout(jPainelGraficoLayout);
        jPainelGraficoLayout.setHorizontalGroup(
            jPainelGraficoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 530, Short.MAX_VALUE)
        );
        jPainelGraficoLayout.setVerticalGroup(
            jPainelGraficoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 280, Short.MAX_VALUE)
        );

        BuscaIndivCol.add(jPainelGrafico, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 930, 530, 280));
        BuscaIndivCol.add(txtCidade, new org.netbeans.lib.awtextra.AbsoluteConstraints(543, 270, 100, -1));

        jLabel44.setText("Cidade:");
        BuscaIndivCol.add(jLabel44, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 270, -1, -1));

        tabEspec.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Comprimento de Onda (nm)", "Irradiância (microW/cm² nm)"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Double.class, java.lang.Double.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane3.setViewportView(tabEspec);

        BuscaIndivCol.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 1260, -1, 316));
        BuscaIndivCol.add(txtAddCompOnda, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 1590, 80, -1));

        jLabel2.setText("Comprimento de Onda:");
        BuscaIndivCol.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 1590, -1, -1));

        jLabel45.setText("Irradiância:");
        BuscaIndivCol.add(jLabel45, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 1590, -1, -1));
        BuscaIndivCol.add(txtAddIrrad, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 1590, 90, -1));

        jButton1.setText("+");
        jButton1.setActionCommand("btnAddInterpolation");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        BuscaIndivCol.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 1590, -1, -1));
        BuscaIndivCol.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 880, 690, -1));

        btnExcluir.setText("-");
        btnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirActionPerformed(evt);
            }
        });
        BuscaIndivCol.add(btnExcluir, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 1590, -1, -1));

        btnSalvarEspec.setText("Salvar");
        btnSalvarEspec.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarEspecActionPerformed(evt);
            }
        });
        BuscaIndivCol.add(btnSalvarEspec, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 1630, -1, -1));

        btnProcurarEspec.setText("Procurar");
        btnProcurarEspec.setActionCommand("btn");
        btnProcurarEspec.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProcurarEspecActionPerformed(evt);
            }
        });
        BuscaIndivCol.add(btnProcurarEspec, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 1630, -1, -1));
        BuscaIndivCol.add(txtEspectro, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 1630, 240, -1));

        jLabel46.setText("Índice de limpidez:");
        BuscaIndivCol.add(jLabel46, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 300, -1, -1));
        BuscaIndivCol.add(lblKT, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 300, 20, 20));

        jScrollPane1.setViewportView(BuscaIndivCol);
        BuscaIndivCol.getAccessibleContext().setAccessibleDescription("BuscaIndivCol");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 807, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 530, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public Double ehCampoNulo(String valor)
    {
        return ((valor.equals("") == false)?(Double.parseDouble(valor.replace(',', '.'))) : (0));
    }
    
    public void preencheCampo(String campo, double valorOriginal, JTextField txt, ArrayList<Pair<String,Object>> alteracoes,ArrayList<Object> novosValores, Consumer<Double> metodo)
    {            
        double novo = ehCampoNulo(txt.getText());
        if(valorOriginal!= novo)
        {
            alteracoes.add(new Pair(campo, valorOriginal));
            novosValores.add(novo);
            metodo.accept(novo);
        }
    }
    
    public void loadTableEspectro(List<ponto> espec)
    {
        System.out.println("LoadTableEspectro");
        DefaultTableModel model = (DefaultTableModel) tabEspec.getModel();
        if(espec != null)
            for(ponto p:espec)
                model.addRow(new Object[]{p.getX(), p.getY()});

    }
    
    private void btnSalvarAlteracoesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarAlteracoesActionPerformed
        /*
            Armazena em 'alteracoes' os campos alterados e os valores q carregou-se do banco, novo valor salvo em coleta
            Verifica quais campos de 'alteracoes' ainda não foram cadastrados
            Cadastra os campos ainda não alterados
            Altera 'coleta'
            
            Verificar se a PK mudou, se sim, mudar as alterações já cadastradas
        */
        try {
            
            AlteracaoDAO daoAlteracoes = new AlteracaoDAO();
            int i = 0;
            
            //alteracoes <- <Campo, valorOriginal>
            ArrayList<Pair<String,Object>> alteracoes = new ArrayList<>();
            ArrayList<Object> novosValores = new ArrayList<>();
            
            //Verifica quais JTextFields foram alterados
            preencheCampo("IrradGlobHorizontal", this.coletaOriginal.getIrradGlobHorizontal(), txtIrradGlobHorizontal, alteracoes, novosValores,this.getColetaOriginal():: setIrradGlobHorizontal);
            preencheCampo("DesvGlobHorizontal", this.coletaOriginal.getDesvGlobHorizontal(), txtDesvGlobHorizontal, alteracoes, novosValores,this.getColetaOriginal():: setDesvGlobHorizontal);
            preencheCampo("IrradDirNormal", this.coletaOriginal.getIrradDirNormal(), txtIrradDirNormal, alteracoes, novosValores,this.getColetaOriginal()::setIrradDirNormal);
            preencheCampo("DesvDirNormal", this.coletaOriginal.getDesvDirNormal(), txtDesvDirNormal, alteracoes, novosValores,this.getColetaOriginal()::setDesvDirNormal);
            preencheCampo("RadiacaoDifusa", this.coletaOriginal.getRadiacaoDifusa(), txtRadDif, alteracoes, novosValores,this.getColetaOriginal()::setRadiacaoDifusa);
            preencheCampo("DesvDifusa", this.coletaOriginal.getDesvDifusa(), txtDesvDifusa, alteracoes, novosValores,this.getColetaOriginal()::setDesvDifusa);
            preencheCampo("TempAr", this.coletaOriginal.getTempAr(), txtTempAr, alteracoes, novosValores,this.getColetaOriginal()::setTempAr);
            preencheCampo("UmidAr", this.coletaOriginal.getUmidAr(), txtUmidAr, alteracoes, novosValores,this.getColetaOriginal()::setUmidAr);
            preencheCampo("PressAr", this.coletaOriginal.getPressAr(), txtPressAr, alteracoes, novosValores,this.getColetaOriginal()::setPressAr);

            preencheCampo("CorrCC_aSi", this.coletaOriginal.getCorrCC_aSi(), txtCCaSi, alteracoes, novosValores,this.getColetaOriginal()::setCorrCC_aSi);
            preencheCampo("CorrCC_micSi", this.coletaOriginal.getCorrCC_micSi(), txtCCmicSi, alteracoes, novosValores, this.getColetaOriginal()::setCorrCC_micSi);
            preencheCampo("CorrCC_pSi", this.coletaOriginal.getCorrCC_pSi(), txtCCpSi, alteracoes, novosValores, this.getColetaOriginal()::setCorrCC_pSi);
            preencheCampo("CorrCC_moSi", this.coletaOriginal.getCorrCC_moSi(), txtCCmoSi, alteracoes, novosValores, this.getColetaOriginal()::setCorrCC_moSi);
            preencheCampo("CorrCC_GaAs", this.coletaOriginal.getCorrCC_GaAs(), txtCCGaAs, alteracoes, novosValores, this.getColetaOriginal()::setCorrCC_GaAs);
            preencheCampo("CorrCC_TJ", this.coletaOriginal.getCorrCC_TJ(), txtCCTJ, alteracoes, novosValores, this.getColetaOriginal()::setCorrCC_TJ);

            preencheCampo("DesvCorrCC_aSi", this.coletaOriginal.getDesvCorrCC_aSi(), txtDesvCCaSi, alteracoes, novosValores, this.getColetaOriginal()::setDesvCorrCC_aSi);
            preencheCampo("DesvCorrCC_micSi", this.coletaOriginal.getDesvCorrCC_micSi(), txtDesvCCmicSi, alteracoes, novosValores, this.getColetaOriginal()::setDesvCorrCC_micSi);
            preencheCampo("DesvCorrCC_pSi", this.coletaOriginal.getDesvCorrCC_pSi(), txtDesvCCpSi, alteracoes, novosValores, this.getColetaOriginal()::setDesvCorrCC_pSi);
            preencheCampo("DesvCorrCC_moSi", this.coletaOriginal.getDesvCorrCC_moSi(), txtDesvCCmoSi, alteracoes, novosValores, this.getColetaOriginal()::setDesvCorrCC_moSi);
            preencheCampo("DesvCorrCC_GaAs", this.coletaOriginal.getDesvCorrCC_GaAs(), txtDesvCCGaAs, alteracoes, novosValores, this.getColetaOriginal()::setDesvCorrCC_GaAs);
            preencheCampo("DesvCorrCC_TJ", this.coletaOriginal.getDesvCorrCC_TJ(), txtDesvCCTJ, alteracoes, novosValores, this.getColetaOriginal()::setDesvCorrCC_TJ);
            
            preencheCampo("TempMod_aSi", this.coletaOriginal.getTempMod_aSi(), txtTempaSi, alteracoes, novosValores, this.getColetaOriginal()::setTempMod_aSi);
            preencheCampo("TempMod_micSi", this.coletaOriginal.getTempMod_micSi(), txtTempmicSi, alteracoes, novosValores, this.getColetaOriginal()::setTempMod_micSi);
            preencheCampo("TempMod_pSi", this.coletaOriginal.getTempMod_pSi(), txtTemppSi, alteracoes, novosValores, this.getColetaOriginal()::setTempMod_pSi);
            preencheCampo("TempMod_moSi", this.coletaOriginal.getTempMod_moSi(), txtTempmoSi, alteracoes, novosValores, this.getColetaOriginal()::setTempMod_moSi);
            preencheCampo("TempMod_GaAs", this.coletaOriginal.getTempMod_GaAs(), txtTempGaAs, alteracoes, novosValores, this.getColetaOriginal()::setTempMod_GaAs);
            preencheCampo("TempMod_TJ", this.coletaOriginal.getTempMod_TJ(), txtTempTJ, alteracoes, novosValores, this.getColetaOriginal()::setTempMod_TJ);

            preencheCampo("DesvTempMod_aSi", this.coletaOriginal.getDesvTempMod_aSi(), txtDesvTempaSi, alteracoes, novosValores, this.getColetaOriginal()::setDesvTempMod_aSi);
            preencheCampo("DesvTempMod_micSi", this.coletaOriginal.getDesvTempMod_micSi(), txtDesvTempmicSi, alteracoes, novosValores, this.getColetaOriginal()::setDesvTempMod_micSi);
            preencheCampo("DesvTempMod_pSi", this.coletaOriginal.getDesvTempMod_pSi(), txtDesvTemppSi, alteracoes, novosValores, this.getColetaOriginal()::setDesvTempMod_pSi);
            preencheCampo("DesvTempMod_moSi", this.coletaOriginal.getDesvTempMod_moSi(), txtDesvTempmoSi, alteracoes, novosValores, this.getColetaOriginal()::setDesvTempMod_moSi);
            preencheCampo("DesvTempMod_GaAs", this.coletaOriginal.getDesvTempMod_GaAs(), txtDesvTempGaAs, alteracoes, novosValores, this.getColetaOriginal()::setDesvTempMod_GaAs);
            preencheCampo("DesvTempMod_TJ", this.coletaOriginal.getDesvTempMod_TJ(), txtDesvTempTJ, alteracoes, novosValores, this.getColetaOriginal()::setDesvTempMod_TJ);

            preencheCampo("IrrFotodiodo", this.coletaOriginal.getIrrFotodiodo(), txtIrradFoto, alteracoes, novosValores, this.getColetaOriginal()::setIrrFotodiodo);
            preencheCampo("DesvIrrFotodiodo", this.coletaOriginal.getDesvIrrFotodiodo(), txtDesvIrradFoto, alteracoes, novosValores, this.getColetaOriginal()::setDesvIrrFotodiodo);
            preencheCampo("IrrPiranômetro", this.coletaOriginal.getIrrPiranômetro(), txtIrradPir, alteracoes, novosValores, this.getColetaOriginal()::setIrrPiranômetro);
            preencheCampo("DesvIrrPiranômetro", this.coletaOriginal.getDesvIrrPiranômetro(), txtDesvIrradPir, alteracoes, novosValores, this.getColetaOriginal()::setDesvIrrPiranômetro);
            
            ConversorData dtConv = new ConversorData();
            Timestamp dtNew = dtConv.FormatarDataConvencionalHoraFinal(txtDt.getText(), txtHr.getText());
            Timestamp dtOld = this.coletaOriginal.getDataHora_col(); //usados no update PK
            String ctOld = this.coletaOriginal.getCidade();
            if(this.coletaOriginal.getDataHora_col().equals(dtNew) == false)
            {
                alteracoes.add(new Pair("DataHora_col", this.coletaOriginal.getDataHora_col()));
                novosValores.add(dtNew);
                this.getColetaOriginal().setDataHora_col(dtNew);
            }
            if(this.coletaOriginal.getCidade().compareTo(txtCidade.getText())== 0)
            {
                alteracoes.add(new Pair("Cidade", txtCidade.getText()));
                novosValores.add(txtCidade.getText());
                this.getColetaOriginal().setCidade(txtCidade.getText());
            }
            if(chkBValidacao.isSelected() != this.getColetaOriginal().isValidacao())
            {
                alteracoes.add(new Pair("Validacao", this.coletaOriginal.isValidacao()));
                novosValores.add(chkBValidacao.isSelected());
                this.getColetaOriginal().setValidacao(chkBValidacao.isSelected());
            }
            
            //Verifica se a PK foi alterada
            ColetaDAO daoColeta = new ColetaDAO();
            if((dtOld.compareTo(dtNew)!=0 && dtNew != null) || (ctOld.compareTo(this.coletaOriginal.getCidade())==1))
            {
                JOptionPane.showMessageDialog(null,"Verifica PK:\nCompara datas: "+dtOld.compareTo(dtNew)+
                        "\n dtNew: "+dtNew+
                        "\nCidade: "+ctOld.compareTo(this.coletaOriginal.getCidade()));
                    
                //Verifica se a PK já não foi usada, se sim, termina a operação
                if(daoColeta.selectPK(dtNew, txtCidade.getText()).isEmpty()==false)
                {
                    JOptionPane.showMessageDialog(null,"Chave de identificação já utilizada, por favor escolha outra.");
                    return;
                }
                
                daoAlteracoes.updatePK(dtOld, dtNew, ctOld, this.coletaOriginal.getCidade());
                EspectroDAO daoEspec = new EspectroDAO();
                daoEspec.updatePK(dtOld, dtNew, ctOld, this.coletaOriginal.getCidade());
                
                //Verifica se precisa alterar o espectro
                //Busca Espectro
                Espectro espectro = daoEspec.select(dtNew);
                if(espectro==null)
                    espectro = new Espectro();
                loadTableEspectro(espectro.getCurva());
                
                //Alterar PK do JFrame
                ConversorData dtconv = new ConversorData();
                this.setTitle("Banco de dados - "+ dtconv.FormatarTimeStampEmDataConvencional(dtNew)+" "+dtconv.FormatarTimeStampEmHora(dtNew)+ " - "+ txtCidade.getText());;
            }            
                            
            //Altera a Coleta
            daoColeta.alterarColeta(alteracoes, novosValores,dtOld, ctOld); //PK antiga
                        
            //Cadastro de Alterações
            //Separa os campos que serão cadastrados
            ArrayList<Pair<String,Object>> aux = new ArrayList<>();
            for(Pair a: alteracoes)
            {
                if(!getAlterados().contains((String)a.getKey()))
                {
                    aux.add(new Pair((String)a.getKey(), a.getValue()));
                    getAlterados().add((String)a.getKey());
                }
            }
            daoAlteracoes.cadAlteracoes(aux, this.getColetaOriginal().getDataHora_col(), this.getColetaOriginal().getCidade());
            
            /*
            //Atualiza parâmetros carregados na página
            for(String a: alteracoes)
            {
                if(!getAlterados().contains(a))
                    this.getAlterados().add(a);
            }
            if(val) this.getAlterados().add("validacao");//Atualiza os campos que foram alterados
            */
            JOptionPane.showMessageDialog(null,"Dados alterados com sucesso!\n");
            carregaColeta(this.getColetaOriginal());
            
        } catch (ParseException ex) {
            Logger.getLogger(CadIndivCol.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnSalvarAlteracoesActionPerformed
    
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        System.out.println("Adicionando ponto");
        DefaultTableModel model = (DefaultTableModel)tabEspec.getModel();
        ponto newP = new ponto(Double.parseDouble(txtAddCompOnda.getText().replace(',', '.')), Double.parseDouble(txtAddIrrad.getText().replace(',', '.')));
        ponto oldP = null;
        //Procura se já não está na tabela
        for(int i=0;i<model.getRowCount();i++)
        {
            if(newP.getX().equals(model.getValueAt(i, 0)))
            {
                oldP = new ponto((Double)model.getValueAt(i,0), (Double)model.getValueAt(i,1));
                model.removeRow(i);
                break;
            }
        }
        if(oldP != null)
        {
            retiraPonto(curvaAdd, oldP);
            curvaExc.add(oldP);
        }
        curvaAdd.add(newP);
        model.addRow(new Object[]{Double.parseDouble(txtAddCompOnda.getText().replace(',', '.')), Double.parseDouble(txtAddIrrad.getText().replace(',', '.'))});
        
        /*GraficoXY g = new GraficoXY();
        jPainelGrafico.removeAll();
        List<ponto> data = getTableData(tabEspec);
        jPainelGrafico.add(g.criar(data));
        jPainelGrafico.revalidate();
        jPainelGrafico.repaint();*/
        carregaGrafico();
        
        txtAddCompOnda.setText("");
        txtAddIrrad.setText("");
    }//GEN-LAST:event_jButton1ActionPerformed

    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed
        
        DefaultTableModel model = (DefaultTableModel) tabEspec.getModel();
        
        for(Integer i: tabEspec.getSelectedRows())
        {
            System.out.println("Removeu "+i);
            curvaExc.add(new ponto((Double)tabEspec.getValueAt(i, 0), (Double)tabEspec.getValueAt(i, 1)));
            model.removeRow(i);           
        }
    }//GEN-LAST:event_btnExcluirActionPerformed

    private void btnSalvarEspecActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarEspecActionPerformed
        PropertyChangeListener[] tcls = tabEspec.getPropertyChangeListeners();
        TableCellListener tcl = null;
        for(int i=0; i<tcls.length; i++)
            if(tcls[i] instanceof TableCellListener)
                tcl = (TableCellListener)tcls[i];
        
        Espectro espec = new Espectro();
        espec.setCidade(coletaOriginal.getCidade());
        espec.setDataHora(coletaOriginal.getDataHora_col());
        
        EspectroDAO dao = new EspectroDAO();
        
        espec.setCurva(curvaExc);
        dao.delete(espec);
        
        espec.setCurva(curvaAdd);
        dao.create(espec);
        
        if(txtEspectro.getText().compareTo("")!=0)
        {
            System.out.println("Entrou");
            espec.setCurva(txtEspectro.getText());
            dao.create(espec);
            
            DefaultTableModel model = (DefaultTableModel)tabEspec.getModel();
            for(ponto p:espec.getCurva())
            {
                model.addRow(new Object[]{p.getX(), p.getY()});
            }
            
            carregaGrafico();
        }
        else
            System.out.println("Não entrou");
                
        curvaAdd.clear();
        curvaExc.clear();
        System.out.println("Terminou de salvar");
    }//GEN-LAST:event_btnSalvarEspecActionPerformed

    private void btnProcurarEspecActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProcurarEspecActionPerformed
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Procurar arquivo do espectro solar");
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        FileNameExtensionFilter filtro = new FileNameExtensionFilter("Arquivo de texto *.tit ou *.txt", "tit", "txt");
        fileChooser.setFileFilter(filtro);
        if(fileChooser.showOpenDialog(this) == fileChooser.APPROVE_OPTION)
        {
            File arquivo = fileChooser.getSelectedFile();
            txtEspectro.setText(arquivo.getPath());
            txtEspectro.setEditable(false);
        }
    }//GEN-LAST:event_btnProcurarEspecActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CadIndivCol.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CadIndivCol.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CadIndivCol.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CadIndivCol.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CadIndivCol().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel BuscaIndivCol;
    private javax.swing.JButton btnExcluir;
    private javax.swing.JButton btnProcurarEspec;
    private javax.swing.JButton btnSalvarAlteracoes;
    private javax.swing.JButton btnSalvarEspec;
    private javax.swing.JCheckBox chkBValidacao;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPainelGrafico;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lblKT;
    private javax.swing.JTable tabEspec;
    private javax.swing.JTextField txtAddCompOnda;
    private javax.swing.JTextField txtAddIrrad;
    private javax.swing.JTextField txtCCGaAs;
    private javax.swing.JTextField txtCCTJ;
    private javax.swing.JTextField txtCCaSi;
    private javax.swing.JTextField txtCCmicSi;
    private javax.swing.JTextField txtCCmoSi;
    private javax.swing.JTextField txtCCpSi;
    private javax.swing.JTextField txtCidade;
    private javax.swing.JTextField txtDesvCCGaAs;
    private javax.swing.JTextField txtDesvCCTJ;
    private javax.swing.JTextField txtDesvCCaSi;
    private javax.swing.JTextField txtDesvCCmicSi;
    private javax.swing.JTextField txtDesvCCmoSi;
    private javax.swing.JTextField txtDesvCCpSi;
    private javax.swing.JTextField txtDesvDifusa;
    private javax.swing.JTextField txtDesvDirNormal;
    private javax.swing.JTextField txtDesvGlobHorizontal;
    private javax.swing.JTextField txtDesvIrradFoto;
    private javax.swing.JTextField txtDesvIrradPir;
    private javax.swing.JTextField txtDesvTempGaAs;
    private javax.swing.JTextField txtDesvTempTJ;
    private javax.swing.JTextField txtDesvTempaSi;
    private javax.swing.JTextField txtDesvTempmicSi;
    private javax.swing.JTextField txtDesvTempmoSi;
    private javax.swing.JTextField txtDesvTemppSi;
    private javax.swing.JFormattedTextField txtDt;
    private javax.swing.JTextField txtEspectro;
    private javax.swing.JFormattedTextField txtHr;
    private javax.swing.JTextField txtIrradDirNormal;
    private javax.swing.JTextField txtIrradFoto;
    private javax.swing.JTextField txtIrradGlobHorizontal;
    private javax.swing.JTextField txtIrradPir;
    private javax.swing.JTextField txtPressAr;
    private javax.swing.JTextField txtRadDif;
    private javax.swing.JTextField txtTempAr;
    private javax.swing.JTextField txtTempGaAs;
    private javax.swing.JTextField txtTempTJ;
    private javax.swing.JTextField txtTempaSi;
    private javax.swing.JTextField txtTempmicSi;
    private javax.swing.JTextField txtTempmoSi;
    private javax.swing.JTextField txtTemppSi;
    private javax.swing.JTextField txtUmidAr;
    // End of variables declaration//GEN-END:variables

}
