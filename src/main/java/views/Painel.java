/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;


import common.ArqvColeta;
import common.Bar;
import common.ColetaTableModel;
import common.ComboBoxConteudo;
import model.bean.Coleta;
import common.csv;
import common.dat;
import common.ConversorData;
import common.MessageBox;
import common.Render;
import common.xlsx;
import Backup.Backup;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.io.File;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.Pair;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.swing.text.MaskFormatter;
import model.bean.Espectro;
import model.bean.Usuario;
import model.dao.ColetaDAO;
import model.dao.EspectroDAO;
import model.dao.UsuarioDAO;
import org.apache.commons.io.FilenameUtils;

public class Painel extends javax.swing.JFrame {

    /**
     * Creates new form Painel
     */
    //Global
    ColetaTableModel modelBuscColetas = new ColetaTableModel();
    List<Coleta> coletasBusca = null;
    private Usuario usuario;
    List<Integer> indexSelectedColetas;
    
    public Painel() {
        
        initComponents();
        setTitle("Banco de dados");
        setLocationRelativeTo(null);
        setSize(650, 600);
        
        //Login
        this.usuario = null;
        btnMenuSair.setVisible(false);
        jButton2.setVisible(false);
        
        //Tela de Busca
        txtBuscDt.setVisible(true);
        txtBuscHr.setVisible(true);
        txtBuscaValor.setVisible(false);
        comboBxComparacaoBusca.setVisible(false);
        comboBxValidacao.setVisible(false);
        jButton5.setVisible(false);
        
        //Tabela de Resultados da Busca
        jtBusca.setModel(modelBuscColetas);
        jtBusca.setDefaultRenderer(Object.class, new Render());
        TableColumn sportColumn = jtBusca.getColumnModel().getColumn(0);
        JCheckBox chk = new JCheckBox();
        sportColumn.setCellEditor(new DefaultCellEditor(chk));
        jtBusca.setRowHeight(25);
        indexSelectedColetas = new ArrayList<Integer>();
        
    }

    public Painel(Usuario usuario, JPanel Buscar, JPanel Cadastrar, JPanel CadastrarUsuario, JPanel Login, JPanel Menu, JTabbedPane PanelCadDadosAmbientais, JPanel Root, JButton btnAbrirCadColetaIndiv,
            JButton btnBuscArqvBD, JButton btnBuscDadosModFot, JButton btnBuscar, JButton btnBuscarVoltar, JButton btnCadUsuCadastrar, JButton btnCadUsuVoltar, JButton btnCadVoltar, JButton btnCadastrarCadDadosAmb, JButton btnCadastrarCadEspec, JButton btnCadastrarDadosBD, JButton btnCadastrarDadosModFot, JButton btnCadastrarProcurarDadosAmbProc, JButton btnCadastrarProcurarEspec, JButton btnDownload, JButton btnLoginCad, JButton btnLoginLogin, JButton btnLoginVoltar, JComboBox<String> comboBxComparacaoBusca, JComboBox<String> comboBxItemBusca, JComboBox<String> comboBxValidacao, JButton jButton1, JButton jButton2, JButton jButton3, JLabel jLabel1, JLabel jLabel10, JLabel jLabel11, JLabel jLabel12, JLabel jLabel13, JLabel jLabel14, JLabel jLabel15, JLabel jLabel16, JLabel jLabel17, JLabel jLabel2, JLabel jLabel3, JLabel jLabel4, JLabel jLabel5, JLabel jLabel6, JLabel jLabel7, JLabel jLabel8, JLabel jLabel9, JPanel jPanel1, JPanel jPanel2, JPanel jPanel3, JPanel jPanel4, JScrollPane jScrollPane2, JTable jtBusca, JTextField txtBoxArquivoCSV, JTextField txtBoxArquivoEspect, JTextField txtBoxArquivoModFot, JFormattedTextField txtBuscDt, JFormattedTextField txtBuscDt2, JFormattedTextField txtBuscHr, JFormattedTextField txtBuscHr2, JTextField txtBuscaValor, JTextField txtCadDadosBD, JFormattedTextField txtCadEspecDt, JFormattedTextField txtCadEspecHr, JTextField txtCadUsuNome, JPasswordField txtCadUsuSenha, JPasswordField txtCadUsuSenha2, JTextField txtCadUsuUsuario, JPasswordField txtLoginSenha, JTextField txtLoginUsu) throws HeadlessException {
        this.usuario = usuario;
        this.Buscar = Buscar;
        this.Cadastrar = Cadastrar;
        this.CadastrarUsuario = CadastrarUsuario;
        this.Login = Login;
        this.Menu = Menu;
        this.PanelCadDadosAmbientais = PanelCadDadosAmbientais;
        this.Root = Root;
        this.btnBuscArqvBD = btnBuscArqvBD;
        this.btnBuscDadosModFot = btnBuscDadosModFot;
        this.btnBuscar = btnBuscar;
        this.btnBuscarVoltar = btnBuscarVoltar;
        this.btnCadUsuCadastrar = btnCadUsuCadastrar;
        this.btnCadUsuVoltar = btnCadUsuVoltar;
        this.btnCadVoltar = btnCadVoltar;
        this.btnCadastrarCadDadosAmb = btnCadastrarCadDadosAmb;
        this.btnCadastrarCadEspec = btnCadastrarCadEspec;
        this.btnCadastrarDadosBD = btnCadastrarDadosBD;
        this.btnCadastrarDadosModFot = btnCadastrarDadosModFot;
        this.btnCadastrarProcurarDadosAmbProc = btnCadastrarProcurarDadosAmbProc;
        this.btnCadastrarProcurarEspec = btnCadastrarProcurarEspec;
        this.btnDownload = btnDownload;
        this.btnLoginCad = btnLoginCad;
        this.btnLoginLogin = btnLoginLogin;
        this.btnLoginVoltar = btnLoginVoltar;
        this.comboBxComparacaoBusca = comboBxComparacaoBusca;
        this.comboBxItemBusca = comboBxItemBusca;
        this.comboBxValidacao = comboBxValidacao;
        this.btnMenuLogin = jButton1;
        this.jButton2 = jButton2;
        this.jButton3 = jButton3;
        this.jLabel1 = jLabel1;
        this.jLabel10 = jLabel10;
        this.jLabel11 = jLabel11;
        this.jLabel12 = jLabel12;
        this.jLabel13 = jLabel13;
        this.jLabel14 = jLabel14;
        this.jLabel15 = jLabel15;
        this.jLabel16 = jLabel16;
        this.jLabel17 = jLabel17;
        this.jLabel2 = jLabel2;
        this.jLabel3 = jLabel3;
        this.jLabel4 = jLabel4;
        this.jLabel5 = jLabel5;
        this.jLabel6 = jLabel6;
        this.jLabel7 = jLabel7;
        this.jLabel8 = jLabel8;
        this.jLabel9 = jLabel9;
        this.jPanel1 = jPanel1;
        this.jPanel2 = jPanel2;
        this.jPanel3 = jPanel3;
        this.jPanel4 = jPanel4;
        this.jScrollPane2 = jScrollPane2;
        this.jtBusca = jtBusca;
        this.txtBoxArquivoCSV = txtBoxArquivoCSV;
        this.txtBoxArquivoEspect = txtBoxArquivoEspect;
        this.txtBoxArquivoModFot = txtBoxArquivoModFot;
        this.txtBuscDt = txtBuscDt;
        this.txtBuscDt2 = txtBuscDt2;
        this.txtBuscHr = txtBuscHr;
        this.txtBuscHr2 = txtBuscHr2;
        this.txtBuscaValor = txtBuscaValor;
        this.txtCadDadosBD = txtCadDadosBD;
        this.txtCadEspecDt = txtCadEspecDt;
        this.txtCadEspecHr = txtCadEspecHr;
        this.txtCadUsuNome = txtCadUsuNome;
        this.txtCadUsuSenha = txtCadUsuSenha;
        this.txtCadUsuSenha2 = txtCadUsuSenha2;
        this.txtCadUsuUsuario = txtCadUsuUsuario;
        this.txtLoginSenha = txtLoginSenha;
        this.txtLoginUsu = txtLoginUsu;
    }
    
    public void setUsuario(Usuario usuario)
    {
        this.usuario = usuario;
    }
    public Usuario getUsuario()
    {
        return this.usuario;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGroupBackup = new javax.swing.ButtonGroup();
        Root = new javax.swing.JPanel();
        Menu = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        btnMenuLogin = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        btnMenuSair = new javax.swing.JButton();
        btnBackup = new javax.swing.JButton();
        Login = new javax.swing.JPanel();
        txtLoginUsu = new javax.swing.JTextField();
        btnLoginLogin = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtLoginSenha = new javax.swing.JPasswordField();
        btnLoginCad = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        btnLoginVoltar = new javax.swing.JButton();
        Cadastrar = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        btnCadVoltar = new javax.swing.JButton();
        PanelCadDadosAmbientais = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        btnCadastrarProcurarDadosAmbProc = new javax.swing.JButton();
        txtBoxArquivoCSV = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        btnCadastrarCadDadosAmb = new javax.swing.JButton();
        jLabel18 = new javax.swing.JLabel();
        txtCadDadosAmbCidade = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        txtBoxArquivoModFot = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        btnBuscDadosModFot = new javax.swing.JButton();
        btnCadastrarDadosModFot = new javax.swing.JButton();
        jLabel19 = new javax.swing.JLabel();
        txtBoxArquivoModCidade = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        txtCadDadosBD = new javax.swing.JTextField();
        btnBuscArqvBD = new javax.swing.JButton();
        btnCadastrarDadosBD = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        txtBoxArquivoEspect = new javax.swing.JTextField();
        btnCadastrarProcurarEspec = new javax.swing.JButton();
        btnCadastrarCadEspec = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        txtCadEspecHr = new javax.swing.JFormattedTextField();
        txtCadEspecDt = new javax.swing.JFormattedTextField();
        txtCadEspecCidade = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel21 = new javax.swing.JLabel();
        txtGenArqv = new javax.swing.JTextField();
        btnGenProcurar = new javax.swing.JButton();
        jLabel22 = new javax.swing.JLabel();
        txtGenDt = new javax.swing.JFormattedTextField();
        jLabel23 = new javax.swing.JLabel();
        txtGenHr = new javax.swing.JFormattedTextField();
        jLabel24 = new javax.swing.JLabel();
        txtGenCidade = new javax.swing.JTextField();
        btnCadastrarCadEspec1 = new javax.swing.JButton();
        comboBoxGen = new javax.swing.JComboBox<>();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        txtGenData = new javax.swing.JFormattedTextField();
        txtGenHeader = new javax.swing.JFormattedTextField();
        jLabel28 = new javax.swing.JLabel();
        txtGenDiv = new javax.swing.JTextField();
        CadastrarUsuario = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        btnCadUsuVoltar = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtCadUsuNome = new javax.swing.JTextField();
        txtCadUsuUsuario = new javax.swing.JTextField();
        btnCadUsuCadastrar = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        txtCadUsuSenha = new javax.swing.JPasswordField();
        txtCadUsuSenha2 = new javax.swing.JPasswordField();
        jLabel12 = new javax.swing.JLabel();
        Buscar = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        btnBuscar = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jtBusca = new javax.swing.JTable();
        txtBuscDt = new javax.swing.JFormattedTextField();
        txtBuscHr = new javax.swing.JFormattedTextField();
        btnBuscarVoltar = new javax.swing.JButton();
        comboBxItemBusca = new javax.swing.JComboBox<>();
        comboBxComparacaoBusca = new javax.swing.JComboBox<>();
        txtBuscaValor = new javax.swing.JTextField();
        comboBxValidacao = new javax.swing.JComboBox<>();
        txtBuscDt2 = new javax.swing.JFormattedTextField();
        txtBuscHr2 = new javax.swing.JFormattedTextField();
        btnDownload = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        btnBuscarSelecionarTodos = new javax.swing.JButton();
        Backup = new javax.swing.JPanel();
        txtBackupDump = new javax.swing.JTextField();
        btnBackupProcDump = new javax.swing.JButton();
        rdBtnRestaurar = new javax.swing.JRadioButton();
        rdBtnDownload = new javax.swing.JRadioButton();
        label1 = new java.awt.Label();
        txtBackupFile = new javax.swing.JTextField();
        btnBackupProcDump1 = new javax.swing.JButton();
        label2 = new java.awt.Label();
        label3 = new java.awt.Label();
        btnBackupAcao = new javax.swing.JButton();
        btnBackupVoltar = new javax.swing.JButton();
        jLabel29 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(600, 670));
        setSize(new java.awt.Dimension(600, 1000));

        Root.setMinimumSize(new java.awt.Dimension(600, 1000));
        Root.setPreferredSize(new java.awt.Dimension(600, 1000));
        Root.setLayout(new java.awt.CardLayout());

        Menu.setBackground(new java.awt.Color(39, 39, 39));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Banco de Dados Células Solares");

        btnMenuLogin.setBackground(new java.awt.Color(255, 102, 0));
        btnMenuLogin.setText("Login");
        btnMenuLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMenuLoginActionPerformed(evt);
            }
        });

        jButton2.setBackground(new java.awt.Color(255, 102, 0));
        jButton2.setText("Cadastrar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setBackground(new java.awt.Color(255, 102, 0));
        jButton3.setText("Buscar");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        btnMenuSair.setBackground(new java.awt.Color(255, 102, 0));
        btnMenuSair.setText("Sair");
        btnMenuSair.setFocusTraversalPolicyProvider(true);
        btnMenuSair.setMaximumSize(new java.awt.Dimension(65, 23));
        btnMenuSair.setMinimumSize(new java.awt.Dimension(65, 23));
        btnMenuSair.setPreferredSize(new java.awt.Dimension(65, 23));
        btnMenuSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMenuSairActionPerformed(evt);
            }
        });

        btnBackup.setBackground(new java.awt.Color(255, 102, 0));
        btnBackup.setText("Backup");
        btnBackup.setToolTipText("");
        btnBackup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackupActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout MenuLayout = new javax.swing.GroupLayout(Menu);
        Menu.setLayout(MenuLayout);
        MenuLayout.setHorizontalGroup(
            MenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MenuLayout.createSequentialGroup()
                .addGap(169, 169, 169)
                .addGroup(MenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, MenuLayout.createSequentialGroup()
                        .addGroup(MenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(btnMenuLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnMenuSair, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnBackup, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(77, 77, 77)))
                .addContainerGap(180, Short.MAX_VALUE))
        );
        MenuLayout.setVerticalGroup(
            MenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MenuLayout.createSequentialGroup()
                .addGap(160, 160, 160)
                .addComponent(jLabel1)
                .addGap(34, 34, 34)
                .addComponent(btnMenuLogin)
                .addGap(18, 18, 18)
                .addComponent(jButton2)
                .addGap(18, 18, 18)
                .addComponent(jButton3)
                .addGap(18, 18, 18)
                .addComponent(btnBackup)
                .addGap(18, 18, 18)
                .addComponent(btnMenuSair, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(288, Short.MAX_VALUE))
        );

        Root.add(Menu, "Menu");

        Login.setBackground(new java.awt.Color(39, 39, 39));
        Login.setMinimumSize(new java.awt.Dimension(600, 670));
        Login.setPreferredSize(new java.awt.Dimension(600, 670));

        btnLoginLogin.setBackground(new java.awt.Color(255, 51, 0));
        btnLoginLogin.setText("Login");
        btnLoginLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoginLoginActionPerformed(evt);
            }
        });

        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Usuário:");

        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Senha:");

        btnLoginCad.setBackground(new java.awt.Color(255, 51, 0));
        btnLoginCad.setText("Cadastre-se");
        btnLoginCad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoginCadActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Login");

        btnLoginVoltar.setBackground(new java.awt.Color(255, 51, 0));
        btnLoginVoltar.setText("Voltar");
        btnLoginVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoginVoltarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout LoginLayout = new javax.swing.GroupLayout(Login);
        Login.setLayout(LoginLayout);
        LoginLayout.setHorizontalGroup(
            LoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(LoginLayout.createSequentialGroup()
                .addGap(182, 182, 182)
                .addGroup(LoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(LoginLayout.createSequentialGroup()
                        .addComponent(btnLoginLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnLoginCad, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(LoginLayout.createSequentialGroup()
                        .addGap(104, 104, 104)
                        .addComponent(jLabel4))
                    .addGroup(LoginLayout.createSequentialGroup()
                        .addGroup(LoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addGap(36, 36, 36)
                        .addGroup(LoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtLoginSenha, javax.swing.GroupLayout.DEFAULT_SIZE, 163, Short.MAX_VALUE)
                            .addComponent(txtLoginUsu))))
                .addContainerGap(179, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, LoginLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnLoginVoltar)
                .addGap(41, 41, 41))
        );
        LoginLayout.setVerticalGroup(
            LoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(LoginLayout.createSequentialGroup()
                .addGap(92, 92, 92)
                .addComponent(jLabel4)
                .addGap(44, 44, 44)
                .addGroup(LoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtLoginUsu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(27, 27, 27)
                .addGroup(LoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtLoginSenha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addGroup(LoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnLoginLogin)
                    .addComponent(btnLoginCad))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 280, Short.MAX_VALUE)
                .addComponent(btnLoginVoltar)
                .addGap(92, 92, 92))
        );

        Root.add(Login, "Login");

        Cadastrar.setBackground(new java.awt.Color(39, 39, 39));
        Cadastrar.setMinimumSize(new java.awt.Dimension(400, 400));
        Cadastrar.setPreferredSize(new java.awt.Dimension(600, 670));

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Cadastrar");

        btnCadVoltar.setText("Voltar");
        btnCadVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCadVoltarActionPerformed(evt);
            }
        });

        PanelCadDadosAmbientais.setBackground(new java.awt.Color(255, 255, 255));
        PanelCadDadosAmbientais.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        PanelCadDadosAmbientais.setFocusCycleRoot(true);

        jPanel1.setBackground(new java.awt.Color(97, 97, 97));
        jPanel1.setToolTipText("");

        btnCadastrarProcurarDadosAmbProc.setBackground(new java.awt.Color(255, 102, 0));
        btnCadastrarProcurarDadosAmbProc.setText("Procurar");
        btnCadastrarProcurarDadosAmbProc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCadastrarProcurarDadosAmbProcActionPerformed(evt);
            }
        });

        jLabel6.setForeground(new java.awt.Color(204, 204, 204));
        jLabel6.setText("Arquivo:");

        btnCadastrarCadDadosAmb.setBackground(new java.awt.Color(255, 102, 0));
        btnCadastrarCadDadosAmb.setText("Cadastrar");
        btnCadastrarCadDadosAmb.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCadastrarCadDadosAmbActionPerformed(evt);
            }
        });

        jLabel18.setForeground(new java.awt.Color(204, 204, 204));
        jLabel18.setText("Cidade:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(101, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel18)
                    .addComponent(jLabel6))
                .addGap(10, 39, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtCadDadosAmbCidade, javax.swing.GroupLayout.DEFAULT_SIZE, 180, Short.MAX_VALUE)
                    .addComponent(txtBoxArquivoCSV))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnCadastrarCadDadosAmb)
                    .addComponent(btnCadastrarProcurarDadosAmbProc, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(41, 41, 41))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(68, 68, 68)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(btnCadastrarProcurarDadosAmbProc)
                    .addComponent(txtBoxArquivoCSV, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(txtCadDadosAmbCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 90, Short.MAX_VALUE)
                .addComponent(btnCadastrarCadDadosAmb)
                .addGap(28, 28, 28))
        );

        jLabel18.getAccessibleContext().setAccessibleName("lblCadDadosAmbCidade");
        txtCadDadosAmbCidade.getAccessibleContext().setAccessibleName("txtCadDadosAmbCidade");

        PanelCadDadosAmbientais.addTab("Dados Ambientais", jPanel1);

        jPanel3.setBackground(new java.awt.Color(97, 97, 97));

        jLabel16.setForeground(new java.awt.Color(204, 204, 204));
        jLabel16.setText("Arquivo:");

        btnBuscDadosModFot.setBackground(new java.awt.Color(255, 102, 0));
        btnBuscDadosModFot.setText("Procurar");
        btnBuscDadosModFot.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscDadosModFotActionPerformed(evt);
            }
        });

        btnCadastrarDadosModFot.setBackground(new java.awt.Color(255, 102, 0));
        btnCadastrarDadosModFot.setText("Cadastrar");
        btnCadastrarDadosModFot.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCadastrarDadosModFotActionPerformed(evt);
            }
        });

        jLabel19.setForeground(new java.awt.Color(204, 204, 204));
        jLabel19.setText("Cidade:");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(82, 82, 82)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel16)
                            .addComponent(jLabel19))
                        .addGap(7, 7, 7)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtBoxArquivoModCidade, javax.swing.GroupLayout.DEFAULT_SIZE, 180, Short.MAX_VALUE)
                            .addComponent(txtBoxArquivoModFot, javax.swing.GroupLayout.DEFAULT_SIZE, 180, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnBuscDadosModFot, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnCadastrarDadosModFot)))
                .addGap(41, 41, 41))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(68, 68, 68)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBoxArquivoModFot, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel16)
                    .addComponent(btnBuscDadosModFot))
                .addGap(9, 9, 9)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(txtBoxArquivoModCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 90, Short.MAX_VALUE)
                .addComponent(btnCadastrarDadosModFot)
                .addGap(28, 28, 28))
        );

        txtBoxArquivoModCidade.getAccessibleContext().setAccessibleName("txtCadDadosModCidade");
        txtBoxArquivoModCidade.getAccessibleContext().setAccessibleDescription("");

        PanelCadDadosAmbientais.addTab("Dados dos Módulos Fotovoltaicos", jPanel3);

        jPanel4.setBackground(new java.awt.Color(97, 97, 97));

        jLabel17.setForeground(new java.awt.Color(204, 204, 204));
        jLabel17.setText("Arquivo:");

        txtCadDadosBD.setForeground(new java.awt.Color(204, 204, 204));
        txtCadDadosBD.setOpaque(false);

        btnBuscArqvBD.setBackground(new java.awt.Color(255, 102, 0));
        btnBuscArqvBD.setText("Procurar");
        btnBuscArqvBD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscArqvBDActionPerformed(evt);
            }
        });

        btnCadastrarDadosBD.setBackground(new java.awt.Color(255, 102, 0));
        btnCadastrarDadosBD.setText("Cadastrar");
        btnCadastrarDadosBD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCadastrarDadosBDActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGap(82, 82, 82)
                .addComponent(jLabel17)
                .addGap(18, 18, 18)
                .addComponent(txtCadDadosBD, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnCadastrarDadosBD, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnBuscArqvBD, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(41, 41, 41))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(68, 68, 68)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCadDadosBD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17)
                    .addComponent(btnBuscArqvBD))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 119, Short.MAX_VALUE)
                .addComponent(btnCadastrarDadosBD)
                .addGap(28, 28, 28))
        );

        PanelCadDadosAmbientais.addTab("Dados do Banco", jPanel4);

        jPanel5.setBackground(new java.awt.Color(97, 97, 97));
        jPanel5.setForeground(new java.awt.Color(255, 255, 255));

        jButton1.setBackground(new java.awt.Color(255, 102, 0));
        jButton1.setText("Abrir");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(189, Short.MAX_VALUE)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(149, 149, 149))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(106, 106, 106)
                .addComponent(jButton1)
                .addContainerGap(132, Short.MAX_VALUE))
        );

        PanelCadDadosAmbientais.addTab("Coleta Individual", jPanel5);

        jPanel2.setBackground(new java.awt.Color(97, 97, 97));

        btnCadastrarProcurarEspec.setBackground(new java.awt.Color(255, 102, 0));
        btnCadastrarProcurarEspec.setText("Procurar");
        btnCadastrarProcurarEspec.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCadastrarProcurarEspecActionPerformed(evt);
            }
        });

        btnCadastrarCadEspec.setBackground(new java.awt.Color(255, 102, 0));
        btnCadastrarCadEspec.setText("Cadastrar");
        btnCadastrarCadEspec.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCadastrarCadEspecActionPerformed(evt);
            }
        });

        jLabel5.setForeground(new java.awt.Color(204, 204, 204));
        jLabel5.setText("Arquivo:");

        jLabel13.setBackground(new java.awt.Color(204, 204, 204));
        jLabel13.setForeground(new java.awt.Color(204, 204, 204));
        jLabel13.setText("Data:");

        jLabel14.setForeground(new java.awt.Color(204, 204, 204));
        jLabel14.setText("Hora:");

        try {
            txtCadEspecHr.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##:##:##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtCadEspecHr.setText("");

        try {
            txtCadEspecDt.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtCadEspecDt.setText("");

        jLabel20.setForeground(new java.awt.Color(204, 204, 204));
        jLabel20.setText("Cidade:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap(357, Short.MAX_VALUE)
                        .addComponent(btnCadastrarCadEspec))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                        .addGap(82, 82, 82)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel20)
                            .addComponent(jLabel14)
                            .addComponent(jLabel13))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtBoxArquivoEspect, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, Short.MAX_VALUE)
                                .addComponent(btnCadastrarProcurarEspec, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtCadEspecHr, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtCadEspecDt, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtCadEspecCidade, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap(55, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(68, 68, 68)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCadastrarProcurarEspec)
                    .addComponent(txtBoxArquivoEspect, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(9, 9, 9)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(txtCadEspecDt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(txtCadEspecHr, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCadEspecCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel20))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
                .addComponent(btnCadastrarCadEspec)
                .addGap(28, 28, 28))
        );

        PanelCadDadosAmbientais.addTab("Espectro", jPanel2);

        jPanel6.setBackground(new java.awt.Color(97, 97, 97));

        jLabel21.setForeground(new java.awt.Color(204, 204, 204));
        jLabel21.setText("Arquivo:");

        btnGenProcurar.setBackground(new java.awt.Color(255, 102, 0));
        btnGenProcurar.setText("Procurar");
        btnGenProcurar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenProcurarActionPerformed(evt);
            }
        });

        jLabel22.setBackground(new java.awt.Color(204, 204, 204));
        jLabel22.setForeground(new java.awt.Color(204, 204, 204));
        jLabel22.setText("Data:");

        try {
            txtGenDt.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtGenDt.setText("");

        jLabel23.setForeground(new java.awt.Color(204, 204, 204));
        jLabel23.setText("Hora:");

        try {
            txtGenHr.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##:##:##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtGenHr.setText("");

        jLabel24.setForeground(new java.awt.Color(204, 204, 204));
        jLabel24.setText("Cidade:");

        btnCadastrarCadEspec1.setBackground(new java.awt.Color(255, 102, 0));
        btnCadastrarCadEspec1.setText("Cadastrar");
        btnCadastrarCadEspec1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCadastrarCadEspec1ActionPerformed(evt);
            }
        });

        comboBoxGen.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Coleta", "Espectro" }));
        comboBoxGen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxGenActionPerformed(evt);
            }
        });

        jLabel25.setForeground(new java.awt.Color(204, 204, 204));
        jLabel25.setText("Tipo de Arquivo:");

        jLabel26.setForeground(new java.awt.Color(204, 204, 204));
        jLabel26.setText("Cabeçalho:");

        jLabel27.setForeground(new java.awt.Color(204, 204, 204));
        jLabel27.setText("Início Dados:");

        txtGenData.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(java.text.NumberFormat.getIntegerInstance())));

        txtGenHeader.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(java.text.NumberFormat.getIntegerInstance())));

        jLabel28.setForeground(new java.awt.Color(204, 204, 204));
        jLabel28.setText("Divisor de Dados:");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel24)
                    .addComponent(jLabel21))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtGenArqv, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtGenCidade, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addContainerGap(268, Short.MAX_VALUE)
                        .addComponent(jLabel27)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtGenData, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addGap(25, 25, 25)
                                .addComponent(jLabel25)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(comboBoxGen, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel23)
                                    .addComponent(jLabel22)
                                    .addComponent(jLabel26))
                                .addGap(10, 10, 10)
                                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtGenHr, javax.swing.GroupLayout.DEFAULT_SIZE, 109, Short.MAX_VALUE)
                                    .addComponent(txtGenDt, javax.swing.GroupLayout.DEFAULT_SIZE, 109, Short.MAX_VALUE)
                                    .addComponent(txtGenHeader))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel28)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(btnGenProcurar, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnCadastrarCadEspec1, javax.swing.GroupLayout.Alignment.TRAILING))
                            .addComponent(txtGenDiv, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(42, 42, 42)))
                .addGap(42, 42, 42))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboBoxGen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel25))
                .addGap(9, 9, 9)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGenProcurar)
                    .addComponent(txtGenArqv, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel21))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24)
                    .addComponent(txtGenCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 56, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel26)
                    .addComponent(txtGenHeader, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel27)
                    .addComponent(txtGenData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel22)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtGenDt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel28)
                        .addComponent(txtGenDiv, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(9, 9, 9)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCadastrarCadEspec1)
                    .addComponent(txtGenHr, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel23))
                .addContainerGap())
        );

        PanelCadDadosAmbientais.addTab("Arquivo genérico", jPanel6);

        javax.swing.GroupLayout CadastrarLayout = new javax.swing.GroupLayout(Cadastrar);
        Cadastrar.setLayout(CadastrarLayout);
        CadastrarLayout.setHorizontalGroup(
            CadastrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CadastrarLayout.createSequentialGroup()
                .addGroup(CadastrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(CadastrarLayout.createSequentialGroup()
                        .addGap(65, 65, 65)
                        .addComponent(PanelCadDadosAmbientais, javax.swing.GroupLayout.PREFERRED_SIZE, 469, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(CadastrarLayout.createSequentialGroup()
                        .addGap(256, 256, 256)
                        .addComponent(jLabel7)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, CadastrarLayout.createSequentialGroup()
                .addGap(0, 501, Short.MAX_VALUE)
                .addComponent(btnCadVoltar)
                .addGap(38, 38, 38))
        );
        CadastrarLayout.setVerticalGroup(
            CadastrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CadastrarLayout.createSequentialGroup()
                .addGap(70, 70, 70)
                .addComponent(jLabel7)
                .addGap(18, 18, 18)
                .addComponent(PanelCadDadosAmbientais, javax.swing.GroupLayout.PREFERRED_SIZE, 305, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 140, Short.MAX_VALUE)
                .addComponent(btnCadVoltar)
                .addGap(92, 92, 92))
        );

        Root.add(Cadastrar, "Cadastrar");

        CadastrarUsuario.setBackground(new java.awt.Color(39, 39, 39));
        CadastrarUsuario.setMinimumSize(new java.awt.Dimension(400, 400));

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Cadastrar Usuário");

        btnCadUsuVoltar.setBackground(new java.awt.Color(255, 51, 0));
        btnCadUsuVoltar.setText("Voltar");
        btnCadUsuVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCadUsuVoltarActionPerformed(evt);
            }
        });

        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Nome:");

        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Usuário:");

        btnCadUsuCadastrar.setBackground(new java.awt.Color(255, 51, 0));
        btnCadUsuCadastrar.setText("Cadastrar");
        btnCadUsuCadastrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCadUsuCadastrarActionPerformed(evt);
            }
        });

        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Senha:");

        txtCadUsuSenha2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCadUsuSenha2ActionPerformed(evt);
            }
        });

        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Digite a senha novamente:");

        javax.swing.GroupLayout CadastrarUsuarioLayout = new javax.swing.GroupLayout(CadastrarUsuario);
        CadastrarUsuario.setLayout(CadastrarUsuarioLayout);
        CadastrarUsuarioLayout.setHorizontalGroup(
            CadastrarUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, CadastrarUsuarioLayout.createSequentialGroup()
                .addGap(0, 108, Short.MAX_VALUE)
                .addGroup(CadastrarUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(CadastrarUsuarioLayout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addGap(18, 18, 18)
                        .addComponent(txtCadUsuSenha, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(CadastrarUsuarioLayout.createSequentialGroup()
                        .addGroup(CadastrarUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel9)
                            .addComponent(jLabel10)
                            .addComponent(jLabel12))
                        .addGap(18, 18, 18)
                        .addGroup(CadastrarUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCadUsuNome, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtCadUsuUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtCadUsuSenha2, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnCadUsuCadastrar, javax.swing.GroupLayout.Alignment.TRAILING))))
                .addGap(165, 165, 165))
            .addGroup(CadastrarUsuarioLayout.createSequentialGroup()
                .addGap(231, 231, 231)
                .addComponent(jLabel8)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, CadastrarUsuarioLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnCadUsuVoltar)
                .addGap(41, 41, 41))
        );
        CadastrarUsuarioLayout.setVerticalGroup(
            CadastrarUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CadastrarUsuarioLayout.createSequentialGroup()
                .addGap(93, 93, 93)
                .addComponent(jLabel8)
                .addGap(64, 64, 64)
                .addGroup(CadastrarUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCadUsuNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(CadastrarUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txtCadUsuUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(CadastrarUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCadUsuSenha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(CadastrarUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCadUsuSenha2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12))
                .addGap(35, 35, 35)
                .addComponent(btnCadUsuCadastrar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 94, Short.MAX_VALUE)
                .addComponent(btnCadUsuVoltar)
                .addGap(85, 85, 85))
        );

        Root.add(CadastrarUsuario, "CadastrarUsuario");

        Buscar.setBackground(new java.awt.Color(39, 39, 39));

        jLabel15.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jLabel15.setText("Buscar");

        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        jScrollPane2.setBackground(new java.awt.Color(153, 153, 153));

        jtBusca.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtBuscaMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jtBusca);

        try {
            txtBuscDt.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtBuscDt.setText("00/00/0000");

        try {
            txtBuscHr.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##:##:##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtBuscHr.setText("00:00:00");

        btnBuscarVoltar.setText("Voltar");
        btnBuscarVoltar.setAlignmentY(200.0F);
        btnBuscarVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarVoltarActionPerformed(evt);
            }
        });

        comboBxItemBusca.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Parâmetro de Busca", "Cidade", "Corrente de Curto Circuito do Módulo de aSi", "Corrente de Curto Circuito do Módulo de GaAs", "Corrente de Curto Circuito do Módulo de micSi", "Corrente de Curto Circuito do Módulo de moSi", "Corrente de Curto Circuito do Módulo de pSi", "Corrente de Curto Circuito do Módulo de TJ", "Data Hora", "Desvio Padrão Corrente de Curto Circuito do Módulo de aSi", "Desvio Padrão Corrente de Curto Circuito do Módulo de GaAs", "Desvio Padrão Corrente de Curto Circuito do Módulo de micSi", "Desvio Padrão Corrente de Curto Circuito do Módulo de moSi", "Desvio Padrão Corrente de Curto Circuito do Módulo de pSi", "Desvio Padrão Corrente de Curto Circuito do Módulo de TJ", "Desvio Padrão da Irradiância do Fotodiodo", "Desvio Padrão da Irradiância do Piranômetro", "Desvio Padrão da Irradiância Global Horizontal", "Desvio Padrão Difusa", "Desvio Padrão Irradiância Direta Normal", "Desvio Padrão Temperatura do Módulo de aSi", "Desvio Padrão Temperatura do Módulo de GaAs", "Desvio Padrão Temperatura do Módulo de micSi", "Desvio Padrão Temperatura do Módulo de moSi", "Desvio Padrão Temperatura do Módulo de pSi", "Desvio Padrão Temperatura do Módulo de TJ", "Irradiância Direta Normal", "Irradiância do Fotodiodo", "Irradiância do Piranômetro", "Irradiância Global Horizontal", "Pressão Ar", "Radiação Difusa", "Temperatura Ar", "Temperatura do Módulo de aSi", "Temperatura do Módulo de GaAs", "Temperatura do Módulo de micSi", "Temperatura do Módulo de moSi", "Temperatura do Módulo de pSi", "Temperatura do Módulo de TJ", "Umidade Ar", "Validação" }));
        comboBxItemBusca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBxItemBuscaActionPerformed(evt);
            }
        });

        comboBxComparacaoBusca.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Igual", "Maior", "Menor", "Maior ou Igual", "Menor ou Igual" }));

        comboBxValidacao.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Verdadeiro", "Falso" }));

        try {
            txtBuscDt2.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtBuscDt2.setText("00/00/0000");

        try {
            txtBuscHr2.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##:##:##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtBuscHr2.setText("00:00:00");

        btnDownload.setText("Download");
        btnDownload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDownloadActionPerformed(evt);
            }
        });

        jButton5.setText("Excluir");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        btnBuscarSelecionarTodos.setText("Selecionar Todos");
        btnBuscarSelecionarTodos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarSelecionarTodosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout BuscarLayout = new javax.swing.GroupLayout(Buscar);
        Buscar.setLayout(BuscarLayout);
        BuscarLayout.setHorizontalGroup(
            BuscarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BuscarLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addGroup(BuscarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(BuscarLayout.createSequentialGroup()
                        .addGroup(BuscarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(BuscarLayout.createSequentialGroup()
                                .addGap(181, 181, 181)
                                .addComponent(jLabel15))
                            .addGroup(BuscarLayout.createSequentialGroup()
                                .addGroup(BuscarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(BuscarLayout.createSequentialGroup()
                                        .addComponent(txtBuscDt2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtBuscHr2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, BuscarLayout.createSequentialGroup()
                                        .addComponent(txtBuscDt, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtBuscHr, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(comboBxValidacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(comboBxItemBusca, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(BuscarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(BuscarLayout.createSequentialGroup()
                                        .addComponent(comboBxComparacaoBusca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(txtBuscaValor, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(BuscarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(btnBuscar)
                                        .addComponent(btnBuscarSelecionarTodos)))))
                        .addContainerGap(34, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, BuscarLayout.createSequentialGroup()
                        .addGroup(BuscarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(BuscarLayout.createSequentialGroup()
                                .addComponent(btnDownload)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnBuscarVoltar))
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 522, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(39, Short.MAX_VALUE))))
        );
        BuscarLayout.setVerticalGroup(
            BuscarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BuscarLayout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addComponent(jLabel15)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(BuscarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboBxItemBusca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBuscaValor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboBxComparacaoBusca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(BuscarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnBuscar)
                    .addComponent(txtBuscDt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBuscHr, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboBxValidacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(BuscarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(BuscarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtBuscDt2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtBuscHr2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnBuscarSelecionarTodos))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 387, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(BuscarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnBuscarVoltar)
                    .addComponent(jButton5)
                    .addComponent(btnDownload))
                .addContainerGap(416, Short.MAX_VALUE))
        );

        Root.add(Buscar, "Buscar");

        Backup.setBackground(new java.awt.Color(39, 39, 39));

        txtBackupDump.setText("C:\\\\Program Files\\\\PostgreSQL\\\\10\\\\bin\\\\pg_dump.exe");

        btnBackupProcDump.setText("Procurar");
        btnBackupProcDump.setToolTipText("");
        btnBackupProcDump.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackupProcDumpActionPerformed(evt);
            }
        });

        rdBtnRestaurar.setBackground(new java.awt.Color(39, 39, 39));
        btnGroupBackup.add(rdBtnRestaurar);
        rdBtnRestaurar.setForeground(new java.awt.Color(255, 255, 255));
        rdBtnRestaurar.setText("Restaurar");
        rdBtnRestaurar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdBtnRestaurarActionPerformed(evt);
            }
        });

        rdBtnDownload.setBackground(new java.awt.Color(39, 39, 39));
        btnGroupBackup.add(rdBtnDownload);
        rdBtnDownload.setForeground(new java.awt.Color(255, 255, 255));
        rdBtnDownload.setText("Download");
        rdBtnDownload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdBtnDownloadActionPerformed(evt);
            }
        });

        label1.setForeground(new java.awt.Color(255, 255, 255));
        label1.setName(""); // NOI18N
        label1.setText("Operação:");

        btnBackupProcDump1.setText("Procurar");
        btnBackupProcDump1.setToolTipText("");
        btnBackupProcDump1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackupProcDump1ActionPerformed(evt);
            }
        });

        label2.setForeground(new java.awt.Color(255, 255, 255));
        label2.setName(""); // NOI18N
        label2.setText("Dumper/Restore:");

        label3.setForeground(new java.awt.Color(255, 255, 255));
        label3.setName(""); // NOI18N
        label3.setText("Arquivo:");

        btnBackupAcao.setText("Ok");
        btnBackupAcao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackupAcaoActionPerformed(evt);
            }
        });

        btnBackupVoltar.setText("Voltar");
        btnBackupVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackupVoltarActionPerformed(evt);
            }
        });

        jLabel29.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel29.setForeground(new java.awt.Color(204, 204, 204));
        jLabel29.setText("Backup");

        javax.swing.GroupLayout BackupLayout = new javax.swing.GroupLayout(Backup);
        Backup.setLayout(BackupLayout);
        BackupLayout.setHorizontalGroup(
            BackupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BackupLayout.createSequentialGroup()
                .addGroup(BackupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(BackupLayout.createSequentialGroup()
                        .addGap(48, 48, 48)
                        .addGroup(BackupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(label3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(label2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(BackupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtBackupFile)
                            .addGroup(BackupLayout.createSequentialGroup()
                                .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rdBtnDownload)
                                .addGap(18, 18, 18)
                                .addComponent(rdBtnRestaurar))
                            .addComponent(txtBackupDump))
                        .addGap(27, 27, 27)
                        .addGroup(BackupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnBackupVoltar)
                            .addGroup(BackupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(btnBackupProcDump, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnBackupProcDump1, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                                .addComponent(btnBackupAcao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(BackupLayout.createSequentialGroup()
                        .addGap(240, 240, 240)
                        .addComponent(jLabel29)))
                .addContainerGap(47, Short.MAX_VALUE))
        );
        BackupLayout.setVerticalGroup(
            BackupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BackupLayout.createSequentialGroup()
                .addGap(86, 86, 86)
                .addComponent(jLabel29)
                .addGap(41, 41, 41)
                .addGroup(BackupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(BackupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(rdBtnRestaurar)
                        .addComponent(rdBtnDownload))
                    .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(BackupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, BackupLayout.createSequentialGroup()
                        .addGroup(BackupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtBackupDump, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnBackupProcDump))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                    .addGroup(BackupLayout.createSequentialGroup()
                        .addComponent(label2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(14, 14, 14)))
                .addGroup(BackupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(label3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(BackupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtBackupFile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnBackupProcDump1)))
                .addGap(37, 37, 37)
                .addComponent(btnBackupAcao)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 217, Short.MAX_VALUE)
                .addComponent(btnBackupVoltar)
                .addGap(152, 152, 152))
        );

        Root.add(Backup, "Backup");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Root, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Root, javax.swing.GroupLayout.PREFERRED_SIZE, 691, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnMenuLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMenuLoginActionPerformed
        CardLayout cartao = (CardLayout) Root.getLayout();
        cartao.show(Root, "Login");
    }//GEN-LAST:event_btnMenuLoginActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        CardLayout cartao = (CardLayout) Root.getLayout();
        cartao.show(Root, "Cadastrar");
    }//GEN-LAST:event_jButton2ActionPerformed

    private void btnLoginCadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoginCadActionPerformed
        CardLayout cartao = (CardLayout) Root.getLayout();
        cartao.show(Root, "CadastrarUsuario");
    }//GEN-LAST:event_btnLoginCadActionPerformed

    private void btnLoginVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoginVoltarActionPerformed
         CardLayout cartao = (CardLayout) Root.getLayout();
         cartao.show(Root, "Menu");
    }//GEN-LAST:event_btnLoginVoltarActionPerformed

    private void btnLoginLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoginLoginActionPerformed
        System.out.println("Procurar Login");
        UsuarioDAO dao = new UsuarioDAO();
        Usuario usuario = dao.login(txtLoginUsu.getText(), txtLoginSenha.getText());
        if(usuario != null)
        {
            this.setUsuario(usuario);
            txtLoginSenha.setText("");
            txtLoginUsu.setText("");
            CardLayout cartao = (CardLayout) Root.getLayout();
            cartao.show(Root, "Menu");
            jButton2.setVisible(true);
            jButton5.setVisible(true);
            btnMenuSair.setVisible(true);
            btnMenuLogin.setVisible(false);
            
        }
        else
        {
            System.out.printf("Não encontrou login");
            JOptionPane.showMessageDialog(null, "Usuario ou senha inválidos!");
        }
    }//GEN-LAST:event_btnLoginLoginActionPerformed

    private void btnCadVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCadVoltarActionPerformed
        //Limpa as caixas
        txtBoxArquivoCSV.setText(null);
        txtBoxArquivoEspect.setText(null);
        
        //Voltar
        CardLayout cartao = (CardLayout) Root.getLayout();
        cartao.show(Root, "Menu");
    }//GEN-LAST:event_btnCadVoltarActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        CardLayout cartao = (CardLayout) Root.getLayout();
        System.out.println("Para Buscar");
        cartao.show(Root, "Buscar");        
    }//GEN-LAST:event_jButton3ActionPerformed

    private void btnCadUsuVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCadUsuVoltarActionPerformed
        //Limpa as caixas
        txtCadUsuNome.setText(null);
        txtCadUsuSenha.setText(null);
        txtCadUsuSenha2.setText(null);
        txtCadUsuUsuario.setText(null);
        
        //Voltar
        CardLayout cartao = (CardLayout) Root.getLayout();
        cartao.show(Root, "Login");
    }//GEN-LAST:event_btnCadUsuVoltarActionPerformed

    private void btnCadUsuCadastrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCadUsuCadastrarActionPerformed
        String senha1, senha2;
        senha1 = txtCadUsuSenha.getText();
        senha2 = txtCadUsuSenha2.getText();
        //Verifica se itens preenchidos
        if((txtCadUsuNome.getText().compareTo("")==0)||(txtCadUsuUsuario.getText().compareTo("")==0)
                ||(txtCadUsuSenha.getText().compareTo("")==0)||(txtCadUsuSenha2.getText().compareTo("")==0))
        {
            JOptionPane.showMessageDialog(null, "Preencha todos os campos para realizar o cadastro!");
            return;
        }
        
        //Cadastro
        if(senha1.compareTo(senha2)==0)
        {   
            //Verifica se elas são alfanumérica com no mínimo 8 caracteres
            if(senha1.length()<8 || senha1.matches("[A-Za-z0-9]+"))
            {
                JOptionPane.showMessageDialog(null,"A senha deve ser alfanumérica, letras maiúsculas e minúsculas e no mínimo 8 caracteres!");
            }
            else
            {
                //Tenho que procurar no banco se já existe um usuário com usuário igual
                Usuario usu = null;
                UsuarioDAO dao = new UsuarioDAO();
                usu = dao.select(txtCadUsuUsuario.getText());
                //Caso não exista, salvo
                if(usu == null)
                {
                    usu = new Usuario(txtCadUsuUsuario.getText(), txtCadUsuSenha.getText(), txtCadUsuNome.getText());
                    dao.create(usu);

                    txtCadUsuNome.setText("");
                    txtCadUsuSenha.setText("");
                    txtCadUsuSenha2.setText("");
                    txtCadUsuUsuario.setText("");
                    JOptionPane.showMessageDialog(null,"Usuário cadastrado com sucesso!");
                }
                else
                    JOptionPane.showMessageDialog(null,"Usuário já cadastrado!");
                }
            }
        else
            JOptionPane.showMessageDialog(null,"Senhas diferentes!");            
    }//GEN-LAST:event_btnCadUsuCadastrarActionPerformed

    private void txtCadUsuSenha2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCadUsuSenha2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCadUsuSenha2ActionPerformed

    private void btnMenuSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMenuSairActionPerformed
        
        btnMenuSair.setVisible(false);        
        btnMenuLogin.setVisible(true);
        jButton5.setVisible(false); //btnExcluir
        jButton2.setVisible(false); //btnCadastrar
        setUsuario(null);
    }//GEN-LAST:event_btnMenuSairActionPerformed

    private void btnBuscarSelecionarTodosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarSelecionarTodosActionPerformed

        Boolean value;
        if(btnBuscarSelecionarTodos.getText().compareTo("Selecionar Todos")==0)
        {
            value = true;
            for(int i=0; i<jtBusca.getRowCount();i++)
                indexSelectedColetas.add(new Integer(i));
            btnBuscarSelecionarTodos.setText("Desselecionar Todos");
        }
        else
        {
            value = false;
            indexSelectedColetas.clear();
            btnBuscarSelecionarTodos.setText("Selecionar Todos");
        }

        ColetaTableModel model = (ColetaTableModel) jtBusca.getModel();
        for(int i=0; i < model.getRowCount();i++)
        model.getData()[i][0] = value;
        model.fireTableDataChanged();
    }//GEN-LAST:event_btnBuscarSelecionarTodosActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        ColetaTableModel m = (ColetaTableModel) jtBusca.getModel();
        ArrayList<Pair<Timestamp,String>> dataRemocao = new ArrayList<>();
        List<Coleta> coletasRemocao = new ArrayList<>();

        for(Integer i: indexSelectedColetas)
        {
            dataRemocao.add(new Pair <Timestamp, String>(m.getColeta(i).getDataHora_col(),m.getColeta(i).getCidade()) {});
            coletasRemocao.add(m.getColeta(i));
        }

        ColetaDAO dao = new ColetaDAO();
        dao.deleta(dataRemocao);
        for(Coleta c: coletasRemocao)
        {
            m.getColetas().remove(c);
        }
        indexSelectedColetas.clear();
        m.loadData(m.getColetas());
        m.fireTableDataChanged();
    }//GEN-LAST:event_jButton5ActionPerformed

    private void btnDownloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDownloadActionPerformed
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Salvar Dados do Banco");
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fileChooser.setAcceptAllFileFilterUsed(false);

        JTextField txtNomeSaida = new JTextField();
        JLabel lblNomeArquivo = new JLabel("Nome do arquivo:");
        txtNomeSaida.setPreferredSize( new Dimension( 200, 24 ));

        JPanel accessory = new JPanel();
        accessory.setLayout(new FlowLayout());
        accessory.add(lblNomeArquivo);
        accessory.add(txtNomeSaida);
        fileChooser.setAccessory(accessory);
        //int ret = fchooser.showOpenDialog(frame);

        String caminho = "";
        if(fileChooser.showOpenDialog(this) == fileChooser.APPROVE_OPTION)
        {
            caminho = fileChooser.getSelectedFile().getPath();
        }
        csv file = new csv(caminho, txtNomeSaida.getText());
        
        List<Coleta> colDownload = new ArrayList<Coleta>();
        ColetaTableModel model = (ColetaTableModel) jtBusca.getModel();
        for(int i: this.indexSelectedColetas)
            colDownload.add(model.getColeta(i));
        
        if(!colDownload.isEmpty())
        {
            file.extractData(colDownload);
            JOptionPane.showMessageDialog(null,"Operação concluída!");
        }
        else
            JOptionPane.showMessageDialog(null,"Nenhum dado para download!");
    }//GEN-LAST:event_btnDownloadActionPerformed

    private void comboBxItemBuscaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBxItemBuscaActionPerformed
        String campo = (String) comboBxItemBusca.getSelectedItem();
        System.out.println("Campo: "+campo);
        if (campo == "Data Hora")
        {
            txtBuscDt.setVisible(true);
            txtBuscHr.setVisible(true);
            txtBuscaValor.setVisible(false);
            comboBxComparacaoBusca.setVisible(false);
            comboBxValidacao.setVisible(false);

        }
        else
        {
            if(campo == "Validação")
            {
                txtBuscDt.setVisible(true);
                txtBuscHr.setVisible(true);
                txtBuscaValor.setVisible(false);
                comboBxComparacaoBusca.setVisible(false);
                comboBxValidacao.setVisible(true);

                //Localização dos itens visíveis
                btnBuscar.setLocation(350, 100);
                comboBxValidacao.setLocation(300, 100);
            }
            else
            {
                if(campo == "Cidade")
                {
                    txtBuscDt.setVisible(true);
                    txtBuscHr.setVisible(true);
                    txtBuscaValor.setVisible(false);
                    comboBxComparacaoBusca.setVisible(false);
                    comboBxValidacao.setVisible(true);
                    comboBxComparacaoBusca.setVisible(false);

                    //Localização dos itens visíveis
                    btnBuscar.setLocation(350, 100);
                    comboBxValidacao.setLocation(300, 100);
                }
                else
                {
                    txtBuscDt.setVisible(true);
                    txtBuscHr.setVisible(true);
                    txtBuscaValor.setVisible(true);
                    comboBxComparacaoBusca.setVisible(true);
                    comboBxValidacao.setVisible(false);
                }
            }
        }
    }//GEN-LAST:event_comboBxItemBuscaActionPerformed

    private void btnBuscarVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarVoltarActionPerformed
        CardLayout cartao = (CardLayout) Root.getLayout();
        cartao.show(Root, "Menu");
    }//GEN-LAST:event_btnBuscarVoltarActionPerformed

    private void jtBuscaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtBuscaMouseClicked

        int column = jtBusca.getColumnModel().getColumnIndexAtX(evt.getX());
        int row = evt.getY()/25; // 25 é o tamanho de cada linha

        if(row<jtBusca.getRowCount() && row>=0 &&
            column < jtBusca.getColumnCount() && column >=0)
        {
            Object value = jtBusca.getValueAt(row, column);
            if(value instanceof JButton)
            {
                ((JButton)value).doClick();
                System.out.println("Abrir");

                //Selecionar a coleta
                ColetaTableModel m = (ColetaTableModel) jtBusca.getModel();
                System.out.println("Linha "+((JButton)value).getName());
                BuscaIndivCol telaBuscaIndivCol = new BuscaIndivCol(m.getColeta(row));
                telaBuscaIndivCol.setLocationRelativeTo(null);
                telaBuscaIndivCol.setVisible(true);
            }
            else if(value instanceof Boolean)
            {
                ColetaTableModel myModel = (ColetaTableModel)jtBusca.getModel();
                myModel.getData()[row][column] = ((Boolean)value)?(false):(true);
                if((Boolean)myModel.getData()[row][column])
                    indexSelectedColetas.add(row);
            }
        }
    }//GEN-LAST:event_jtBuscaMouseClicked

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed

        switch((String)comboBxItemBusca.getSelectedItem())
        {
            case "Data Hora":
            if(txtBuscDt.getText().compareTo("  /  /    ")==0)
            {
                JOptionPane.showMessageDialog(null,"Preencha o(s) campo(s) para realizar a busca!");
                return;
            }
            break;
            case "Validação":
            break;
            default:
            if(txtBuscaValor.getText().compareTo("")==0)
            {
                JOptionPane.showMessageDialog(null,"Preencha o(s) campo(s) para realizar a busca!");
                return;
            }
            else
                txtBuscaValor.setText(txtBuscaValor.getText().replace(',', '.'));
            break;
        }

        List<Coleta> coletas = null;
        ColetaDAO dao = new ColetaDAO();

        //Verificar se há intervalo ou não
        /*Modo de Busca:
        0 - Só o Campo; 1 - Campo e Data; 2 - Campo e DataHora;
        5 - Campo com intervalo de Datas; 6 - Campo com intervalo DataHora1 e data2 ;
        8 - Campo com intervalo de DataHora*/
        int modoBusca=0;
        Timestamp dt1 = null, dt2 = null;
        if(txtBuscDt.getText().compareTo("00/00/0000")!=0 && txtBuscDt.getText().compareTo("  /  /    ")!=0)
        {
            if((txtBuscHr.getText().compareTo("00:00:00")!=0)&&(txtBuscHr.getText().compareTo("  :  :  ")!=0))
            modoBusca+=2;
            else
            modoBusca+=1;
        }
        if(txtBuscDt2.getText().compareTo("00/00/0000")!=0 && modoBusca!=0 && txtBuscDt.getText().compareTo("  /  /    ")!=0)
        {
            if(txtBuscHr2.getText().compareTo("00:00:00")!=0 && txtBuscHr2.getText().compareTo("  :  :  ")!=0)
            modoBusca+=6;
            else
            modoBusca+=4;
        }
        System.out.println("Modo de Busca: "+ modoBusca);
        if(modoBusca == 1 || modoBusca == 5 || modoBusca == 7)
        {
            try {
                dt1 = ConversorData.FormatarDataConvencionalHoraFinal(txtBuscDt.getText(), "00:00:00");
            } catch (ParseException ex) {
                Logger.getLogger(Painel.class.getName()).log(Level.SEVERE, null, ex);
            }

            if(modoBusca == 1)
            dt2 = ConversorData.FinalDoDia(dt1);
            if(modoBusca == 5)
            {
                try {
                    dt2 = ConversorData.FormatarDataConvencionalHoraFinal(txtBuscDt2.getText(), "24:00:00");
                } catch (ParseException ex) {
                    Logger.getLogger(Painel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if(modoBusca == 7)
            {
                try {
                    dt2 = ConversorData.FormatarDataConvencionalHoraFinal(txtBuscDt2.getText(), txtBuscHr2.getText());
                } catch (ParseException ex) {
                    Logger.getLogger(Painel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        if(modoBusca == 2 || modoBusca == 6 || modoBusca == 8)
        {
            try {
                dt1 = ConversorData.FormatarDataConvencionalHoraFinal(txtBuscDt.getText(), txtBuscHr.getText());
            } catch (ParseException ex) {
                Logger.getLogger(Painel.class.getName()).log(Level.SEVERE, null, ex);
            }
            if(modoBusca == 6)
            {
                try{
                    dt2 = ConversorData.FormatarDataConvencionalHoraFinal(txtBuscDt2.getText(), "24:00:00");
                }catch (ParseException ex){
                    Logger.getLogger(Painel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if(modoBusca == 8)
            {
                try{
                    dt2 = ConversorData.FormatarDataConvencionalHoraFinal(txtBuscDt2.getText(), txtBuscHr2.getText());
                }catch (ParseException ex){
                    Logger.getLogger(Painel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        System.out.println("Dt1 = "+dt1+"\nDt2 = "+dt2);

        //Definir qual é o campo de busca
        switch((String)comboBxItemBusca.getSelectedItem())
        {
            case "Data Hora":
            coletas = dao.selectDtHr(dt1, dt2, (modoBusca==2)?(false):(true));
            break;
            case "Validação":
            String validacao = (String)comboBxValidacao.getSelectedItem();
            coletas = dao.selectValidacao((validacao.compareTo("Verdadeiro")==0)?(true):(false), modoBusca, dt1, dt2);
            break;
            case "Cidade":
            System.out.println("Selecionou Cidade");
            coletas = dao.selectCidade(txtBuscaValor.getText(), modoBusca, dt2, dt2);
            break;
            default:
            ComboBoxConteudo cmBox = new ComboBoxConteudo();
            String campo = cmBox.getCampo((String)comboBxItemBusca.getSelectedItem());
            System.out.println("Campo: "+campo);
            coletas = dao.selectCampo(campo, ComboBoxConteudo.getComparador((String)comboBxComparacaoBusca.getSelectedItem()),
                Double.parseDouble(txtBuscaValor.getText()), modoBusca, dt1, dt2);
            break;
        }

        //Busca
        System.out.println("Antes do loadData");
        if(coletas != null)
        {
            modelBuscColetas.loadData(coletas);
            this.coletasBusca = coletas;
        }
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnCadastrarDadosModFotActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCadastrarDadosModFotActionPerformed
        //Verifica se há campos nulos
        if(txtBoxArquivoModFot.getText().compareTo("")==0 ||
            txtBoxArquivoModCidade.getText().compareTo("")==0)
        {
            JOptionPane.showMessageDialog(null, "Preencha todos os campos!");
            return;
        }

        //Abrir tela loading
        /*Loading screenLoading = new Loading();
        screenLoading.setLocationRelativeTo(null);
        screenLoading.setVisible(true);
        screenLoading.setMesssage("Lendo o arquivo!");*/

        long startTime = System.currentTimeMillis();
        ArqvColeta arquivo = new xlsx(txtBoxArquivoModFot.getText());
        List<Coleta> coletas = new ArrayList<Coleta>();
        coletas = arquivo.ler(txtBoxArquivoModCidade.getText());

        if(coletas.isEmpty()){
            /*screenLoading.setProgress(100);
            screenLoading.setMesssage("Não há coletas a serem cadastradas!");
            */
            return;
        }
        else
        {
            /*screenLoading.setProgress(33);
            screenLoading.setMesssage("Busca por duplicatas!");*/
        }
        ColetaDAO dao = new ColetaDAO();

        //Verifica repeticoes
        List<List<Coleta>> listas = new ArrayList<List<Coleta>>(2);
        listas = dao.buscaRepetido(coletas, arquivo.getColunasExistentes());
        //screenLoading.setProgress(66);
        if(listas.get(0).size() != coletas.size())
        JOptionPane.showMessageDialog(null,"Há dados já cadastrados!\n");

        //Cadastro
        if(!listas.get(0).isEmpty())
        {
            //screenLoading.setMesssage("Inserindo novas coletas no banco");
            dao.create(listas.get(0), arquivo.getColunasExistentes());
        }
        //screenLoading.setProgress(83);
        if(!listas.get(1).isEmpty())
        {
            //screenLoading.setMesssage("Inserindo novos dados em coletas já cadastradas");
            dao.update(listas.get(1), arquivo.getColunasExistentes());
        }
        JOptionPane.showMessageDialog(null,"Dados cadastrados com sucesso!\nTempo total"+ (System.currentTimeMillis() - startTime));
        /*screenLoading.setProgress(100);
        screenLoading.setMesssage("Fim!");*/
        txtBoxArquivoCSV.setText(null);
    }//GEN-LAST:event_btnCadastrarDadosModFotActionPerformed

    private void btnBuscDadosModFotActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscDadosModFotActionPerformed
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Procurar Dados dos Módulos Fotovoltaicos");
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        FileNameExtensionFilter filtro = new FileNameExtensionFilter("Arquivo de texto (*.dat) ou xlsx", "dat", "xlsx");
        fileChooser.setFileFilter(filtro);
        if(fileChooser.showOpenDialog(this) == fileChooser.APPROVE_OPTION)
        {
            File arquivo = fileChooser.getSelectedFile();
            txtBoxArquivoModFot.setText(arquivo.getPath());
            txtBoxArquivoModFot.setEditable(false);
        }
    }//GEN-LAST:event_btnBuscDadosModFotActionPerformed

    private void btnCadastrarCadDadosAmbActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCadastrarCadDadosAmbActionPerformed

        System.out.println("Cadastrar Dados Ambientais");
        if(txtCadDadosAmbCidade.getText().compareTo("")==0 ||
            txtBoxArquivoCSV.getText().compareTo("")==0)
        {
            JOptionPane.showMessageDialog(null, "Preencha todos os campos!");
            return;
        }
        //Abrir tela loading
        /*Bar loading = new Bar();
        loading.setMessage("Lendo dados...");
        loading.setProgress(0);
        Thread t1 = new Thread(loading);
        t1.start();*/

        long startTime = System.currentTimeMillis();
        System.out.println("Vai criar aquivo");
        ArqvColeta arquivo = new dat(txtBoxArquivoCSV.getText());
        List<Coleta> coletas = null;
        System.out.println("Vai ler arquivo");
        coletas = arquivo.ler(txtCadDadosAmbCidade.getText());
        if (coletas == null)
            return;
        if (coletas.isEmpty())
            return;
        System.out.println("Coletas "+coletas.size());
        
        //Se não existir colunas no arquivo, acaba a tarefa
        if(arquivo.getColunasExistentes()==null||arquivo.getColunasExistentes().isEmpty()||
                (arquivo.getColunasExistentes().size()==1 && arquivo.getColunasExistentes().get(0).compareTo("Cidade")==0))
        {
            /*loading.setMessage("Não há colunas reconhecidas para cadastro!");
            loading.setProgress(100);*/
            return;
        }

        ColetaDAO dao = new ColetaDAO();

        //Verifica repeticoes
        int qtdColetas = coletas.size();
        long startTime2 = System.currentTimeMillis();

        List<List<Coleta>> listas = new ArrayList<List<Coleta>>(2);
        /*loading.setMessage("Procurando duplicatas!");
        loading.setProgress(33);
        loading.getProgress();*/
        listas = dao.buscaRepetido(coletas, arquivo.getColunasExistentes());
        System.out.println("Terminou de procurar duplicatas");
        
        /*int result = 0;
        if(qtdColetas != listas.get(0).size())
        {
            result = MessageBox.messageYesNO("Há dados já cadastrados!\nDeseja cancelar a operação?");
        }
        
        if(result == JOptionPane.YES_OPTION)
            return;*/
        //Cadastro
        System.out.println("Cadastro");
        /*loading.setMessage("Cadastrando novas coletas");
        loading.setProgress(66);
        loading.getProgress();*/
        dao.create(listas.get(0), arquivo.getColunasExistentes());
        /*loading.setMessage("Inserindo dados em coletas já existentes!");
        loading.setProgress(83);
        loading.getProgress();*/
        dao.update(listas.get(1), arquivo.getColunasExistentes());
        System.out.println("Fim update");
        /*loading.setMessage("Dados cadastrados com sucesso!\nTempo total"+ (System.currentTimeMillis() - startTime));
        loading.setProgress(100);
        loading.getProgress();*/
        txtBoxArquivoCSV.setText(null);
        System.out.println("Fim da operação");
    }//GEN-LAST:event_btnCadastrarCadDadosAmbActionPerformed

    private void btnCadastrarProcurarDadosAmbProcActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCadastrarProcurarDadosAmbProcActionPerformed
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Procurar Dados Ambientais");
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        FileNameExtensionFilter filtro = new FileNameExtensionFilter("Arquivo de texto (*.dat)", "dat");
        fileChooser.setFileFilter(filtro);
        if(fileChooser.showOpenDialog(this) == fileChooser.APPROVE_OPTION)
        {
            File arquivo = fileChooser.getSelectedFile();
            txtBoxArquivoCSV.setText(arquivo.getPath());
            txtBoxArquivoCSV.setEditable(false);
        }
    }//GEN-LAST:event_btnCadastrarProcurarDadosAmbProcActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        CadIndivCol telaCadIndivCol = new CadIndivCol();
        telaCadIndivCol.setLocationRelativeTo(null);
        telaCadIndivCol.setVisible(true);        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void btnCadastrarCadEspecActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCadastrarCadEspecActionPerformed
        if(txtCadEspecDt.getText().compareTo("  /  /    ")==0 || txtCadEspecHr.getText().compareTo("  :  :  ")==0
            || txtBoxArquivoEspect.getText().compareTo("")==0 || txtCadEspecCidade.getText().compareTo("")==0)
        {
            JOptionPane.showMessageDialog(null, "Há campos ainda não preenchidos!");
            return;
        }

        Espectro espec;
        EspectroDAO dao = new EspectroDAO();

        //Converte para Timestamp e armazena no espec
        ConversorData dtConv = null;
        Timestamp dataHora;

        try {
            //Abrir tela loading
            /*Loading screenLoading = new Loading();
            screenLoading.setLocationRelativeTo(null);
            screenLoading.setVisible(true);
            screenLoading.setMesssage("Lendo o arquivo!");*/

            dataHora = ConversorData.FormatarDataConvencionalHoraFinal(txtCadEspecDt.getText(), txtCadEspecHr.getText());
            espec = new Espectro();
            espec.setCurva(txtBoxArquivoEspect.getText());
            espec.setDataHora(dataHora);
            espec.setCidade(txtCadEspecCidade.getText());
            int qtdPontos = espec.getCurva().size();
            
            if(qtdPontos<1)
            {
                JOptionPane.showMessageDialog(null, "Não foram identificados interpolação para cadastro.");
                return;
            }

            /*screenLoading.setMesssage("Verificando duplicatas!");
            screenLoading.setProgress(33);*/

            //Retira, se houver, plotagens já cadastradas
            espec = dao.getNaoRepetidos(espec);

            if(espec.getCurva().size()!=qtdPontos)
            JOptionPane.showMessageDialog(null,"Há plotagem(s) repetidas!\n");

            //cadastra
            if(espec.getCurva().size()>0)
            {
                /*screenLoading.setMesssage("Cadastrando!");
                screenLoading.setProgress(66);*/
                dao.create(espec);
            }

            //procura se coleta já existe ou não
            ColetaDAO daoCol = new ColetaDAO();
            Coleta col = new Coleta(espec.getDataHora(), espec.getCidade());
            if(!daoCol.colJaCad(col))
            {
                System.out.println("Coleta ainda não cadastrada "+ col.getDataHora_col() +" "+ col.getCidade());
                String[] colExistentes = {"TIMESTAMP","Cidade"};
                daoCol.createIndiv(col, colExistentes);
            }

            /*screenLoading.setMesssage("Fim!");
            screenLoading.setProgress(100);*/

        } catch (ParseException ex) {
            Logger.getLogger(Painel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnCadastrarCadEspecActionPerformed

    private void btnCadastrarProcurarEspecActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCadastrarProcurarEspecActionPerformed
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Procurar espectro");
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        FileNameExtensionFilter filtro = new FileNameExtensionFilter("Arquivo de texto (*.txt, *.tit)", "txt", "tit");
        fileChooser.setCurrentDirectory(new File("C:\\Users\\Flávia Yumi\\Desktop\\Projetos\\IC INPE\\Arquivos\\Dados Flavia\\CP_ESPECTRO\\1006"));
        fileChooser.setFileFilter(filtro);
        if(fileChooser.showOpenDialog(this) == fileChooser.APPROVE_OPTION)
        {
            File arquivo = fileChooser.getSelectedFile();
            txtBoxArquivoEspect.setText(arquivo.getPath());
            txtBoxArquivoEspect.setEditable(false);
        }
    }//GEN-LAST:event_btnCadastrarProcurarEspecActionPerformed

    private void btnCadastrarDadosBDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCadastrarDadosBDActionPerformed
        long startTime = System.currentTimeMillis();
        csv arquivo = new csv(txtCadDadosBD.getText());
        List<Coleta> coletas = new ArrayList<Coleta>();
        coletas = arquivo.importData();
        ColetaDAO dao = new ColetaDAO();

        //Verifica repeticoes
        /*int qtdColetas = coletas.size();
        long startTime2 = System.currentTimeMillis();
        coletas = dao.buscaRepetido(coletas);
        JOptionPane.showMessageDialog(null,"Tempo de busca por repetidos: "+(System.currentTimeMillis() - startTime));

        if(qtdColetas != coletas.size())
        JOptionPane.showMessageDialog(null,"Há dados já cadastrados!\n");
        */
        //Cadastro

        if(!coletas.isEmpty())
        {
            dao.createBD(coletas);
            JOptionPane.showMessageDialog(null,"Dados cadastrados com sucesso!\nTempo total"+ (System.currentTimeMillis() - startTime));
        }
        else
        JOptionPane.showMessageDialog(null,"Não há dados a serem cadastrados!\nTempo total"+ (System.currentTimeMillis() - startTime));

        txtBoxArquivoCSV.setText(null);
    }//GEN-LAST:event_btnCadastrarDadosBDActionPerformed

    private void btnBuscArqvBDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscArqvBDActionPerformed
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Procurar Arquivo do Banco de Dados");
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        FileNameExtensionFilter filtro = new FileNameExtensionFilter("Arquivo de texto (*.csv)", "csv");
        fileChooser.setFileFilter(filtro);
        if(fileChooser.showOpenDialog(this) == fileChooser.APPROVE_OPTION)
        {
            File arquivo = fileChooser.getSelectedFile();
            txtCadDadosBD.setText(arquivo.getPath());
            txtCadDadosBD.setEditable(false);
        }
    }//GEN-LAST:event_btnBuscArqvBDActionPerformed

    private void btnGenProcurarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenProcurarActionPerformed
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Procurar Arquivo");
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        if(fileChooser.showOpenDialog(this) == fileChooser.APPROVE_OPTION)
        {
            File arquivo = fileChooser.getSelectedFile();
            txtGenArqv.setText(arquivo.getPath());
            txtGenArqv.setEditable(false);
        }
    }//GEN-LAST:event_btnGenProcurarActionPerformed

    private void btnCadastrarCadEspec1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCadastrarCadEspec1ActionPerformed
        System.out.println("Escolheu " + comboBoxGen.getSelectedItem());
        long startTime = System.currentTimeMillis();
        List<String> completeFields = new ArrayList<>();
        
        if(txtGenCidade.getText().compareTo("")==0)
            completeFields.add("Cidade");
        System.out.println("Cidade");
        if(txtGenHeader.getText().compareTo("")==0)
            completeFields.add("Cabeçalho");
        else if(Integer.parseInt(txtGenHeader.getText())<=0)
            completeFields.add("Cabeçalho");
        System.out.println("Cabeçalho");
        if(txtGenData.getText().compareTo("")==0)
            completeFields.add("Início Dados");
        else if(Integer.parseInt(txtGenData.getText())<=0 || Integer.parseInt(txtGenData.getText())<=Integer.parseInt(txtGenHeader.getText()))
            completeFields.add("Início Dados");
        System.out.println("Inicio Dados");
        if(String.valueOf(comboBoxGen.getSelectedItem()).compareTo("Coleta")==0)
        {
            //Verifica se campos preenchidos corretamente
            System.out.println("Coleta");
            if(txtGenArqv.getText().compareTo("")==0)
                completeFields.add("Arquivo");
            
            //Verifica o tipo de arquivo
            System.out.println("Verifica qual o tipo do arquivo");
            ArqvColeta arquivo = null;
            switch(FilenameUtils.getExtension(txtGenArqv.getText()))
            {
                case "xlsx":  
                    arquivo = new xlsx(txtGenArqv.getText());
                    break;
                default:
                    arquivo = new dat(txtGenArqv.getText());
                    if(txtGenDiv.getText().compareTo("")==0)
                        completeFields.add("Divisor de Dados");
                    break;
                
            }
            System.out.println("Monta o texto para completar");
            if(completeFields.isEmpty()==false)
            {
                System.out.println("Para para para");
                String completeFieldsMessage = new String();
                for(String s:completeFields)
                    completeFieldsMessage += s + '\n';
                JOptionPane.showMessageDialog(null, "Preencha o(s) campo(s) com valores válidos: \n"+completeFieldsMessage);
                return;
            }
            System.out.println("Não parou");
            
            
            List<Coleta> coletas = new ArrayList<>();
            if(arquivo != null)
                coletas = arquivo.lerGen(txtGenCidade.getText(), Integer.parseInt(txtGenHeader.getText()), Integer.parseInt(txtGenData.getText()), txtGenDiv.getText());
            else
                System.out.println("arquivo == null");
            
            if(coletas !=null && coletas.isEmpty() == false)
            {
                //Verifica repeticoes
                ColetaDAO dao = new ColetaDAO();
                List<List<Coleta>> listas = new ArrayList<List<Coleta>>(2);
                listas = dao.buscaRepetido(coletas, arquivo.getColunasExistentes());
                if(listas.get(0).size() != coletas.size())
                JOptionPane.showMessageDialog(null,"Há dados já cadastrados!\n");

                //Cadastro
                if(!listas.get(0).isEmpty())
                {
                    dao.create(listas.get(0), arquivo.getColunasExistentes());
                }
                if(!listas.get(1).isEmpty())
                {
                    dao.update(listas.get(1), arquivo.getColunasExistentes());
                }
                JOptionPane.showMessageDialog(null,"Dados cadastrados com sucesso!\nTempo total"+ (System.currentTimeMillis() - startTime));
                txtGenArqv.setText(null);
                txtGenCidade.setText(null);
                txtGenHeader.setText(null);
                txtGenData.setText(null);                
            }
        }
        else
        {
            //Verifica se campos preenchidos corretamente
            System.out.println("Espectro");
            if(txtGenArqv.getText().compareTo("")==0)
                completeFields.add("Arquivo");
            if(txtGenDt.getText().compareTo("  /  /    ")==0)
                completeFields.add("Data");
            if(txtGenHr.getText().compareTo("  :  :  ")==0)
                completeFields.add("Hora");
            
            if(completeFields.isEmpty()==false)
            {
                String completeFieldsMessage = new String();
                for(String s:completeFields)
                    completeFieldsMessage += s + '\n';
                JOptionPane.showMessageDialog(null, "Preencha o(s) campo(s): \n"+completeFieldsMessage);
                return;
            }
        }
        System.out.println("Fim da ação");
    }//GEN-LAST:event_btnCadastrarCadEspec1ActionPerformed

    private void comboBoxGenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxGenActionPerformed
        if(String.valueOf(comboBoxGen.getSelectedItem()).compareTo("Coleta")==0)
        {
            txtGenDt.setEditable(false);
            txtGenHr.setEditable(false);
            System.out.println("Coleta");
        }
        else
        {
            txtGenDt.setEditable(true);
            txtGenHr.setEditable(true);
            System.out.println("Espectro");
        }
    }//GEN-LAST:event_comboBoxGenActionPerformed

    private void btnBackupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackupActionPerformed
        CardLayout cartao = (CardLayout) Root.getLayout();
        System.out.println("Para Backup");
        cartao.show(Root, "Backup");         
    }//GEN-LAST:event_btnBackupActionPerformed

    private void btnBackupVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackupVoltarActionPerformed
        CardLayout cartao = (CardLayout) Root.getLayout();
        cartao.show(Root, "Menu");
    }//GEN-LAST:event_btnBackupVoltarActionPerformed

    private void btnBackupAcaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackupAcaoActionPerformed
        
        Enumeration rdBtns = btnGroupBackup.getElements();
        JRadioButton btn = null;
        while(rdBtns.hasMoreElements())
        {
            btn = (JRadioButton) rdBtns.nextElement();
            if(btn.isSelected())
                break;
        }
        
        if(btn != null)
        {
            Backup bkp = new Backup();
            if(btn.getActionCommand().compareTo("Download")==0)
            {
                System.out.println("Download");
                bkp.backup(txtBackupFile.getText(), txtBackupDump.getText());
            }
            else if(btn.getActionCommand().compareTo("Restaurar")==0)
            {
                System.out.println("Restaurar");
                Backup backup = new Backup();
                backup.restore(txtBackupFile.getText(),txtBackupDump.getText());
            }
            else
                JOptionPane.showMessageDialog(null, "Operação escolhida não é reconhecida!");
        }
    }//GEN-LAST:event_btnBackupAcaoActionPerformed

    private void btnBackupProcDump1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackupProcDump1ActionPerformed
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Arquivo de Backup");
        JTextField txtNomeSaida = null;
        if(rdBtnDownload.isSelected())
        {
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            fileChooser.setAcceptAllFileFilterUsed(false);
            
            txtNomeSaida = new JTextField();
            JLabel lblNomeArquivo = new JLabel("Nome do arquivo:");
            txtNomeSaida.setPreferredSize( new Dimension( 200, 24 ));

            JPanel accessory = new JPanel();
            accessory.setLayout(new FlowLayout());
            accessory.add(lblNomeArquivo);
            accessory.add(txtNomeSaida);
            fileChooser.setAccessory(accessory);
        }
        

        if(fileChooser.showOpenDialog(this) == fileChooser.APPROVE_OPTION)
        {
            File arquivo = fileChooser.getSelectedFile();
            if(rdBtnDownload.isSelected())
                txtBackupFile.setText(arquivo.getPath()+"\\\\"+txtNomeSaida.getText().replace("\\", "\\\\")+".backup");
            else
                txtBackupFile.setText(arquivo.getPath());
            txtBackupFile.setEditable(false);
        }

    }//GEN-LAST:event_btnBackupProcDump1ActionPerformed

    private void btnBackupProcDumpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackupProcDumpActionPerformed
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Dumper/Restore");

        if(fileChooser.showOpenDialog(this) == fileChooser.APPROVE_OPTION)
        {
            File arquivo = fileChooser.getSelectedFile();
            txtBackupFile.setText(arquivo.getPath());
            txtBackupFile.setEditable(false);
        }
    }//GEN-LAST:event_btnBackupProcDumpActionPerformed

    private void rdBtnDownloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdBtnDownloadActionPerformed
        txtBackupDump.setText("C:\\\\Program Files\\\\PostgreSQL\\\\10\\\\bin\\\\pg_dump.exe");
    }//GEN-LAST:event_rdBtnDownloadActionPerformed

    private void rdBtnRestaurarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdBtnRestaurarActionPerformed
        txtBackupDump.setText("C:\\\\Program Files\\\\PostgreSQL\\\\10\\\\bin\\\\pg_restore.exe");
    }//GEN-LAST:event_rdBtnRestaurarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Painel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Painel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Painel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Painel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Painel().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Backup;
    private javax.swing.JPanel Buscar;
    private javax.swing.JPanel Cadastrar;
    private javax.swing.JPanel CadastrarUsuario;
    private javax.swing.JPanel Login;
    private javax.swing.JPanel Menu;
    private javax.swing.JTabbedPane PanelCadDadosAmbientais;
    private javax.swing.JPanel Root;
    private javax.swing.JButton btnBackup;
    private javax.swing.JButton btnBackupAcao;
    private javax.swing.JButton btnBackupProcDump;
    private javax.swing.JButton btnBackupProcDump1;
    private javax.swing.JButton btnBackupVoltar;
    private javax.swing.JButton btnBuscArqvBD;
    private javax.swing.JButton btnBuscDadosModFot;
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnBuscarSelecionarTodos;
    private javax.swing.JButton btnBuscarVoltar;
    private javax.swing.JButton btnCadUsuCadastrar;
    private javax.swing.JButton btnCadUsuVoltar;
    private javax.swing.JButton btnCadVoltar;
    private javax.swing.JButton btnCadastrarCadDadosAmb;
    private javax.swing.JButton btnCadastrarCadEspec;
    private javax.swing.JButton btnCadastrarCadEspec1;
    private javax.swing.JButton btnCadastrarDadosBD;
    private javax.swing.JButton btnCadastrarDadosModFot;
    private javax.swing.JButton btnCadastrarProcurarDadosAmbProc;
    private javax.swing.JButton btnCadastrarProcurarEspec;
    private javax.swing.JButton btnDownload;
    private javax.swing.JButton btnGenProcurar;
    private javax.swing.ButtonGroup btnGroupBackup;
    private javax.swing.JButton btnLoginCad;
    private javax.swing.JButton btnLoginLogin;
    private javax.swing.JButton btnLoginVoltar;
    private javax.swing.JButton btnMenuLogin;
    private javax.swing.JButton btnMenuSair;
    private javax.swing.JComboBox<String> comboBoxGen;
    private javax.swing.JComboBox<String> comboBxComparacaoBusca;
    private javax.swing.JComboBox<String> comboBxItemBusca;
    private javax.swing.JComboBox<String> comboBxValidacao;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton5;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jtBusca;
    private java.awt.Label label1;
    private java.awt.Label label2;
    private java.awt.Label label3;
    private javax.swing.JRadioButton rdBtnDownload;
    private javax.swing.JRadioButton rdBtnRestaurar;
    private javax.swing.JTextField txtBackupDump;
    private javax.swing.JTextField txtBackupFile;
    private javax.swing.JTextField txtBoxArquivoCSV;
    private javax.swing.JTextField txtBoxArquivoEspect;
    private javax.swing.JTextField txtBoxArquivoModCidade;
    private javax.swing.JTextField txtBoxArquivoModFot;
    private javax.swing.JFormattedTextField txtBuscDt;
    private javax.swing.JFormattedTextField txtBuscDt2;
    private javax.swing.JFormattedTextField txtBuscHr;
    private javax.swing.JFormattedTextField txtBuscHr2;
    private javax.swing.JTextField txtBuscaValor;
    private javax.swing.JTextField txtCadDadosAmbCidade;
    private javax.swing.JTextField txtCadDadosBD;
    private javax.swing.JTextField txtCadEspecCidade;
    private javax.swing.JFormattedTextField txtCadEspecDt;
    private javax.swing.JFormattedTextField txtCadEspecHr;
    private javax.swing.JTextField txtCadUsuNome;
    private javax.swing.JPasswordField txtCadUsuSenha;
    private javax.swing.JPasswordField txtCadUsuSenha2;
    private javax.swing.JTextField txtCadUsuUsuario;
    private javax.swing.JTextField txtGenArqv;
    private javax.swing.JTextField txtGenCidade;
    private javax.swing.JFormattedTextField txtGenData;
    private javax.swing.JTextField txtGenDiv;
    private javax.swing.JFormattedTextField txtGenDt;
    private javax.swing.JFormattedTextField txtGenHeader;
    private javax.swing.JFormattedTextField txtGenHr;
    private javax.swing.JPasswordField txtLoginSenha;
    private javax.swing.JTextField txtLoginUsu;
    // End of variables declaration//GEN-END:variables

    private void swicth(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
