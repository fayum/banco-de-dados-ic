/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;

/**
 *
 * @author Flávia Yumi
 */
public class Loading extends javax.swing.JFrame {
    
    private Timer t;
    private ActionListener al;
    private int progress;
    private String message;
    
    public void setProgress(int value)
    {
        this.progress = value;
    }
    
    public void setMesssage(String message)
    {
        this.message = message;
    }
        
    public Loading() {
        System.out.println("Criou a janela de loading");
        initComponents();
        progress = 0;
        message = "";
        this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
                
        al = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(jProgressBar1.getValue() < progress)
                {
                    lblMensagem.setText(message);                    
                    jProgressBar1.setValue(jProgressBar1.getValue()+1);
                }
                else
                {
                    lblMensagem.setText(message);
                    t.stop();
                }
            }
        };
        System.out.println("Fim da janela de loading");
        t = new Timer(50, al);
        t.start();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lblMensagem = new java.awt.Label();
        jProgressBar1 = new javax.swing.JProgressBar();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblMensagem.setAlignment(java.awt.Label.CENTER);
        jPanel1.add(lblMensagem, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 40, 300, 52));

        jProgressBar1.setForeground(new java.awt.Color(255, 102, 0));
        jProgressBar1.setOpaque(true);
        jPanel1.add(jProgressBar1, new org.netbeans.lib.awtextra.AbsoluteConstraints(78, 117, 300, 22));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 451, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Loading().setVisible(true);
            }
        });
        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JProgressBar jProgressBar1;
    private java.awt.Label lblMensagem;
    // End of variables declaration//GEN-END:variables
}
