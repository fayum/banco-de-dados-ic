/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package views;

import model.bean.Coleta;
import common.ConversorData;
import java.awt.TextField;
import java.io.File;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.CheckBox;
import javafx.scene.control.RadioButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;
import model.bean.Espectro;
import model.dao.ColetaDAO;
import model.dao.EspectroDAO;

/**
 *
 * @author Valdete
 */
public class CadIndivCol extends javax.swing.JFrame {

    private List<String> camposExistentes;

    public List<String> getCamposExistentes() {
        return camposExistentes;
    }

    public void setCamposExistentes(List<String> camposExistentes) {
        this.camposExistentes = camposExistentes;
    }
    /** Creates new form CadIndivEspec */
    public CadIndivCol() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        txtHr = new javax.swing.JFormattedTextField();
        txtIrradGlobHorizontal = new javax.swing.JTextField();
        txtDesvGlobHorizontal = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtDesvDirNormal = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtIrradDirNormal = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtRadDif = new javax.swing.JTextField();
        txtTempAr = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtUmidAr = new javax.swing.JTextField();
        txtCadIndivCidade = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtCCaSi = new javax.swing.JTextField();
        txtDesvCCaSi = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txtDt = new javax.swing.JFormattedTextField();
        jLabel13 = new javax.swing.JLabel();
        txtCCmicSi = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        txtDesvCCmicSi = new javax.swing.JTextField();
        txtCCpSi = new javax.swing.JTextField();
        txtDesvCCpSi = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        txtCCmoSi = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        txtDesvCCmoSi = new javax.swing.JTextField();
        txtDesvCCGaAs = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        txtCCGaAs = new javax.swing.JTextField();
        txtCCTJ = new javax.swing.JTextField();
        txtDesvCCTJ = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        txtTempaSi = new javax.swing.JTextField();
        txtTempmicSi = new javax.swing.JTextField();
        txtTemppSi = new javax.swing.JTextField();
        txtTempmoSi = new javax.swing.JTextField();
        txtTempGaAs = new javax.swing.JTextField();
        txtTempTJ = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        jLabel37 = new javax.swing.JLabel();
        txtDesvTempaSi = new javax.swing.JTextField();
        txtDesvTempmicSi = new javax.swing.JTextField();
        txtDesvTemppSi = new javax.swing.JTextField();
        txtDesvTempmoSi = new javax.swing.JTextField();
        txtDesvTempGaAs = new javax.swing.JTextField();
        txtDesvTempTJ = new javax.swing.JTextField();
        txtIrradPir = new javax.swing.JTextField();
        jLabel38 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        txtDesvIrradPir = new javax.swing.JTextField();
        txtDesvIrradFoto = new javax.swing.JTextField();
        jLabel40 = new javax.swing.JLabel();
        txtIrradFoto = new javax.swing.JTextField();
        jLabel41 = new javax.swing.JLabel();
        chkBValidacao = new javax.swing.JCheckBox();
        jButton2 = new javax.swing.JButton();
        txtDesvDifusa = new javax.swing.JTextField();
        jLabel42 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtPressAr = new javax.swing.JTextField();
        txtCadIndivEspec = new javax.swing.JTextField();
        jLabel44 = new javax.swing.JLabel();
        btnBuscArqEspecCadIndiv = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(51, 51, 51));

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        try {
            txtHr.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##:##:##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtHr.setText("00:00:00");
        txtHr.setToolTipText("");
        jPanel1.add(txtHr, new org.netbeans.lib.awtextra.AbsoluteConstraints(543, 129, 100, -1));
        jPanel1.add(txtIrradGlobHorizontal, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 167, 100, -1));
        jPanel1.add(txtDesvGlobHorizontal, new org.netbeans.lib.awtextra.AbsoluteConstraints(543, 167, 100, -1));

        jLabel3.setText("Data:");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(125, 132, -1, -1));

        jLabel4.setText("Hora:");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(498, 132, -1, -1));

        jLabel1.setText("Irradiância Global Horizontal:");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(13, 170, -1, -1));

        jLabel5.setText("Desvio Padrão Irradiância Global Horizontal:");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 170, -1, -1));
        jPanel1.add(txtDesvDirNormal, new org.netbeans.lib.awtextra.AbsoluteConstraints(543, 193, 100, -1));

        jLabel6.setText("Desvio Padrão Irradiância Direta:");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 196, -1, -1));
        jPanel1.add(txtIrradDirNormal, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 193, 100, -1));

        jLabel7.setText("Irradiância Direta:");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(63, 196, -1, -1));
        jPanel1.add(txtRadDif, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 219, 100, -1));
        jPanel1.add(txtTempAr, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 270, 100, -1));

        jLabel8.setText("Radiação Difusa:");
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 222, -1, -1));

        jLabel9.setText("Temperatura Ar:");
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 270, -1, -1));
        jPanel1.add(txtUmidAr, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 245, 100, -1));
        jPanel1.add(txtCadIndivCidade, new org.netbeans.lib.awtextra.AbsoluteConstraints(543, 270, 100, -1));

        jLabel10.setText("Umidade Ar:");
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(92, 248, -1, -1));

        jLabel11.setText("Pressão Ar:");
        jPanel1.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 248, -1, -1));
        jPanel1.add(txtCCaSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 360, 100, -1));
        jPanel1.add(txtDesvCCaSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 360, 100, -1));

        jLabel12.setText("aSi:");
        jPanel1.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 363, -1, -1));

        try {
            txtDt.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtDt.setText("00/00/0000");
        jPanel1.add(txtDt, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 129, 100, -1));

        jLabel13.setText("Desvio Padrão aSi:");
        jPanel1.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 363, -1, -1));
        jPanel1.add(txtCCmicSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 385, 100, -1));

        jLabel14.setText("micSi:");
        jPanel1.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 388, -1, -1));

        jLabel15.setText("Desvio Padrão micSi:");
        jPanel1.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 388, -1, -1));
        jPanel1.add(txtDesvCCmicSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 385, 100, -1));
        jPanel1.add(txtCCpSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 410, 100, -1));
        jPanel1.add(txtDesvCCpSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 410, 100, -1));

        jLabel16.setText("pSi");
        jPanel1.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 413, -1, -1));

        jLabel17.setText("Desvio Padrão pSi:");
        jPanel1.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 413, -1, -1));

        jLabel18.setText("Desvio Padrão moSi:");
        jPanel1.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 438, -1, -1));
        jPanel1.add(txtCCmoSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 435, 100, -1));

        jLabel19.setText("moSi:");
        jPanel1.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 438, -1, -1));
        jPanel1.add(txtDesvCCmoSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 435, 100, -1));
        jPanel1.add(txtDesvCCGaAs, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 460, 100, -1));

        jLabel20.setText("GaAs:");
        jPanel1.add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 463, -1, -1));

        jLabel21.setText("Desvio Padrão GaAsi:");
        jPanel1.add(jLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 463, -1, -1));
        jPanel1.add(txtCCGaAs, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 460, 100, -1));
        jPanel1.add(txtCCTJ, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 485, 100, -1));
        jPanel1.add(txtDesvCCTJ, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 485, 100, -1));

        jLabel22.setText("TJ:");
        jPanel1.add(jLabel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 488, -1, -1));

        jLabel23.setText("Desvio Padrão TJi:");
        jPanel1.add(jLabel23, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 488, -1, -1));

        jLabel24.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel24.setText("Cadastro de Coleta");
        jPanel1.add(jLabel24, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 40, -1, -1));

        jLabel25.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel25.setText("Temperatura Módulo Solar");
        jPanel1.add(jLabel25, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 540, -1, -1));

        jLabel26.setText("aSi:");
        jPanel1.add(jLabel26, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 573, -1, -1));

        jLabel27.setText("micSi:");
        jPanel1.add(jLabel27, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 598, -1, -1));

        jLabel28.setText("pSi");
        jPanel1.add(jLabel28, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 623, -1, -1));

        jLabel29.setText("moSi:");
        jPanel1.add(jLabel29, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 648, -1, -1));

        jLabel30.setText("GaAs:");
        jPanel1.add(jLabel30, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 673, -1, -1));

        jLabel31.setText("TJ:");
        jPanel1.add(jLabel31, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 698, -1, -1));
        jPanel1.add(txtTempaSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 570, 100, -1));
        jPanel1.add(txtTempmicSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 595, 100, -1));
        jPanel1.add(txtTemppSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 620, 100, -1));
        jPanel1.add(txtTempmoSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 645, 100, -1));
        jPanel1.add(txtTempGaAs, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 670, 100, -1));
        jPanel1.add(txtTempTJ, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 695, 100, -1));

        jLabel32.setText("Desvio Padrão aSi:");
        jPanel1.add(jLabel32, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 573, -1, -1));

        jLabel33.setText("Desvio Padrão micSi:");
        jPanel1.add(jLabel33, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 598, -1, -1));

        jLabel34.setText("Desvio Padrão moSi:");
        jPanel1.add(jLabel34, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 648, -1, -1));

        jLabel35.setText("Desvio Padrão pSi:");
        jPanel1.add(jLabel35, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 623, -1, -1));

        jLabel36.setText("Desvio Padrão GaAsi:");
        jPanel1.add(jLabel36, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 673, -1, -1));

        jLabel37.setText("Desvio Padrão TJi:");
        jPanel1.add(jLabel37, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 698, -1, -1));
        jPanel1.add(txtDesvTempaSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 570, 100, -1));
        jPanel1.add(txtDesvTempmicSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 595, 100, -1));
        jPanel1.add(txtDesvTemppSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 620, 100, -1));
        jPanel1.add(txtDesvTempmoSi, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 645, 100, -1));
        jPanel1.add(txtDesvTempGaAs, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 670, 100, -1));
        jPanel1.add(txtDesvTempTJ, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 695, 100, -1));
        jPanel1.add(txtIrradPir, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 770, 100, -1));

        jLabel38.setText("Irradiância Piranômetro:");
        jPanel1.add(jLabel38, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 780, -1, -1));

        jLabel39.setText("Desvio Padrão Irradiância Piranômetro:");
        jPanel1.add(jLabel39, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 780, -1, -1));
        jPanel1.add(txtDesvIrradPir, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 770, 100, -1));
        jPanel1.add(txtDesvIrradFoto, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 800, 100, -1));

        jLabel40.setText("Desvio Padrão Irradiância Fotodiodo:");
        jPanel1.add(jLabel40, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 800, -1, -1));
        jPanel1.add(txtIrradFoto, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 800, 100, -1));

        jLabel41.setText("Irradiância Fotodiodo:");
        jPanel1.add(jLabel41, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 800, -1, -1));

        chkBValidacao.setText("Validação");
        jPanel1.add(chkBValidacao, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 830, -1, -1));

        jButton2.setText("Cadastrar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 880, -1, -1));
        jPanel1.add(txtDesvDifusa, new org.netbeans.lib.awtextra.AbsoluteConstraints(543, 220, 100, -1));

        jLabel42.setText("Desvio Padrão Radiação Difusa:");
        jPanel1.add(jLabel42, new org.netbeans.lib.awtextra.AbsoluteConstraints(363, 223, -1, -1));

        jLabel43.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel43.setText("Corrente de Curto Circuito");
        jPanel1.add(jLabel43, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 330, -1, -1));

        jLabel2.setText("Cidade:");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 270, -1, -1));
        jPanel1.add(txtPressAr, new org.netbeans.lib.awtextra.AbsoluteConstraints(543, 245, 100, -1));
        jPanel1.add(txtCadIndivEspec, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 880, 220, -1));

        jLabel44.setText("Espectro:");
        jPanel1.add(jLabel44, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 880, -1, -1));

        btnBuscArqEspecCadIndiv.setText("Procurar");
        btnBuscArqEspecCadIndiv.setToolTipText("");
        btnBuscArqEspecCadIndiv.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscArqEspecCadIndivActionPerformed(evt);
            }
        });
        jPanel1.add(btnBuscArqEspecCadIndiv, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 880, -1, -1));

        jScrollPane1.setViewportView(jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 807, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 632, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public void verificaCampoNaoNuloDouble(JTextField txt, Consumer<Double> metodo, String campo)
    {
        if(!txt.getText().isEmpty())
        {
            this.getCamposExistentes().add(campo);
            metodo.accept(Double.parseDouble(txt.getText().replace(',', '.')));
        }
    }
    public void verificaCampoNaoNuloString(JTextField txt, Consumer<String> metodo, String campo)
    {
        if(!txt.getText().isEmpty())
        {
            this.getCamposExistentes().add(campo);
            metodo.accept(txt.getText());
        }
    }
    public void verificaCampoNaoNuloBoolean(CheckBox ckb, Consumer<Boolean> metodo, String campo)
    {
        this.getCamposExistentes().add(campo);
        metodo.accept(ckb.isPressed());
    }
 
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
    if(txtDt.getText().compareTo("  /  /    ")==0 || txtHr.getText().compareTo("  :  :  ")==0
                || txtCadIndivCidade.getText().compareTo("")==0)
        {
            JOptionPane.showMessageDialog(null, "Data, hora ou cidade ainda não preenchidos!");
            return;
        }
        Coleta col = new Coleta();
        this.setCamposExistentes(new ArrayList<String>());
        try {
            System.out.println("Inicio try");
            //Converte para Timestamp e armazena dados em col
            ConversorData dtConv = new ConversorData();
            col.setDataHora_col(dtConv.FormatarDataConvencionalHoraFinal(txtDt.getText(), txtHr.getText()));
            System.out.println("Converteu a data");
            col.setCidade(txtCadIndivCidade.getText());
            col.setValidacao(chkBValidacao.isSelected());
            System.out.println("Coleta: "+col.getCidade()+" "+col.isValidacao());
            this.getCamposExistentes().add("Cidade");
            this.getCamposExistentes().add("TIMESTAMP");
            
            System.out.println("Começa Verifica campo nulo");
            
            this.verificaCampoNaoNuloDouble(txtIrradGlobHorizontal, col::setIrradGlobHorizontal, "Global_Avg");
            this.verificaCampoNaoNuloDouble(txtDesvGlobHorizontal, col::setDesvGlobHorizontal, "Desvio Padrão da Irradiância Global Horizontal");
            this.verificaCampoNaoNuloDouble(txtIrradDirNormal, col::setIrradDirNormal, "Direta_Avg");
            this.verificaCampoNaoNuloDouble(txtDesvDirNormal, col::setDesvDirNormal, "Desvio Padrão DirNormal");
            this.verificaCampoNaoNuloDouble(txtRadDif, col::setRadiacaoDifusa, "Difusa_Avg");
            this.verificaCampoNaoNuloDouble(txtDesvDifusa, col::setDesvDifusa, "Desvio Padrão Difusa");
            this.verificaCampoNaoNuloDouble(txtTempAr, col::setTempAr, "Temp_ar");
            this.verificaCampoNaoNuloDouble(txtUmidAr, col::setUmidAr, "Umid_ar");
            this.verificaCampoNaoNuloDouble(txtPressAr, col::setPressAr, "Press");
            System.out.println("Parte 1");
            this.verificaCampoNaoNuloDouble(txtCCaSi, col::setCorrCC_aSi, "I_Amorfo_Avg");
            this.verificaCampoNaoNuloDouble(txtCCmicSi, col::setCorrCC_micSi, "I_Micro_Avg");
            this.verificaCampoNaoNuloDouble(txtCCpSi, col::setCorrCC_pSi, "I_Poli_Avg");
            this.verificaCampoNaoNuloDouble(txtCCmoSi, col::setCorrCC_moSi, "I_Mono_Avg");
            this.verificaCampoNaoNuloDouble(txtCCGaAs, col::setCorrCC_GaAs, "I_GaAs_Avg");
            this.verificaCampoNaoNuloDouble(txtCCTJ, col::setCorrCC_TJ, "I_TMJ_Avg");
            System.out.println("Parte 2");
            this.verificaCampoNaoNuloDouble(txtDesvCCaSi, col::setDesvCorrCC_aSi, "I_Amorfo_Std");
            this.verificaCampoNaoNuloDouble(txtDesvCCmicSi, col::setDesvCorrCC_micSi, "I_Micro_Std");
            this.verificaCampoNaoNuloDouble(txtDesvCCpSi, col::setDesvCorrCC_pSi, "I_Poli_Std");
            this.verificaCampoNaoNuloDouble(txtDesvCCmoSi, col::setDesvCorrCC_moSi, "I_Mono_Std");
            this.verificaCampoNaoNuloDouble(txtDesvCCGaAs, col::setDesvCorrCC_GaAs, "I_GaAs_Std");
            this.verificaCampoNaoNuloDouble(txtDesvCCTJ, col::setDesvCorrCC_TJ, "I_TMJ_Std");
            System.out.println("Parte 3");
            this.verificaCampoNaoNuloDouble(txtTempaSi, col::setTempMod_aSi, "T_Amorfo_Avg");
            this.verificaCampoNaoNuloDouble(txtTempmicSi, col::setTempMod_micSi, "T_Micro_Avg");
            this.verificaCampoNaoNuloDouble(txtTemppSi, col::setTempMod_pSi, "T_Poli_Avg");
            this.verificaCampoNaoNuloDouble(txtTempmoSi, col::setTempMod_moSi, "T_Mono_Avg");
            this.verificaCampoNaoNuloDouble(txtTempGaAs, col::setTempMod_GaAs, "T_GaAs_Avg");
            this.verificaCampoNaoNuloDouble(txtTempTJ, col::setTempMod_TJ, "T_TMJ_Avg");
            System.out.println("Parte 4");
            this.verificaCampoNaoNuloDouble(txtDesvTempaSi, col::setDesvTempMod_aSi, "T_Amorfo_Std");
            this.verificaCampoNaoNuloDouble(txtDesvTempmicSi, col::setDesvTempMod_micSi, "T_Micro_Std");
            this.verificaCampoNaoNuloDouble(txtDesvTemppSi, col::setDesvTempMod_pSi, "T_Poli_Std");
            this.verificaCampoNaoNuloDouble(txtDesvTempmoSi, col::setDesvTempMod_moSi, "T_Mono_Std");
            this.verificaCampoNaoNuloDouble(txtDesvTempGaAs, col::setDesvTempMod_GaAs, "T_GaAs_Std");
            this.verificaCampoNaoNuloDouble(txtDesvTempTJ, col::setDesvTempMod_TJ, "T_TMJ_Std");
            System.out.println("Parte 5");
            this.verificaCampoNaoNuloDouble(txtIrradPir, col::setIrrPiranômetro, "Irradiância do Piranômetro");
            this.verificaCampoNaoNuloDouble(txtDesvIrradPir, col::setDesvIrrPiranômetro, "Desvio Padrão da Irradiância do Piranômetro");
            this.verificaCampoNaoNuloDouble(txtIrradFoto, col::setIrrFotodiodo, "Irradiância do Fotodiodo");
            this.verificaCampoNaoNuloDouble(txtDesvIrradFoto, col::setDesvIrrFotodiodo, "Desvio Padrão da Irradiância do Fotodiodo");
       
            System.out.println("DAO");
            System.out.println("Coleta: "+ col.getDesvGlobHorizontal()+" "+ col.getDesvDifusa());
            ColetaDAO dao = new ColetaDAO();
            dao.createIndiv(col, this.getCamposExistentes().toArray(new String[this.getCamposExistentes().size()]));
            System.out.println("Cadastrou coleta, verificará se há espectro");
            
            //Se possuir arquivo de espectro
            if(txtCadIndivEspec.getText().compareTo("")!=0){
                
                EspectroDAO especDao = new EspectroDAO();
                Espectro espec = new Espectro();
                espec.setCurva(txtCadIndivEspec.getText());
                espec.setDataHora(col.getDataHora_col());
                espec.setCidade(txtCadIndivCidade.getText());

                int qtdPontos = espec.getCurva().size();

                //Retira, se houver, plotagens já cadastradas
                espec = especDao.getNaoRepetidos(espec);

                if(espec.getCurva().size()!=qtdPontos)
                    JOptionPane.showMessageDialog(null,"Há plotagem(s) repetidas!\n");
                //cadastra
                especDao.create(espec);
            }

            //Limpa conteúdo das textBoxes
            txtDt.setText("00/00/0000");
            txtHr.setText("00:00:00");
            txtIrradGlobHorizontal.setText("");
            txtDesvGlobHorizontal.setText("");
            txtIrradDirNormal.setText("");
            txtDesvDirNormal.setText("");
            txtRadDif.setText("");
            txtDesvDifusa.setText("");
            txtTempAr.setText("");
            txtUmidAr.setText("");
            txtPressAr.setText("");

            txtCCaSi.setText("");
            txtCCmicSi.setText("");
            txtCCpSi.setText("");
            txtCCmoSi.setText("");
            txtCCGaAs.setText("");
            txtCCTJ.setText("");

            txtDesvCCaSi.setText("");
            txtDesvCCmicSi.setText("");
            txtDesvCCpSi.setText("");
            txtDesvCCmoSi.setText("");
            txtDesvCCGaAs.setText("");
            txtDesvCCTJ.setText("");

            txtTempaSi.setText("");
            txtTempmicSi.setText("");
            txtTemppSi.setText("");
            txtTempmoSi.setText("");
            txtTempGaAs.setText("");
            txtTempTJ.setText("");

            txtDesvTempaSi.setText("");
            txtDesvTempmicSi.setText("");
            txtDesvTemppSi.setText("");
            txtDesvTempmoSi.setText("");
            txtDesvTempGaAs.setText("");
            txtDesvTempTJ.setText("");

            txtIrradPir.setText("");
            txtDesvIrradPir.setText("");
            txtIrradFoto.setText("");
            txtDesvIrradFoto.setText("");
            chkBValidacao.setSelected(false);
            txtCadIndivEspec.setText("");
            txtCadIndivCidade.setText("");

        } catch (ParseException ex) {
            Logger.getLogger(CadIndivCol.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void btnBuscArqEspecCadIndivActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscArqEspecCadIndivActionPerformed
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Procurar espectro");
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        FileNameExtensionFilter filtro = new FileNameExtensionFilter("Arquivo de texto (*.txt)", "txt");
        fileChooser.setFileFilter(filtro);
        if(fileChooser.showOpenDialog(this) == fileChooser.APPROVE_OPTION)
        {
            File arquivo = fileChooser.getSelectedFile();
            txtCadIndivEspec.setText(arquivo.getPath());
            txtCadIndivEspec.setEditable(false);
        }
    }//GEN-LAST:event_btnBuscArqEspecCadIndivActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CadIndivCol.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CadIndivCol.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CadIndivCol.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CadIndivCol.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CadIndivCol().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscArqEspecCadIndiv;
    private javax.swing.JCheckBox chkBValidacao;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField txtCCGaAs;
    private javax.swing.JTextField txtCCTJ;
    private javax.swing.JTextField txtCCaSi;
    private javax.swing.JTextField txtCCmicSi;
    private javax.swing.JTextField txtCCmoSi;
    private javax.swing.JTextField txtCCpSi;
    private javax.swing.JTextField txtCadIndivCidade;
    private javax.swing.JTextField txtCadIndivEspec;
    private javax.swing.JTextField txtDesvCCGaAs;
    private javax.swing.JTextField txtDesvCCTJ;
    private javax.swing.JTextField txtDesvCCaSi;
    private javax.swing.JTextField txtDesvCCmicSi;
    private javax.swing.JTextField txtDesvCCmoSi;
    private javax.swing.JTextField txtDesvCCpSi;
    private javax.swing.JTextField txtDesvDifusa;
    private javax.swing.JTextField txtDesvDirNormal;
    private javax.swing.JTextField txtDesvGlobHorizontal;
    private javax.swing.JTextField txtDesvIrradFoto;
    private javax.swing.JTextField txtDesvIrradPir;
    private javax.swing.JTextField txtDesvTempGaAs;
    private javax.swing.JTextField txtDesvTempTJ;
    private javax.swing.JTextField txtDesvTempaSi;
    private javax.swing.JTextField txtDesvTempmicSi;
    private javax.swing.JTextField txtDesvTempmoSi;
    private javax.swing.JTextField txtDesvTemppSi;
    private javax.swing.JFormattedTextField txtDt;
    private javax.swing.JFormattedTextField txtHr;
    private javax.swing.JTextField txtIrradDirNormal;
    private javax.swing.JTextField txtIrradFoto;
    private javax.swing.JTextField txtIrradGlobHorizontal;
    private javax.swing.JTextField txtIrradPir;
    private javax.swing.JTextField txtPressAr;
    private javax.swing.JTextField txtRadDif;
    private javax.swing.JTextField txtTempAr;
    private javax.swing.JTextField txtTempGaAs;
    private javax.swing.JTextField txtTempTJ;
    private javax.swing.JTextField txtTempaSi;
    private javax.swing.JTextField txtTempmicSi;
    private javax.swing.JTextField txtTempmoSi;
    private javax.swing.JTextField txtTemppSi;
    private javax.swing.JTextField txtUmidAr;
    // End of variables declaration//GEN-END:variables

}
