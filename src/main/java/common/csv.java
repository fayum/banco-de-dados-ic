/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import com.opencsv.CSVWriter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import model.bean.Coleta;
import model.dao.AlteracaoDAO;

public class csv {
    
    private String caminho, nome;
    private String[] colunasInexistentes;
    private String[] colunasExistentes;
    
    public csv(String path, String name)
    {
        caminho = path;
        nome = name;
        colunasInexistentes = null;
        colunasExistentes = null;
    }
    
    public csv(String path)
    {
        caminho = path;
        colunasInexistentes = null;
        colunasExistentes = null;
    }
    
    public List<Coleta> importData()
    {
        BufferedReader br = null;        
        String linha = "";
        String datDivisor = ",";
                
        List<Coleta> coletas = new ArrayList<Coleta>();
        Coleta col;
        try {
            br = new BufferedReader(new FileReader(caminho));
            
            //Para começar inserir a partir da segunda linha
            br.readLine();
            while ((linha = br.readLine()) != null ) {
                String[] dados = linha.split(datDivisor);
                col = new Coleta();
                
                try {
                    col.setDataHora_col(ConversorData.FormatarDataConvertidaHoraFinal(dados[0].substring(1, 11), dados[0].substring(dados[0].length()-11, dados[0].length()-3)));
                } catch (ParseException ex) {
                    Logger.getLogger(csv.class.getName()).log(Level.SEVERE, null, ex);
                }
                col.setIrradGlobHorizontal(Double.parseDouble(dados[1].substring(1, dados[1].length()-1)));
                col.setDesvGlobHorizontal(Double.parseDouble(dados[2].substring(1, dados[2].length()-1)));
                col.setIrradDirNormal(Double.parseDouble(dados[3].substring(1, dados[3].length()-1))); 
                col.setDesvDirNormal(Double.parseDouble(dados[4].substring(1, dados[4].length()-1)));
                col.setRadiacaoDifusa(Double.parseDouble(dados[5].substring(1, dados[5].length()-1))); 
                col.setDesvDifusa(Double.parseDouble(dados[6].substring(1, dados[6].length()-1)));
                col.setTempAr(Double.parseDouble(dados[7].substring(1, dados[7].length()-1))); 
                col.setPressAr(Double.parseDouble(dados[8].substring(1, dados[8].length()-1)));
                col.setUmidAr(Double.parseDouble(dados[9].substring(1, dados[9].length()-1))); 
                
                col.setCorrCC_GaAs(Double.parseDouble(dados[10].substring(1, dados[10].length()-1)));
                col.setDesvCorrCC_GaAs(Double.parseDouble(dados[11].substring(1, dados[11].length()-1)));
                col.setCorrCC_TJ(Double.parseDouble(dados[12].substring(1, dados[12].length()-1)));
                col.setDesvCorrCC_TJ(Double.parseDouble(dados[13].substring(1, dados[13].length()-1)));
                col.setCorrCC_aSi(Double.parseDouble(dados[14].substring(1, dados[14].length()-1)));
                col.setDesvCorrCC_aSi(Double.parseDouble(dados[15].substring(1, dados[15].length()-1)));
                col.setCorrCC_micSi(Double.parseDouble(dados[16].substring(1, dados[16].length()-1)));
                col.setDesvCorrCC_micSi(Double.parseDouble(dados[17].substring(1, dados[17].length()-1)));
                col.setCorrCC_moSi(Double.parseDouble(dados[18].substring(1, dados[18].length()-1)));
                col.setDesvCorrCC_moSi(Double.parseDouble(dados[19].substring(1, dados[19].length()-1)));
                col.setCorrCC_pSi(Double.parseDouble(dados[20].substring(1, dados[20].length()-1)));
                col.setDesvCorrCC_pSi(Double.parseDouble(dados[21].substring(1, dados[21].length()-1)));
                
                col.setTempMod_GaAs(Double.parseDouble(dados[22].substring(1, dados[22].length()-1)));
                col.setDesvTempMod_GaAs(Double.parseDouble(dados[23].substring(1, dados[23].length()-1)));
                col.setTempMod_TJ(Double.parseDouble(dados[24].substring(1, dados[24].length()-1)));
                col.setDesvTempMod_TJ(Double.parseDouble(dados[25].substring(1, dados[25].length()-1)));
                col.setTempMod_aSi(Double.parseDouble(dados[26].substring(1, dados[26].length()-1)));
                col.setDesvTempMod_aSi(Double.parseDouble(dados[27].substring(1, dados[27].length()-1)));
                col.setTempMod_micSi(Double.parseDouble(dados[28].substring(1, dados[28].length()-1)));
                col.setDesvTempMod_micSi(Double.parseDouble(dados[29].substring(1, dados[29].length()-1)));
                col.setTempMod_moSi(Double.parseDouble(dados[30].substring(1, dados[30].length()-1)));
                col.setDesvTempMod_moSi(Double.parseDouble(dados[31].substring(1, dados[31].length()-1)));
                col.setTempMod_pSi(Double.parseDouble(dados[32].substring(1, dados[32].length()-1)));
                col.setDesvTempMod_pSi(Double.parseDouble(dados[33].substring(1, dados[33].length()-1)));
               
                col.setIrrFotodiodo(Double.parseDouble(dados[34].substring(1, dados[34].length()-1)));
                col.setDesvIrrFotodiodo(Double.parseDouble(dados[35].substring(1, dados[35].length()-1)));
                col.setIrrPiranômetro(Double.parseDouble(dados[36].substring(1, dados[36].length()-1)));
                col.setDesvIrrPiranômetro(Double.parseDouble(dados[37].substring(1, dados[37].length()-1)));
                col.setValidacao(Boolean.parseBoolean(dados[38].substring(1, dados[38].length()-1)));
                col.setInsercaoManual(Boolean.parseBoolean(dados[39].substring(1, dados[39].length()-1)));
                col.setCidade(dados[40].substring(1, dados[40].length()-1));
                
                coletas.add(col);  
            }
        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(null, "Arquivo não encontrado!");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Erro!!");
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        return coletas;    
    }
    
    public void extractData(List<Coleta> coletas)
    {
        CSVWriter csvWriter = null;
        File file = new File(this.caminho+"/"+this.nome+".csv");
        try {
            csvWriter = new CSVWriter(new FileWriter(file));
            List<String[]> rows = new LinkedList<String[]>();
            
            //Header
            rows.add(new String[]{
                "Data e Hora", 
                "Irradiância Global Horizontal", "Desvio Padrão Irradiância Global Horizontal",
                "Irradiância Direta Normal","Desvio Irradiância Direta Normal",
                "Irradiância Difusa","Desvio Irradiância Difusa",
                "Temperatura Ar","Pressão Ar","Umidade Ar", 
                    
                "Corrente de Curto Circuito de GaAs", "Desvio Padrão de Corrente de Curto Circuito GaAs",
                "Corrente de Curto Circuito de TJ", "Desvio Padrão de Corrente de Curto Circuito TJ",
                "Corrente de Curto Circuito de aSi", "Desvio Padrão de Corrente de Curto Circuito aSi",
                "Corrente de Curto Circuito de micSi", "Desvio Padrão de Corrente de Curto Circuito micSi",
                "Corrente de Curto Circuito de moSi", "Desvio Padrão de Corrente de Curto Circuito moSi",
                "Corrente de Curto Circuito de pSi", "Desvio Padrão de Corrente de Curto Circuito pSi",

                "Temperatura do Módulo de GaAs", "Desvio Padrão Temperatura do Módulo GaAs",
                "Temperatura do Módulo de TJ", "Desvio Padrão Temperatura do Módulo TJ",
                "Temperatura do Módulo de aSi", "Desvio Padrão Temperatura do Módulo aSi",
                "Temperatura do Módulo de micSi", "Desvio Padrão Temperatura do Módulo micSi",
                "Temperatura do Módulo de moSi", "Desvio Padrão Temperatura do Módulo moSi",
                "Temperatura do Módulo de pSi", "Desvio Padrão Temperatura do Módulo pSi",

                "Irradiância Fotodiodo", "Desvio Padrão Fotodiodo",
                "Irradiância Piranômetro", "Desvio Padrão Piranômetro",
                "Validação",  "InsercaoManual", "Cidade","Dados Alterados"
            });
            
            //Coletas
            String alteracoes = "";
            ComboBoxConteudo cont = new ComboBoxConteudo();
            AlteracaoDAO dao = new AlteracaoDAO();
            for(Coleta c:coletas){
                alteracoes = "";
                for(String s: dao.buscaAlteracoes(c.getDataHora_col()))
                {
                    alteracoes += cont.getCampo(s) + ", ";
                }
                if(alteracoes.compareTo("")!=0)
                    alteracoes = alteracoes.substring(0,alteracoes.length()-2);
                rows.add(new String[]{c.getDataHora_col().toString(), 
                    Double.toString(c.getIrradGlobHorizontal()), Double.toString(c.getDesvGlobHorizontal()),
                    Double.toString(c.getIrradDirNormal()), Double.toString(c.getDesvDirNormal()),
                    Double.toString(c.getRadiacaoDifusa()), Double.toString(c.getDesvDifusa()),
                    Double.toString(c.getTempAr()), Double.toString(c.getPressAr()),
                    Double.toString(c.getUmidAr()), 
                    
                    Double.toString(c.getCorrCC_GaAs()), Double.toString(c.getDesvCorrCC_GaAs()),
                    Double.toString(c.getCorrCC_TJ()), Double.toString(c.getDesvCorrCC_TJ()),
                    Double.toString(c.getCorrCC_aSi()), Double.toString(c.getDesvCorrCC_aSi()),
                    Double.toString(c.getCorrCC_micSi()), Double.toString(c.getDesvCorrCC_micSi()),
                    Double.toString(c.getCorrCC_moSi()), Double.toString(c.getDesvCorrCC_moSi()),
                    Double.toString(c.getCorrCC_pSi()), Double.toString(c.getDesvCorrCC_pSi()),
                    
                    Double.toString(c.getTempMod_GaAs()), Double.toString(c.getDesvTempMod_GaAs()),
                    Double.toString(c.getTempMod_TJ()), Double.toString(c.getDesvTempMod_TJ()),
                    Double.toString(c.getTempMod_aSi()), Double.toString(c.getDesvTempMod_aSi()),
                    Double.toString(c.getTempMod_micSi()), Double.toString(c.getDesvTempMod_micSi()),
                    Double.toString(c.getTempMod_moSi()), Double.toString(c.getDesvTempMod_moSi()),
                    Double.toString(c.getTempMod_pSi()), Double.toString(c.getDesvTempMod_pSi()),
                    
                    Double.toString(c.getIrrFotodiodo()), Double.toString(c.getDesvIrrFotodiodo()),
                    Double.toString(c.getIrrPiranômetro()), Double.toString(c.getDesvIrrPiranômetro()),
                    Boolean.toString(c.isValidacao()), Boolean.toString(c.isInsercaoManual()),
                    c.getCidade(),
                    /*String gerada pela busca de dados alterados*/
                    alteracoes
                });
            }
            csvWriter.writeAll(rows);         
            csvWriter.close();
        } catch (IOException ex) {
            Logger.getLogger(csv.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
