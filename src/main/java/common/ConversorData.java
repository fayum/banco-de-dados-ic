/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class ConversorData {
     
    /**
     * Método converte Data dd/MM/aaaa em 
     * @return String aaaa-MM-dd
     */
    public static String FormatarDataConvencional(String dt)
    {
        Date date = new Date();
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        formatter.setLenient(false);
        try {
            date = (java.util.Date)formatter.parse(dt);
            String ano = Integer.toString(date.getYear()+1900); 
            int anoVal= date.getYear()+1900;
            if(anoVal <1000)
            {    
                ano = "0"+ ano;
                if(anoVal<100)
                {
                    ano = "0"+ano;
                    if(anoVal<10)
                    {
                        ano = "0"+ano;
                    }
                }                
            }
            return ano +"-"+ ((date.getMonth()+1)<10 ? "0"+(date.getMonth()+1): (date.getMonth()+1)) +"-"+(date.getDate()<10 ? "0"+date.getDate() : date.getDate());
        } catch (ParseException ex) {
            JOptionPane.showMessageDialog(null, "Data inválida ou em formato incorreto!");
            Logger.getLogger(ConversorData.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    /**
     * Método converte Data dd/MM/aaaa + Hora HH:mm:ss em 
     * @return Timestamp aaaa-MM-dd HH:mm:ss
     * @throws java.text.ParseException
     */
    public static Timestamp FormatarDataConvencionalHoraFinal(String dia, String hora) throws ParseException{
        dia = FormatarDataConvencional(dia) + " "+ hora;
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dtConv1 = LocalDateTime.parse(dia, df); 
        return Timestamp.valueOf(dtConv1);
    }
    
    /**
     * Método converte Data aaaa-MM-dd + Hora HH:mm:ss em 
     * @return Timestamp aaaa-MM-dd HH:mm:ss
     */
    public static Timestamp FormatarDataConvertidaHoraFinal(String data, String hora) throws ParseException{
        
        data = data + " "+ hora;
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dtConv1 = LocalDateTime.parse(data, df); 
        
        return Timestamp.valueOf(dtConv1);
    }

    /**
     * Parâmetro: Timestamp
     * Saída: dd/mm/aaaa
     * 
     */
     public static String FormatarTimeStampEmDataConvencional(Timestamp data)
     {
         return ((data.getDate()<10)?("0"+data.getDate()):(data.getDate()))+"/"
                            +((data.getMonth()+1<10)?("0"+(data.getMonth()+1)):(data.getMonth()+1))+"/"
                            +(((data.getYear()+1900)<1000)?("0"):(""))
                            +(((data.getYear()+1900)<100)?("0"):(""))
                            +(((data.getYear()+1900)<10)?("0"):(""))
                            +(data.getYear()+1900);
     }
     /**
     * Parâmetro: Timestamp
     * Saída: HH:mm:ss
     * 
     */
     public static String FormatarTimeStampEmHora(Timestamp data)
     {
         return ((data.getHours()<10)?("0"):(""))+data.getHours()+":"
                            +((data.getMinutes()<10)?("0"):(""))+data.getMinutes()+":"
                            +((data.getSeconds()<10)?("0"):(""))+data.getSeconds();
     }
     
     public static Timestamp FinalDoDia(Timestamp Data)
     {
        try {
            return FormatarDataConvencionalHoraFinal(Data.getDate()+"/"+(Data.getMonth()+1)+"/"+(Data.getYear()+1900), "24:00:00");
        } catch (ParseException ex) {
            Logger.getLogger(ConversorData.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
     }
}
