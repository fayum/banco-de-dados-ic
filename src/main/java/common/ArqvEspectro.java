/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import java.util.List;

/**
 *
 * @author Flávia Yumi
 */
public abstract class ArqvEspectro {
    
    private String path;
    
    public ArqvEspectro(String path)
    {
        this.path = path;
    }
    
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
    
    public abstract List<ponto> ler();    
}
