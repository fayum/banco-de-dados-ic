/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import model.bean.Espectro;
import org.apache.commons.lang3.math.NumberUtils;

/**
 *
 * @author Flávia Yumi
 */
public class txtEspectro extends ArqvEspectro{

    public txtEspectro(String path) {
        super(path);
    }

    @Override
    public List<ponto> ler() {
        
        List<ponto> curva = new ArrayList<>();
        try{
        BufferedReader texto = new BufferedReader(new FileReader(this.getPath()));
            String linha;
            int i = 0;
            
            while ((linha = texto.readLine()) != null) 
            {
                String[] parte = linha.split(",");
                if(parte.length==2)
                {
                    System.out.println("["+parte[0]+"]["+parte[1]+"]");
                    if((NumberUtils.isCreatable(parte[0])) && (NumberUtils.isCreatable(parte[1])))
                    {
                        curva.add(new ponto(Double.parseDouble(parte[0]), Double.parseDouble(parte[1])));
                        System.out.println(curva.get(i));
                        i++;
                    }
                }
            }
            
        }
        catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(null,"Arquivo não encontrado!");
        }
        catch (IOException ex) {
            Logger.getLogger(Espectro.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return curva;
    }
}
