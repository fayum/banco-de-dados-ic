/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import model.bean.Coleta;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.apache.commons.lang3.math.NumberUtils;


public class dat  extends ArqvColeta{

    public dat(String nome) {
        super(nome);
    }
   
    public Map procurarColunas(Map<String, Integer> mapa, int header, String div)
    {
        System.out.println("Procurar Colunas arquivo");
        BufferedReader br = null;
        String linha;
        
        try {
            
            br = new BufferedReader(new FileReader(this.getCaminho()));
            int i=0;
            while(i<header-1 && br.readLine()!=null) // Posiciona a linha que deve ser feita a análise
                i++;
                
            if ((linha = br.readLine()) != null) {
                String[] dados = linha.split(div);
                
                for (Map.Entry<String, Integer> entry : mapa.entrySet()) {
                    for(int j=0;j<dados.length;j++)
                    {
                        if(dados[j].substring(1,dados[j].length()-1).compareTo(entry.getKey())==0)
                        {
                            mapa.replace(dados[j].substring(1,dados[j].length()-1), -1, j);
                            this.getColunasExistentes().add(entry.getKey());
                            break;
                        }
                    }
                    if(entry.getValue() == -1)
                    {
                        this.getColunasInexistentes().add(entry.getKey());
                    }
                }
                
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.getColunasExistentes().add("Cidade");
        return mapa;
    }
    
    public List<Coleta> ler(String Cidade)
    {
        BufferedReader br = null;
        String linha = "";
        String datDivisor = ",";
        //System.out.println("Vai preencher o vetor de colunas");
        Map<String, Integer> colunas = new HashMap<String, Integer>(); //Campo, qual coluna no arquivo
        colunas.put("TIMESTAMP",-1);
        colunas.put("Global_Avg",-1);
        colunas.put("Difusa_Avg",-1);
        colunas.put("Temp_ar",-1);
        colunas.put("Umid_ar",-1);
        colunas.put("Press",-1);
        colunas.put("Direta_Avg",-1);
        
        colunas = this.procurarColunas(colunas, 2, datDivisor);
        
        if(this.getColunasExistentes().isEmpty()|| this.getColunasExistentes()==null ||
                (this.getColunasExistentes().size()==1 && this.getColunasExistentes().get(0).compareTo("Cidade")==0))
        {
            JOptionPane.showMessageDialog(null, "Não identificou campos suficientes para cadastro");
            System.out.println("Não identificou campos suficientes para cadastro");
            return null;
        }
        
        /*Campos existentes*/
        System.out.println("Identificou: ");
        for(String s:this.getColunasExistentes())
            System.out.println(s);
        
        //Campos inexistentes
        if(this.getColunasInexistentes().isEmpty()==false)
        {
            if(this.getColunasInexistentes().contains("TIMESTAMP"))
            {
                JOptionPane.showMessageDialog(null, "Identifique o campo 'TIMESTAMP' no arquivo.");
                return null;
            }
            String camposInexistentes="";
            for(String s: this.getColunasInexistentes())
                camposInexistentes += s +"\n";
            JOptionPane.showMessageDialog(null, "Nao encontrou a(s) coluna(s):\n"+camposInexistentes);
        }
        
        
        List<Coleta> coletas = new ArrayList<Coleta>();
        List<Coleta> coletasErroCampo = new ArrayList<Coleta>();
        Boolean erroCampo = false; //flag para indicar se coleta tem erro
        Coleta col;
        try {
            br = new BufferedReader(new FileReader(this.getCaminho()));
            
            //Para começar a inserir na quinta linha
            br.readLine();
            br.readLine();
            br.readLine();
            br.readLine();
            linha = br.readLine();
            if(linha == null)
            {
                JOptionPane.showMessageDialog(null, "Arquivo sem conteúdo.");
                return null;
            }
            else
                System.out.println(linha);
            
            do {
                String[] dados = linha.split(datDivisor);
                col = new Coleta();
                erroCampo = false;
                try {
                    if(colunas.get("TIMESTAMP")!=-1)
                    {
                        if(dados[colunas.get("TIMESTAMP")].length()>19)
                            col.setDataHora_col(ConversorData.FormatarDataConvertidaHoraFinal(dados[colunas.get("TIMESTAMP")].substring(1, 11), dados[colunas.get("TIMESTAMP")].substring(12, 20)));
                        else
                        {
                            System.out.println("Erro: "+dados[colunas.get("TIMESTAMP")].length()+"\n["+
                                    dados[colunas.get("TIMESTAMP")]);
                            JOptionPane.showMessageDialog(null, "Campo 'TIMESTAMP' com formato desconhecido.\n"
                                    + "Deve seguir o modelo: AAAA-MM-dd HH:mm:ss.");
                                    return null;
                        }
                        System.out.println(col.getDataHora_col());
                    }
                } catch (ParseException ex) {
                    Logger.getLogger(dat.class.getName()).log(Level.SEVERE, null, ex);
                }
                if(colunas.get("Global_Avg")!=-1 && NumberUtils.isCreatable(dados[colunas.get("Global_Avg")]))
                        col.setIrradGlobHorizontal(Double.parseDouble(dados[colunas.get("Global_Avg")]));
                else
                    erroCampo = true;
                if(colunas.get("Difusa_Avg")!=-1 && NumberUtils.isCreatable(dados[colunas.get("Difusa_Avg")]))
                        col.setRadiacaoDifusa(Double.parseDouble(dados[colunas.get("Difusa_Avg")]));
                else
                    erroCampo = true;
                if(colunas.get("Umid_ar")!=-1 && NumberUtils.isCreatable(dados[colunas.get("Umid_ar")]))
                        col.setUmidAr(Double.parseDouble(dados[colunas.get("Umid_ar")]));               
                else
                    erroCampo = true;
                if(colunas.get("Temp_ar")!=-1 && NumberUtils.isCreatable((dados[colunas.get("Temp_ar")])))
                        col.setTempAr(Double.parseDouble(dados[colunas.get("Temp_ar")]));
                else
                    erroCampo = true;
                if(colunas.get("Press")!=-1 && NumberUtils.isCreatable(dados[colunas.get("Press")]))
                        col.setPressAr(Double.parseDouble(dados[colunas.get("Press")]));
                else
                    erroCampo = true;
                dados[colunas.get("Direta_Avg")] = "hanna";
                if(colunas.get("Direta_Avg")!=-1 && NumberUtils.isCreatable(dados[colunas.get("Direta_Avg")]))
                        col.setIrradDirNormal(Double.parseDouble(dados[colunas.get("Direta_Avg")]));
                else
                    erroCampo = true;
                col.setInsercaoManual(false);
                col.setCidade(Cidade);
                if(erroCampo)
                    coletasErroCampo.add(col);
                else
                    coletas.add(col);  
            }while ((linha = br.readLine()) != null);

        } catch (FileNotFoundException e) {
            System.out.println("Arquivo não encontrado!");
            JOptionPane.showMessageDialog(null, "Arquivo não encontrado");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Erro!!");
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        if(!coletasErroCampo.isEmpty())
        {
            String[] options = {"Sim", "Não"};
            int x = JOptionPane.showOptionDialog(null, coletasErroCampo.size()+" coletas tiveram erro de leitura. "
                    + "Deseja substituir o valor por 0 (zero) e adicionar essas coletas ao banco?",
                    "Erro de leitura",
                    JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);
            if(x==0)
            {
                x = JOptionPane.showOptionDialog(null, "Você tem certeza?",
                    "Erro de leitura",
                    JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);
                if(x==0)
                {
                    coletasErroCampo.forEach((c) -> {
                        coletas.add(c);
                    });
                }
            }
        }
        return coletas;
    }

    public List<Coleta> lerGen(String Cidade, int header, int data, String div) {
        
        BufferedReader br = null;
        String linha = "";
        
        Map<String, Integer> colunas = new HashMap<String, Integer>(); //Campo, qual coluna no arquivo
        colunas.put("TIMESTAMP",-1);
        colunas.put("Global_Avg",-1);
        colunas.put("Difusa_Avg",-1);
        colunas.put("Temp_ar",-1);
        colunas.put("Umid_ar",-1);
        colunas.put("Press",-1);
        colunas.put("Direta_Avg",-1);
        
        colunas.put("T_Amorfo_Avg",-1);
        colunas.put("T_Micro_Avg",-1);
        colunas.put("T_Poli_Avg",-1);
        colunas.put("T_Mono_Avg",-1);
        colunas.put("T_GaAs_Avg",-1);
        colunas.put("T_TMJ_Avg",-1);
        
        colunas.put("T_Amorfo_Std",-1);
        colunas.put("T_Micro_Std",-1);
        colunas.put("T_Poli_Std",-1);
        colunas.put("T_Mono_Std",-1);
        colunas.put("T_GaAs_Std",-1);
        colunas.put("T_TMJ_Std",-1);
        
        colunas.put("I_Amorfo_Avg",-1);
        colunas.put("I_Micro_Avg",-1);
        colunas.put("I_Poli_Avg",-1);
        colunas.put("I_Mono_Avg",-1);
        colunas.put("I_GaAs_Avg",-1);
        colunas.put("I_TMJ_Avg",-1);
        
        colunas.put("I_Amorfo_Std",-1);
        colunas.put("I_Micro_Std",-1);
        colunas.put("I_Poli_Std",-1);
        colunas.put("I_Mono_Std",-1);
        colunas.put("I_GaAs_Std",-1);
        colunas.put("I_TMJ_Std",-1);
        
        colunas = this.procurarColunas(colunas, header, div);
        //Campos inexistentes
        if(this.getColunasInexistentes().isEmpty()==false)
        {
            String camposInexistentes="";
            for(String s: this.getColunasInexistentes())
                camposInexistentes += s +"\n";
            JOptionPane.showMessageDialog(null, "Nao encontrou a(s) coluna(s):\n"+camposInexistentes);
        }
        
        List<Coleta> coletas = new ArrayList<Coleta>();
        Coleta col;
        System.out.println("Antes do try");
        try {
            br = new BufferedReader(new FileReader(this.getCaminho()));
            
            //Para começar a inserir na quinta linha
            for(int i=1; i<data;i++)
                br.readLine();
            
            System.out.println("Começa a leitura de dados");
            while ((linha = br.readLine()) != null) {
                String[] dados = linha.split(div);
                col = new Coleta();
                try {
                    
                    System.out.println("Tam: "+ dados.length+ "\ndata "+colunas.get("TIMESTAMP"));
                    
                    if(colunas.get("TIMESTAMP")!=-1 && colunas.get("TIMESTAMP")<dados.length)
                        if(dados[colunas.get("TIMESTAMP")].length()>=20)
                        {
                            col.setDataHora_col(ConversorData.FormatarDataConvertidaHoraFinal(dados[colunas.get("TIMESTAMP")].substring(1, 11), dados[colunas.get("TIMESTAMP")].substring(12, 20)));
                        
                            System.out.println("Parte 1");
                            if(colunas.get("Global_Avg")!=-1 && colunas.get("Global_Avg")<dados.length && NumberUtils.isCreatable(dados[colunas.get("Global_Avg")]))
                                col.setIrradGlobHorizontal(Double.parseDouble(dados[colunas.get("Global_Avg")]));
                            if(colunas.get("Difusa_Avg")!=-1 && colunas.get("Difusa_Avg")<dados.length && NumberUtils.isCreatable(dados[colunas.get("Difusa_Avg")]))
                                col.setRadiacaoDifusa(Double.parseDouble(dados[colunas.get("Difusa_Avg")]));
                            if(colunas.get("Umid_ar")!=-1 && colunas.get("Umid_ar")<dados.length && NumberUtils.isCreatable(dados[colunas.get("Umid_ar")]))
                                col.setUmidAr(Double.parseDouble(dados[colunas.get("Umid_ar")]));
                            if(colunas.get("Temp_ar")!=-1 && colunas.get("Temp_ar")<dados.length && NumberUtils.isCreatable(dados[colunas.get("Temp_ar")]))
                                col.setTempAr(Double.parseDouble(dados[colunas.get("Temp_ar")]));
                            if(colunas.get("Press")!=-1 && colunas.get("Press")<dados.length && NumberUtils.isCreatable(dados[colunas.get("Press")]))
                                col.setPressAr(Double.parseDouble(dados[colunas.get("Press")]));
                            if(colunas.get("Direta_Avg")!=-1 && colunas.get("Direta_Avg")<dados.length && NumberUtils.isCreatable(dados[colunas.get("Direta_Avg")]))
                                col.setIrradDirNormal(Double.parseDouble(dados[colunas.get("Direta_Avg")]));
                            System.out.println("Parte 2");
                            if(colunas.get("I_GaAs_Avg")!=-1 && colunas.get("I_GaAs_Avg")<dados.length && NumberUtils.isCreatable(dados[colunas.get("I_GaAs_Avg")]))
                                col.setCorrCC_GaAs(Double.parseDouble(dados[colunas.get("I_GaAs_Avg")]));
                            if(colunas.get("I_TMJ_Avg")!=-1 && colunas.get("I_TMJ_Avg")<dados.length && NumberUtils.isCreatable(dados[colunas.get("I_TMJ_Avg")]))
                                col.setCorrCC_TJ(Double.parseDouble(dados[colunas.get("I_TMJ_Avg")]));
                            if(colunas.get("I_Amorfo_Avg")!=-1 && colunas.get("I_Amorfo_Avg")<dados.length && NumberUtils.isCreatable(dados[colunas.get("I_Amorfo_Avg")]))
                                col.setCorrCC_aSi(Double.parseDouble(dados[colunas.get("I_Amorfo_Avg")]));
                            if(colunas.get("I_Micro_Avg")!=-1 && colunas.get("I_Micro_Avg")<dados.length && NumberUtils.isCreatable(dados[colunas.get("I_Micro_Avg")]))
                                col.setCorrCC_micSi(Double.parseDouble(dados[colunas.get("I_Micro_Avg")]));
                            if(colunas.get("I_Mono_Avg")!=-1 && colunas.get("I_Mono_Avg")<dados.length && NumberUtils.isCreatable(dados[colunas.get("I_Mono_Avg")]))
                                col.setCorrCC_moSi(Double.parseDouble(dados[colunas.get("I_Mono_Avg")]));
                            if(colunas.get("I_Poli_Avg")!=-1 && colunas.get("I_Poli_Avg")<dados.length && NumberUtils.isCreatable(dados[colunas.get("I_Poli_Avg")]))
                                col.setCorrCC_pSi(Double.parseDouble(dados[colunas.get("I_Poli_Avg")]));
                            System.out.println("Parte 3");
                            if(colunas.get("I_GaAs_Std")!=-1 && colunas.get("I_GaAs_Std")<dados.length && NumberUtils.isCreatable(dados[colunas.get("I_GaAs_Std")]))
                                col.setDesvCorrCC_GaAs(Double.parseDouble(dados[colunas.get("I_GaAs_Std")]));
                            if(colunas.get("I_TMJ_Std")!=-1 && colunas.get("I_TMJ_Std")<dados.length && NumberUtils.isCreatable(dados[colunas.get("I_TMJ_Std")]))
                                col.setDesvCorrCC_TJ(Double.parseDouble(dados[colunas.get("I_TMJ_Std")]));
                            if(colunas.get("I_Amorfo_Std")!=-1 && colunas.get("I_Amorfo_Std")<dados.length && NumberUtils.isCreatable(dados[colunas.get("I_Amorfo_Std")]))
                                col.setDesvCorrCC_aSi(Double.parseDouble(dados[colunas.get("I_Amorfo_Std")]));
                            if(colunas.get("I_Micro_Std")!=-1 && colunas.get("I_Micro_Std")<dados.length && NumberUtils.isCreatable(dados[colunas.get("I_Micro_Std")]))
                                col.setDesvCorrCC_micSi(Double.parseDouble(dados[colunas.get("I_Micro_Std")]));
                            if(colunas.get("I_Mono_Std")!=-1 && colunas.get("I_Mono_Std")<dados.length && NumberUtils.isCreatable(dados[colunas.get("I_Mono_Std")]))
                                col.setDesvCorrCC_moSi(Double.parseDouble(dados[colunas.get("I_Mono_Std")]));
                            if(colunas.get("I_Poli_Std")!=-1 && colunas.get("I_Poli_Std")<dados.length && NumberUtils.isCreatable(dados[colunas.get("I_Poli_Std")]))
                                col.setDesvCorrCC_pSi(Double.parseDouble(dados[colunas.get("I_Poli_Std")]));
                            System.out.println("Parte 4");
                            if(colunas.get("T_GaAs_Avg")!=-1 && colunas.get("T_GaAs_Avg")<dados.length && NumberUtils.isCreatable(dados[colunas.get("T_GaAs_Avg")]))
                                col.setTempMod_GaAs(Double.parseDouble(dados[colunas.get("T_GaAs_Avg")]));
                            if(colunas.get("T_TMJ_Avg")!=-1 && colunas.get("T_TMJ_Avg")<dados.length && NumberUtils.isCreatable(dados[colunas.get("T_TMJ_Avg")]))
                                col.setTempMod_TJ(Double.parseDouble(dados[colunas.get("T_TMJ_Avg")]));
                            if(colunas.get("T_Amorfo_Avg")!=-1 && colunas.get("T_Amorfo_Avg")<dados.length && NumberUtils.isCreatable(dados[colunas.get("T_Amorfo_Avg")]))
                                col.setTempMod_aSi(Double.parseDouble(dados[colunas.get("T_Amorfo_Avg")]));
                            if(colunas.get("T_Micro_Avg")!=-1 && colunas.get("T_Micro_Avg")<dados.length && NumberUtils.isCreatable(dados[colunas.get("T_Micro_Avg")]))
                                col.setTempMod_micSi(Double.parseDouble(dados[colunas.get("T_Micro_Avg")]));
                            if(colunas.get("T_Mono_Avg")!=-1 && colunas.get("T_Mono_Avg")<dados.length && NumberUtils.isCreatable(dados[colunas.get("T_Mono_Avg")]))
                                col.setTempMod_moSi(Double.parseDouble(dados[colunas.get("T_Mono_Avg")]));
                            if(colunas.get("T_Poli_Avg")!=-1 && colunas.get("T_Poli_Avg")<dados.length && NumberUtils.isCreatable(dados[colunas.get("T_Poli_Avg")]))
                                col.setTempMod_pSi(Double.parseDouble(dados[colunas.get("T_Poli_Avg")]));
                            System.out.println("Parte 5");
                            if(colunas.get("T_GaAs_Std")!=-1 && colunas.get("T_GaAs_Std")<dados.length && NumberUtils.isCreatable(dados[colunas.get("T_GaAs_Std")]))
                                col.setDesvTempMod_GaAs(Double.parseDouble(dados[colunas.get("T_GaAs_Std")]));
                            if(colunas.get("T_TMJ_Std")!=-1 && colunas.get("T_TMJ_Std")<dados.length && NumberUtils.isCreatable(dados[colunas.get("T_TMJ_Std")]))
                                col.setDesvTempMod_TJ(Double.parseDouble(dados[colunas.get("T_TMJ_Std")]));
                            if(colunas.get("T_Amorfo_Std")!=-1 && colunas.get("T_Amorfo_Std")<dados.length && NumberUtils.isCreatable(dados[colunas.get("T_Amorfo_Std")]))
                                col.setDesvTempMod_aSi(Double.parseDouble(dados[colunas.get("T_Amorfo_Std")]));
                            if(colunas.get("T_Micro_Std")!=-1 && colunas.get("T_Micro_Std")<dados.length && NumberUtils.isCreatable(dados[colunas.get("T_Micro_Std")]))
                                col.setDesvTempMod_micSi(Double.parseDouble(dados[colunas.get("T_Micro_Std")]));
                            if(colunas.get("T_Mono_Std")!=-1 && colunas.get("T_Mono_Std")<dados.length && NumberUtils.isCreatable(dados[colunas.get("T_Mono_Std")]))
                                col.setDesvTempMod_moSi(Double.parseDouble(dados[colunas.get("T_Mono_Std")]));
                            if(colunas.get("T_Poli_Std")!=-1 && colunas.get("T_Poli_Std")<dados.length && NumberUtils.isCreatable(dados[colunas.get("T_Poli_Std")]))
                                col.setDesvTempMod_pSi(Double.parseDouble(dados[colunas.get("T_Poli_Std")]));
                            System.out.println("Parte 6");
                            col.setInsercaoManual(false);
                            col.setCidade(Cidade);

                            coletas.add(col);
                        }
                } catch (ParseException ex) {
                    Logger.getLogger(dat.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            System.out.println("Terminou a leitura");

        } catch (FileNotFoundException e) {
            System.out.println("Arquivo não encontrado!");
            JOptionPane.showMessageDialog(null, "Arquivo não encontrado");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Erro!!");
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        return coletas;
    }
}
