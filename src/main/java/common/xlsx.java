/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import model.bean.Coleta;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Flávia Yumi
 */
public class xlsx extends ArqvColeta{

    public xlsx(String nome) {
        super(nome);
    }

    @Override
    public List<Coleta> ler(String Cidade) {
        
        Map<String, Integer> colunas = new HashMap<String, Integer>();
        colunas.put("TIMESTAMP",-1);
        
        colunas.put("T_Amorfo_Avg",-1);
        colunas.put("T_Micro_Avg",-1);
        colunas.put("T_Poli_Avg",-1);
        colunas.put("T_Mono_Avg",-1);
        colunas.put("T_GaAs_Avg",-1);
        colunas.put("T_TMJ_Avg",-1);
        
        colunas.put("T_Amorfo_Std",-1);
        colunas.put("T_Micro_Std",-1);
        colunas.put("T_Poli_Std",-1);
        colunas.put("T_Mono_Std",-1);
        colunas.put("T_GaAs_Std",-1);
        colunas.put("T_TMJ_Std",-1);
        
        colunas.put("I_Amorfo_Avg",-1);
        colunas.put("I_Micro_Avg",-1);
        colunas.put("I_Poli_Avg",-1);
        colunas.put("I_Mono_Avg",-1);
        colunas.put("I_GaAs_Avg",-1);
        colunas.put("I_TMJ_Avg",-1);
        
        colunas.put("I_Amorfo_Std",-1);
        colunas.put("I_Micro_Std",-1);
        colunas.put("I_Poli_Std",-1);
        colunas.put("I_Mono_Std",-1);
        colunas.put("I_GaAs_Std",-1);
        colunas.put("I_TMJ_Std",-1);
                
        File myFile = new File(this.getCaminho()); 
        FileInputStream fis;
        List<Coleta> coletas = new ArrayList<Coleta>();
        Coleta col;
        int i;
        
        try {        
            fis = new FileInputStream(myFile);
            XSSFWorkbook  wb = new XSSFWorkbook(fis);
            XSSFSheet sheet = wb.getSheetAt(0);
            
            XSSFRow row = null; 
            Iterator rows = sheet.rowIterator();

            colunas = this.procurarColunasGen( rows, colunas, 2, "");
            if(colunas.get("TIMESTAMP")<0)
            {
                JOptionPane.showMessageDialog(null,"Coluna 'TIMESTAMP' não encontrada!\nAção interrompida.");
                return null;
            }
            i = 0;
            while(rows.hasNext()&& i <2)
            {
                row = (XSSFRow) rows.next();
                i++;
            }
            
            SimpleDateFormat timeFormat = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
            String dt="";
            
            while(rows.hasNext())
            {
                col = new Coleta();
                row = (XSSFRow) rows.next();
                
                if(row.getCell(colunas.get("TIMESTAMP")).getCellType() == CellType.NUMERIC)
                {
                    if (DateUtil.isCellDateFormatted(row.getCell(colunas.get("TIMESTAMP")))) {
                        dt = timeFormat.format(row.getCell(colunas.get("TIMESTAMP")).getDateCellValue());
                        col.setDataHora_col(dt);
                    }
                    else
                        System.out.println("Data não aceita");
                    col.setCidade(Cidade);

                    if(colunas.get("I_GaAs_Avg")>-1 && row.getCell(colunas.get("I_GaAs_Avg")).getCellType() == CellType.NUMERIC)
                        col.setCorrCC_GaAs(row.getCell(colunas.get("I_GaAs_Avg")).getNumericCellValue());
                    if(colunas.get("I_TMJ_Avg")>-1 && row.getCell(colunas.get("I_TMJ_Avg")).getCellType() == CellType.NUMERIC)
                        col.setCorrCC_TJ(row.getCell(colunas.get("I_TMJ_Avg")).getNumericCellValue());
                    if(colunas.get("I_Amorfo_Avg")>-1 && row.getCell(colunas.get("I_Amorfo_Avg")).getCellType() == CellType.NUMERIC)
                        col.setCorrCC_aSi(row.getCell(colunas.get("I_Amorfo_Avg")).getNumericCellValue());
                    if(colunas.get("I_Micro_Avg")>-1 && row.getCell(colunas.get("I_Micro_Avg")).getCellType() == CellType.NUMERIC)
                        col.setCorrCC_micSi(row.getCell(colunas.get("I_Micro_Avg")).getNumericCellValue());
                    if(colunas.get("I_Mono_Avg")>-1 && row.getCell(colunas.get("I_Mono_Avg")).getCellType() == CellType.NUMERIC)
                        col.setCorrCC_moSi(row.getCell(colunas.get("I_Mono_Avg")).getNumericCellValue());
                    if(colunas.get("I_Poli_Avg")>-1 && row.getCell(colunas.get("I_Poli_Avg")).getCellType() == CellType.NUMERIC)
                        col.setCorrCC_pSi(row.getCell(colunas.get("I_Poli_Avg")).getNumericCellValue());

                    if(colunas.get("I_GaAs_Std")>-1 && row.getCell(colunas.get("I_GaAs_Std")).getCellType() == CellType.NUMERIC)
                        col.setDesvCorrCC_GaAs(row.getCell(colunas.get("I_GaAs_Std")).getNumericCellValue());
                    if(colunas.get("I_TMJ_Std")>-1 && row.getCell(colunas.get("I_TMJ_Std")).getCellType() == CellType.NUMERIC)
                        col.setDesvCorrCC_TJ(row.getCell(colunas.get("I_TMJ_Std")).getNumericCellValue());
                    if(colunas.get("I_Amorfo_Std")>-1 && row.getCell(colunas.get("I_Amorfo_Std")).getCellType() == CellType.NUMERIC)
                        col.setDesvCorrCC_aSi(row.getCell(colunas.get("I_Amorfo_Std")).getNumericCellValue());
                    if(colunas.get("I_Micro_Std")>-1 && row.getCell(colunas.get("I_Micro_Std")).getCellType() == CellType.NUMERIC)
                        col.setDesvCorrCC_micSi(row.getCell(colunas.get("I_Micro_Std")).getNumericCellValue());
                    if(colunas.get("I_Mono_Std")>-1 && row.getCell(colunas.get("I_Mono_Std")).getCellType() == CellType.NUMERIC)
                        col.setDesvCorrCC_moSi(row.getCell(colunas.get("I_Mono_Std")).getNumericCellValue());
                    if(colunas.get("I_Poli_Std")>-1 && row.getCell(colunas.get("I_Poli_Std")).getCellType() == CellType.NUMERIC)
                        col.setDesvCorrCC_pSi(row.getCell(colunas.get("I_Poli_Std")).getNumericCellValue());

                    if(colunas.get("T_GaAs_Avg")>-1 && row.getCell(colunas.get("T_GaAs_Avg")).getCellType() == CellType.NUMERIC)
                        col.setTempMod_GaAs(row.getCell(colunas.get("T_GaAs_Avg")).getNumericCellValue());
                    if(colunas.get("T_TMJ_Avg")>-1 && row.getCell(colunas.get("T_TMJ_Avg")).getCellType() == CellType.NUMERIC)
                        col.setTempMod_TJ(row.getCell(colunas.get("T_TMJ_Avg")).getNumericCellValue());
                    if(colunas.get("T_Amorfo_Avg")>-1 && row.getCell(colunas.get("T_Amorfo_Avg")).getCellType() == CellType.NUMERIC)
                        col.setTempMod_aSi(row.getCell(colunas.get("T_Amorfo_Avg")).getNumericCellValue());
                    if(colunas.get("T_Micro_Avg")>-1 && row.getCell(colunas.get("T_Micro_Avg")).getCellType() == CellType.NUMERIC)
                        col.setTempMod_micSi(row.getCell(colunas.get("T_Micro_Avg")).getNumericCellValue());
                    if(colunas.get("T_Mono_Avg")>-1 && row.getCell(colunas.get("T_Mono_Avg")).getCellType() == CellType.NUMERIC)
                        col.setTempMod_moSi(row.getCell(colunas.get("T_Mono_Avg")).getNumericCellValue());
                    if(colunas.get("T_Poli_Avg")>-1 && row.getCell(colunas.get("T_Poli_Avg")).getCellType() == CellType.NUMERIC)
                        col.setTempMod_pSi(row.getCell(colunas.get("T_Poli_Avg")).getNumericCellValue());

                    if(colunas.get("T_GaAs_Std")>-1 && row.getCell(colunas.get("T_GaAs_Std")).getCellType() == CellType.NUMERIC)
                        col.setDesvTempMod_GaAs(row.getCell(colunas.get("T_GaAs_Std")).getNumericCellValue());
                    if(colunas.get("T_TMJ_Std")>-1 && row.getCell(colunas.get("T_TMJ_Std")).getCellType() == CellType.NUMERIC)
                        col.setDesvTempMod_TJ(row.getCell(colunas.get("T_TMJ_Std")).getNumericCellValue());
                    if(colunas.get("T_Amorfo_Std")>-1 && row.getCell(colunas.get("T_Amorfo_Std")).getCellType() == CellType.NUMERIC)
                        col.setDesvTempMod_aSi(row.getCell(colunas.get("T_Amorfo_Std")).getNumericCellValue());
                    if(colunas.get("T_Micro_Std")>-1 && row.getCell(colunas.get("T_Micro_Std")).getCellType() == CellType.NUMERIC)
                        col.setDesvTempMod_micSi(row.getCell(colunas.get("T_Micro_Std")).getNumericCellValue());
                    if(colunas.get("T_Mono_Std")>-1 && row.getCell(colunas.get("T_Mono_Std")).getCellType() == CellType.NUMERIC)
                        col.setDesvTempMod_moSi(row.getCell(colunas.get("T_Mono_Std")).getNumericCellValue());
                    if(colunas.get("T_Poli_Std")>-1 && row.getCell(colunas.get("T_Poli_Std")).getCellType() == CellType.NUMERIC)
                        col.setDesvTempMod_pSi(row.getCell(colunas.get("T_Poli_Std")).getNumericCellValue());

                    coletas.add(col);
                }
            } 
        } catch (FileNotFoundException ex) {
            Logger.getLogger(xlsx.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (IOException ex) {
            Logger.getLogger(xlsx.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return coletas;
    }

    @Override
    public List<Coleta> lerGen(String Cidade, int header, int data, String div) {
        System.out.println("LerGen");
        Map<String, Integer> colunas = new HashMap<String, Integer>();
        colunas.put("TIMESTAMP",-1);
        colunas.put("Global_Avg",-1);
        colunas.put("Difusa_Avg",-1);
        colunas.put("Temp_ar",-1);
        colunas.put("Umid_ar",-1);
        colunas.put("Press",-1);
        colunas.put("Direta_Avg",-1);
        
        colunas.put("T_Amorfo_Avg",-1);
        colunas.put("T_Micro_Avg",-1);
        colunas.put("T_Poli_Avg",-1);
        colunas.put("T_Mono_Avg",-1);
        colunas.put("T_GaAs_Avg",-1);
        colunas.put("T_TMJ_Avg",-1);
        
        colunas.put("T_Amorfo_Std",-1);
        colunas.put("T_Micro_Std",-1);
        colunas.put("T_Poli_Std",-1);
        colunas.put("T_Mono_Std",-1);
        colunas.put("T_GaAs_Std",-1);
        colunas.put("T_TMJ_Std",-1);
        
        colunas.put("I_Amorfo_Avg",-1);
        colunas.put("I_Micro_Avg",-1);
        colunas.put("I_Poli_Avg",-1);
        colunas.put("I_Mono_Avg",-1);
        colunas.put("I_GaAs_Avg",-1);
        colunas.put("I_TMJ_Avg",-1);
        
        colunas.put("I_Amorfo_Std",-1);
        colunas.put("I_Micro_Std",-1);
        colunas.put("I_Poli_Std",-1);
        colunas.put("I_Mono_Std",-1);
        colunas.put("I_GaAs_Std",-1);
        colunas.put("I_TMJ_Std",-1);
                
        File myFile = new File(this.getCaminho()); 
        FileInputStream fis;
        List<Coleta> coletas = new ArrayList<Coleta>();
        Coleta col;
        int i;
        
        try {        
            fis = new FileInputStream(myFile);
            XSSFWorkbook  wb = new XSSFWorkbook(fis);
            XSSFSheet sheet = wb.getSheetAt(0);
            
            XSSFRow row = null;
            Iterator rows = sheet.rowIterator();

            colunas = this.procurarColunasGen( rows, colunas, header, div);
            if(colunas.get("TIMESTAMP")<0)
            {
                JOptionPane.showMessageDialog(null,"Coluna 'TIMESTAMP' não encontrada!\nAção interrompida.");
                return null;
            }
            JOptionPane.showMessageDialog(null,"Reconheceu as colunas:\n"+ getColunasEntrontradas(colunas));
            
            i = 0;
            while(rows.hasNext()&& i <data-header-1)
            {
                row = (XSSFRow) rows.next();
                i++;
            }
            
            SimpleDateFormat timeFormat = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
            String dt="";
            
            while(rows.hasNext())
            {
                col = new Coleta();
                row = (XSSFRow) rows.next();
                
                System.out.println("Parte 0");
                if(row.getCell(colunas.get("TIMESTAMP")).getCellType() == CellType.NUMERIC)
                {
                    if (DateUtil.isCellDateFormatted(row.getCell(colunas.get("TIMESTAMP")))) {
                        System.out.println("Parte 0.1");
                        dt = timeFormat.format(row.getCell(colunas.get("TIMESTAMP")).getDateCellValue());
                        col.setDataHora_col(dt);
                    }
                    else
                        System.out.println("Data não aceita");
                    col.setCidade(Cidade);

                    if(colunas.get("Global_Avg")>-1 && row.getCell(colunas.get("Global_Avg")).getCellType() == CellType.NUMERIC)
                        col.setIrradGlobHorizontal(row.getCell(colunas.get("Global_Avg")).getNumericCellValue());
                    if(colunas.get("Difusa_Avg")>-1 && row.getCell(colunas.get("Difusa_Avg")).getCellType() == CellType.NUMERIC)
                        col.setRadiacaoDifusa(row.getCell(colunas.get("Difusa_Avg")).getNumericCellValue());
                    if(colunas.get("Temp_ar")>-1 && row.getCell(colunas.get("Temp_ar")).getCellType() == CellType.NUMERIC)
                        col.setTempAr(row.getCell(colunas.get("Temp_ar")).getNumericCellValue());
                    if(colunas.get("Umid_ar")>-1 && row.getCell(colunas.get("Umid_ar")).getCellType() == CellType.NUMERIC)
                        col.setUmidAr(row.getCell(colunas.get("Umid_ar")).getNumericCellValue());
                    if(colunas.get("Press")>-1 && row.getCell(colunas.get("Press")).getCellType() == CellType.NUMERIC)
                        col.setPressAr(row.getCell(colunas.get("Press")).getNumericCellValue());
                    if(colunas.get("Direta_Avg")>-1 && row.getCell(colunas.get("Direta_Avg")).getCellType() == CellType.NUMERIC)
                        col.setIrradDirNormal(row.getCell(colunas.get("Direta_Avg")).getNumericCellValue());
                    System.out.println("Parte 1");

                    if(colunas.get("I_GaAs_Avg")>-1 && row.getCell(colunas.get("I_GaAs_Avg")).getCellType() == CellType.NUMERIC)
                        col.setCorrCC_GaAs(row.getCell(colunas.get("I_GaAs_Avg")).getNumericCellValue());
                    if(colunas.get("I_TMJ_Avg")>-1 && row.getCell(colunas.get("I_TMJ_Avg")).getCellType() == CellType.NUMERIC)
                        col.setCorrCC_TJ(row.getCell(colunas.get("I_TMJ_Avg")).getNumericCellValue());
                    if(colunas.get("I_Amorfo_Avg")>-1 && row.getCell(colunas.get("I_Amorfo_Avg")).getCellType() == CellType.NUMERIC)
                        col.setCorrCC_aSi(row.getCell(colunas.get("I_Amorfo_Avg")).getNumericCellValue());
                    if(colunas.get("I_Micro_Avg")>-1 && row.getCell(colunas.get("I_Micro_Avg")).getCellType() == CellType.NUMERIC)
                        col.setCorrCC_micSi(row.getCell(colunas.get("I_Micro_Avg")).getNumericCellValue());
                    if(colunas.get("I_Mono_Avg")>-1 && row.getCell(colunas.get("I_Mono_Avg")).getCellType() == CellType.NUMERIC)
                        col.setCorrCC_moSi(row.getCell(colunas.get("I_Mono_Avg")).getNumericCellValue());
                    if(colunas.get("I_Poli_Avg")>-1 && row.getCell(colunas.get("I_Poli_Avg")).getCellType() == CellType.NUMERIC)
                        col.setCorrCC_pSi(row.getCell(colunas.get("I_Poli_Avg")).getNumericCellValue());
                    System.out.println("Parte 2");

                    if(colunas.get("I_GaAs_Std")>-1 && row.getCell(colunas.get("I_GaAs_Std")).getCellType() == CellType.NUMERIC)
                        col.setDesvCorrCC_GaAs(row.getCell(colunas.get("I_GaAs_Std")).getNumericCellValue());
                    if(colunas.get("I_TMJ_Std")>-1 && row.getCell(colunas.get("I_TMJ_Std")).getCellType() == CellType.NUMERIC)
                        col.setDesvCorrCC_TJ(row.getCell(colunas.get("I_TMJ_Std")).getNumericCellValue());
                    if(colunas.get("I_Amorfo_Std")>-1 && row.getCell(colunas.get("I_Amorfo_Std")).getCellType() == CellType.NUMERIC)
                        col.setDesvCorrCC_aSi(row.getCell(colunas.get("I_Amorfo_Std")).getNumericCellValue());
                    if(colunas.get("I_Micro_Std")>-1 && row.getCell(colunas.get("I_Micro_Std")).getCellType() == CellType.NUMERIC)
                        col.setDesvCorrCC_micSi(row.getCell(colunas.get("I_Micro_Std")).getNumericCellValue());
                    if(colunas.get("I_Mono_Std")>-1 && row.getCell(colunas.get("I_Mono_Std")).getCellType() == CellType.NUMERIC)
                        col.setDesvCorrCC_moSi(row.getCell(colunas.get("I_Mono_Std")).getNumericCellValue());
                    if(colunas.get("I_Poli_Std")>-1 && row.getCell(colunas.get("I_Poli_Std")).getCellType() == CellType.NUMERIC)
                        col.setDesvCorrCC_pSi(row.getCell(colunas.get("I_Poli_Std")).getNumericCellValue());
                    System.out.println("Parte 3");

                    if(colunas.get("T_GaAs_Avg")>-1 && row.getCell(colunas.get("T_GaAs_Avg")).getCellType() == CellType.NUMERIC)
                        col.setTempMod_GaAs(row.getCell(colunas.get("T_GaAs_Avg")).getNumericCellValue());
                    if(colunas.get("T_TMJ_Avg")>-1 && row.getCell(colunas.get("T_TMJ_Avg")).getCellType() == CellType.NUMERIC)
                        col.setTempMod_TJ(row.getCell(colunas.get("T_TMJ_Avg")).getNumericCellValue());
                    if(colunas.get("T_Amorfo_Avg")>-1 && row.getCell(colunas.get("T_Amorfo_Avg")).getCellType() == CellType.NUMERIC)
                        col.setTempMod_aSi(row.getCell(colunas.get("T_Amorfo_Avg")).getNumericCellValue());
                    if(colunas.get("T_Micro_Avg")>-1 && row.getCell(colunas.get("T_Micro_Avg")).getCellType() == CellType.NUMERIC)
                        col.setTempMod_micSi(row.getCell(colunas.get("T_Micro_Avg")).getNumericCellValue());
                    if(colunas.get("T_Mono_Avg")>-1 && row.getCell(colunas.get("T_Mono_Avg")).getCellType() == CellType.NUMERIC)
                        col.setTempMod_moSi(row.getCell(colunas.get("T_Mono_Avg")).getNumericCellValue());
                    if(colunas.get("T_Poli_Avg")>-1 && row.getCell(colunas.get("T_Poli_Avg")).getCellType() == CellType.NUMERIC)
                        col.setTempMod_pSi(row.getCell(colunas.get("T_Poli_Avg")).getNumericCellValue());

                    if(colunas.get("T_GaAs_Std")>-1 && row.getCell(colunas.get("T_GaAs_Std")).getCellType() == CellType.NUMERIC)
                        col.setDesvTempMod_GaAs(row.getCell(colunas.get("T_GaAs_Std")).getNumericCellValue());
                    if(colunas.get("T_TMJ_Std")>-1 && row.getCell(colunas.get("T_TMJ_Std")).getCellType() == CellType.NUMERIC)
                        col.setDesvTempMod_TJ(row.getCell(colunas.get("T_TMJ_Std")).getNumericCellValue());
                    if(colunas.get("T_Amorfo_Std")>-1 && row.getCell(colunas.get("T_Amorfo_Std")).getCellType() == CellType.NUMERIC)
                        col.setDesvTempMod_aSi(row.getCell(colunas.get("T_Amorfo_Std")).getNumericCellValue());
                    if(colunas.get("T_Micro_Std")>-1 && row.getCell(colunas.get("T_Micro_Std")).getCellType() == CellType.NUMERIC)
                        col.setDesvTempMod_micSi(row.getCell(colunas.get("T_Micro_Std")).getNumericCellValue());
                    if(colunas.get("T_Mono_Std")>-1 && row.getCell(colunas.get("T_Mono_Std")).getCellType() == CellType.NUMERIC)
                        col.setDesvTempMod_moSi(row.getCell(colunas.get("T_Mono_Std")).getNumericCellValue());
                    if(colunas.get("T_Poli_Std")>-1 && row.getCell(colunas.get("T_Poli_Std")).getCellType() == CellType.NUMERIC)
                        col.setDesvTempMod_pSi(row.getCell(colunas.get("T_Poli_Std")).getNumericCellValue());

                    coletas.add(col);
                }
            } 
        } catch (FileNotFoundException ex) {
            Logger.getLogger(xlsx.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (IOException ex) {
            Logger.getLogger(xlsx.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return coletas;
    }

    public Map procurarColunasGen(Iterator rows, Map<String, Integer> mapa, int header, String div) {
        List<String> colunasExistentes = new ArrayList();
        System.out.println("Procurar Colunas");
        
        XSSFRow row = null; 
        XSSFCell cell;
        int i;
        if (rows.hasNext())
        {   
            i=0;
            while(rows.hasNext()&&i<header)
            {
                System.out.println("Pulou linha");
                row = (XSSFRow) rows.next();
                i++;                    
            }

            Iterator cells = row.cellIterator();
            i=0;
            while (cells.hasNext())
            {
                cell=(XSSFCell) cells.next();
                if(cell.getCellType() == CellType.STRING)
                {
                    System.out.println("Ceula: "+cell.getStringCellValue());
                    if(mapa.containsKey(cell.getStringCellValue()))
                    {
                        mapa.replace(cell.getStringCellValue(), -1, i);
                        colunasExistentes.add(cell.getStringCellValue());
                    }
                }
                i++;
            }
        }
        colunasExistentes.add("Cidade");
        this.setColunasExistentes(colunasExistentes);
        return mapa;
    }
    
    public static void imprimirMapa(Map<String, Integer> mapa)
    {
        System.out.println("MAPA: ");
         for (String key : mapa.keySet()) {
            //Capturamos o valor a partir da chave
            Integer value = mapa.get(key);
            System.out.println(key + " = " + value);
        }
    }
    
    public static String getColunasEntrontradas(Map<String, Integer> mapa)
    {
        String s = new String();
         for (String key : mapa.keySet()) {
            Integer value = mapa.get(key);
            if(value>-1)
                s+= key + "\n";
        }
        return s;
    }
   
}
