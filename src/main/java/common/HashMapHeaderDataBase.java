/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import java.util.HashMap;

/**
 *
 * @author Flávia Yumi
 */
public class HashMapHeaderDataBase {
    
    private HashMap<String, String> mapa = new HashMap<String, String>();
    
    public HashMapHeaderDataBase()
    {
        mapa.put("TIMESTAMP","DataHora_col");
        mapa.put("Global_Avg","IrradGlobHorizontal");
        mapa.put("Desvio Padrão da Irradiância Global Horizontal","DesvGlobHorizontal");
        mapa.put("Direta_Avg","IrradDirNormal");
        mapa.put("Desvio Padrão DirNormal","DesvDirNormal");
        mapa.put("Difusa_Avg","RadiacaoDifusa");
        mapa.put("Desvio Padrão Difusa","DesvDifusa");
        mapa.put("Temp_ar","TempAr");
        mapa.put("Umid_ar","UmidAr");
        mapa.put("Press","PressAr");
                
        mapa.put("I_Amorfo_Avg","CorrCC_aSi");
        mapa.put("I_Micro_Avg","CorrCC_micSi");
        mapa.put("I_Poli_Avg","CorrCC_pSi");
        mapa.put("I_Mono_Avg","CorrCC_moSi");
        mapa.put("I_GaAs_Avg","CorrCC_GaAs");
        mapa.put("I_TMJ_Avg","CorrCC_TJ");
        
        mapa.put("I_Amorfo_Std","DesvCorrCC_aSi");
        mapa.put("I_Micro_Std","DesvCorrCC_micSi");
        mapa.put("I_Poli_Std","DesvCorrCC_pSi");
        mapa.put("I_Mono_Std","DesvCorrCC_moSi");
        mapa.put("I_GaAs_Std","DesvCorrCC_GaAs");
        mapa.put("I_TMJ_Std","DesvCorrCC_TJ");
        
        mapa.put("T_Amorfo_Avg","TempMod_aSi");
        mapa.put("T_Micro_Avg","TempMod_micSi");
        mapa.put("T_Poli_Avg","TempMod_pSi");
        mapa.put("T_Mono_Avg","TempMod_moSi");
        mapa.put("T_GaAs_Avg","TempMod_GaAs");
        mapa.put("T_TMJ_Avg","TempMod_TJ");
        
        mapa.put("T_Amorfo_Std","DesvTempMod_aSi");
        mapa.put("T_Micro_Std","DesvTempMod_micSi");
        mapa.put("T_Poli_Std","DesvTempMod_pSi");
        mapa.put("T_Mono_Std","DesvTempMod_moSi");
        mapa.put("T_GaAs_Std","DesvTempMod_GaAs");
        mapa.put("T_TMJ_Std","DesvTempMod_TJ");
        
        mapa.put("Irradiância do Piranômetro","IrrPiranômetro");
        mapa.put("Desvio Padrão da Irradiância do Piranômetro","DesvIrrPiranômetro");
        mapa.put("Irradiância do Fotodiodo","IrrFotodiodo");
        mapa.put("Desvio Padrão da Irradiância do Fotodiodo","DesvIrrFotodiodo");
        mapa.put("Cidade","Cidade");
        mapa.put("Validacao","Validacao");
        mapa.put("Insercao Manual","InsercaoManual");
    }
    public HashMap<String, String> getMapa() {
        return mapa;
    }
    public void setMapa(HashMap<String, String> mapa) {
        this.mapa = mapa;
    }
    public String getCampo(String campo)
    {
        return  mapa.get(campo);    
    }
}
