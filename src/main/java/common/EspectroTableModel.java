/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Flávia Yumi
 */
public class EspectroTableModel extends AbstractTableModel{
    
    private List<ponto> curva = new ArrayList<ponto>();
    private String[] colunas = {"Comprimento de Onda (nm)", "Irradiância (microW/cm² nm)"};
    Object[][] data;
    Class[] tipoColunas;
    
    @Override
    public String getColumnName(int num) {
        return colunas[num];
    }
    
    @Override
    public int getRowCount() {
        return curva.size();
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }
    
    @Override
   public Class getColumnClass(int column) {
        return getValueAt(0, column).getClass();
   }

    @Override
    public Object getValueAt(int linha, int coluna) {
        return data[linha][coluna];
    }
    
    public void loadData(List<ponto> pontosBanco)
    {
        this.curva = pontosBanco;
        this.data = new Object[curva.size()][4];
        int i = 0;
        for(ponto p: curva)
        {
            data[i][0] = p.getX();
            data[i][1] = p.getY();
            i++;
        }
        fireTableDataChanged();
    }
        
    public Object[][] getData()
    {
        return this.data;
    }
    public ponto getPonto(int row)
    {
        return curva.get(row);
    }
    public List<ponto> getCurva()
    {
        return curva;
    }
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }
}
