/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Flávia Yumi
 */
public class Tit extends ArqvEspectro{

    public Tit(String path) {
        super(path);
    }
    
    @Override
    public List<ponto> ler()
    {
        BufferedReader br = null;        
        String linha = "";
        String datDivisor = ";";
                
        List<ponto> pontos = new ArrayList<ponto>();
        try {
            br = new BufferedReader(new FileReader(this.getPath()));
            
            //Para começar inserir a partir da segunda linha
            br.readLine();
            br.readLine();
            br.readLine();
            br.readLine();
            br.readLine();
            br.readLine();
            br.readLine();
            while ((linha = br.readLine()) != null ) {
                String[] dados = linha.split(datDivisor);
                pontos.add(new ponto(Double.parseDouble(dados[0].replace(',', '.')),Double.parseDouble(dados[4].replace(',', '.'))));
            }
        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(null, "Arquivo não encontrado!");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Erro!!");
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return pontos;        
    }
    
}
