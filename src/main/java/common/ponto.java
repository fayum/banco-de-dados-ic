/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

public class ponto {
    private Double x,y;
        
    public ponto(Double x, Double y)
    {
        setX(x);
        setY(y);
    }
    
    public Double getX() {
        return x;
    }

    public Double getY() {
        return y;
    }
    
    public void setX(Double x) {
        this.x = x;
    }

    public void setY(Double y) {
        this.y = y;
    }
    
    @Override
    public String toString()
    {
        return "("+this.getX()+", "+this.getY()+")";
    }
}
