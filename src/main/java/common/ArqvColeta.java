/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import model.bean.Coleta;

/**
 *
 * @author Flávia Yumi
 */
public abstract class ArqvColeta{
    
    private String caminho;
    private List colunasInexistentes;
    private List colunasExistentes;
    
    public ArqvColeta(String nome)
    {
        this.caminho = nome;
        colunasInexistentes = new ArrayList<String>();
        colunasExistentes = new ArrayList<String>();
    }
    
    public String getCaminho() {
        return caminho;
    }

    public void setCaminho(String caminho) {
        this.caminho = caminho;
    }

    public List<String> getColunasInexistentes() {
        return colunasInexistentes;
    }

    public void setColunasInexistentes(List<String> colunasInexistentes) {
        this.colunasInexistentes = colunasInexistentes;
    }
    
    public List<String> getColunasExistentes() {
        return colunasExistentes;
    }

    public void setColunasExistentes(List<String> colunasInexistentes) {
        this.colunasExistentes = colunasInexistentes;
    }
    
    public abstract List<Coleta> ler(String Cidade);
        
    public abstract List<Coleta> lerGen(String Cidade, int header, int data, String div);
}
