/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import model.bean.Coleta;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

public class ColetaTableModel extends AbstractTableModel{

    private List<Coleta> coletas = new ArrayList<Coleta>();
    private String[] colunas = {"", "Data", "Hora", "Cidade", ""};
    Object[][] data;
    Class[] tipoColunas;

    @Override
    public String getColumnName(int num) {
        return colunas[num];
    }
    
    @Override
    public int getRowCount() {
        return coletas.size();
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }
    
    @Override
   public Class getColumnClass(int column) {
        return getValueAt(0, column).getClass();
   }

    @Override
    public Object getValueAt(int linha, int coluna) {
        return data[linha][coluna];
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }
    
    public void loadData(List<Coleta> coletasBanco)
    {
        this.coletas = coletasBanco;
        this.data = new Object[coletas.size()][5];
        int i = 0;
        for(Coleta c: coletas)
        {
            data[i][0] = false;
            data[i][1] = ConversorData.FormatarTimeStampEmDataConvencional(c.getDataHora_col());
            data[i][2] = ConversorData.FormatarTimeStampEmHora(c.getDataHora_col());
            data[i][3] = c.getCidade();
            data[i][4] = new JButton("Abrir");
            i++;
        }
        fireTableDataChanged();
    }
    public Object[][] getData()
    {
        return this.data;
    }
    public Coleta getColeta(int row)
    {
        return coletas.get(row);
    }
    public List<Coleta> getColetas()
    {
        return coletas;
    }
    
}
