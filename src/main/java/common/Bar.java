/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JProgressBar;
import javax.swing.border.TitledBorder;

/**
 *
 * @author Flávia Yumi
 */
public class Bar extends JFrame implements Runnable{
    
    private JProgressBar progressBar;
    private TitledBorder border;
    private String message;
    private int progress;
    private int limiteProgresso;
    
    public Bar()
    {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container content = this.getContentPane();
        
        progressBar = new JProgressBar();
        progressBar.setStringPainted(true);
        progress = 0;
        limiteProgresso = 0;
        border = BorderFactory.createTitledBorder("Aguarde");
        progressBar.setBorder(border);
        content.add(progressBar, BorderLayout.NORTH);
        this.setSize(300, 100);
        this.setVisible(true);
        
    }
    
    public void setMessage(String message)
    {
        System.out.println("Menssagem mudou :" + message);
        this.message = message;
    }
    
    public void setProgress(int p)
    {
        if(this.limiteProgresso < p)
            this.limiteProgresso = p;
    }
    
    public void getProgress()
    {
        System.out.println("Progresso: " + progress);
    }
    
    @Override
    public void run() {
        Boolean hi = true;
        while(progress <= limiteProgresso && limiteProgresso<100)
        {
            //if(hi)
            //{
                hi = false;
                System.out.println("Entrou no loop");
            //}
            if(progress<=limiteProgresso)
                progress = progress + 1;
            progressBar.setValue(progress);
            this.border.setTitle(message);
        }
        this.border.setTitle(message);
        System.out.println("Saiu do loop");
    }
    
}
