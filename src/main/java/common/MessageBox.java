/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import javax.swing.border.Border;

/**
 *
 * @author Flávia Yumi
 */
public class MessageBox {
    
    public static int messageYesNO(String message)
    {
        Object[] options1 = { "Sim", "Não"};
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(new JLabel(message, JLabel.CENTER),  "South");
        
        return JOptionPane.showOptionDialog(null, panel, "Menssagem",
               JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE,
               null, options1, null);
    }
    
}
