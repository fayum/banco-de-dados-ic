/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import model.bean.Coleta;
import model.dao.ColetaDAO;

/**
 *
 * @author Flávia Yumi
 */
public class ComboBoxConteudo {
    
    private HashMap<String, String> mapa = new HashMap<String, String>();
    
    public ComboBoxConteudo()
    {
        mapa.put("Irradiância Global Horizontal","IrradGlobHorizontal");
        mapa.put("Desvio Padrão da Irradiância Global Horizontal","DesvGlobHorizontal");
        mapa.put("Irradiância Direta Normal","IrradDirNormal");
        mapa.put("Desvio Padrão DirNormal","DesvDirNormal");
        mapa.put("Radiação Difusa","RadiacaoDifusa");
        mapa.put("Desvio Padrão Difusa","DesvDifusa");
        mapa.put("Temperatura Ar","TempAr");
        mapa.put("Umidade Ar","UmidAr");
        mapa.put("Pressão Ar","PressAr");
        
        mapa.put("Corrente de Curto Circuito do Módulo de aSi","CorrCC_aSi");
        mapa.put("Corrente de Curto Circuito do Módulo de micSi","CorrCC_micSi");
        mapa.put("Corrente de Curto Circuito do Módulo de pSi","CorrCC_pSi");
        mapa.put("Corrente de Curto Circuito do Módulo de moSi","CorrCC_moSi");
        mapa.put("Corrente de Curto Circuito do Módulo de GaAs","CorrCC_GaAs");
        mapa.put("Corrente de Curto Circuito do Módulo de TJ","CorrCC_TJ");
        
        mapa.put("Desvio Padrão Corrente de Curto Circuito do Módulo de aSi","DesvCorrCC_aSi");
        mapa.put("Desvio Padrão Corrente de Curto Circuito do Módulo de micSi","DesvCorrCC_micSi");
        mapa.put("Desvio Padrão Corrente de Curto Circuito do Módulo de pSi","DesvCorrCC_pSi");
        mapa.put("Desvio Padrão Corrente de Curto Circuito do Módulo de moSi","DesvCorrCC_moSi");
        mapa.put("Desvio Padrão Corrente de Curto Circuito do Módulo de GaAs","DesvCorrCC_GaAs");
        mapa.put("Desvio Padrão Corrente de Curto Circuito do Módulo de TJ","DesvCorrCC_TJ");
        
        mapa.put("Temperatura do Módulo de aSi","TempMod_aSi");
        mapa.put("Temperatura do Módulo de micSi","TempMod_micSi");
        mapa.put("Temperatura do Módulo de pSi","TempMod_pSi");
        mapa.put("Temperatura do Módulo de moSi","TempMod_moSi");
        mapa.put("Temperatura do Módulo de GaAs","TempMod_GaAs");
        mapa.put("Temperatura do Módulo de TJ","TempMod_TJ");
        
        mapa.put("Desvio Padrão Temperatura do Módulo de aSi","DesvTempMod_aSi");
        mapa.put("Desvio Padrão Temperatura do Módulo de micSi","DesvTempMod_micSi");
        mapa.put("Desvio Padrão Temperatura do Módulo de pSi","DesvTempMod_pSi");
        mapa.put("Desvio Padrão Temperatura do Módulo de moSi","DesvTempMod_moSi");
        mapa.put("Desvio Padrão Temperatura do Módulo de GaAs","DesvTempMod_GaAs");
        mapa.put("Desvio Padrão Temperatura do Módulo de TJ","DesvTempMod_TJ");
        
        mapa.put("Irradiância do Piranômetro","IrrPiranômetro");
        mapa.put("Desvio Padrão da Irradiância do Piranômetro","DesvIrrPiranômetro");
        mapa.put("Irradiância do Fotodiodo","IrrFotodiodo");
        mapa.put("Desvio Padrão da Irradiância do Fotodiodo","DesvIrrFotodiodo");
        
        /*-----------------------------------------------------*/
        mapa.put("DataHora_col", "Data e Hora");
        mapa.put("IrradGlobHorizontal", "Irradiância Global Horizontal");
        mapa.put("DesvGlobHorizontal","Desvio Padrão da Irradiância Global Horizontal");
        mapa.put("IrradDirNormal", "Irradiância Direta Normal");
        mapa.put("DesvDirNormal", "Desvio Padrão da Irradiância Direta Normal");
        mapa.put("RadiacaoDifusa","Radiação Difusa");
        mapa.put("DesvDifusa","Desvio Padrão Difusa");
        mapa.put("TempAr","Temperatura Ar");
        mapa.put("UmidAr", "Umidade Ar");
        mapa.put("PressAr", "Pressão Ar");
        
        mapa.put("CorrCC_aSi", "Corrente de Curto Circuito do Módulo de aSi");
        mapa.put("CorrCC_micSi", "Corrente de Curto Circuito do Módulo de micSi");
        mapa.put("CorrCC_pSi", "Corrente de Curto Circuito do Módulo de pSi");
        mapa.put("CorrCC_moSi","Corrente de Curto Circuito do Módulo de moSi");
        mapa.put("CorrCC_GaAs", "Corrente de Curto Circuito do Módulo de GaAs");
        mapa.put("CorrCC_TJ", "Corrente de Curto Circuito do Módulo de TJ");
        
        mapa.put("DesvCorrCC_aSi", "Desvio Padrão da Corrente de Curto Circuito do Módulo de aSi");
        mapa.put("DesvCorrCC_micSi", "Desvio Padrão da Corrente de Curto Circuito do Módulo de micSi");
        mapa.put("DesvCorrCC_pSi", "Desvio Padrão da Corrente de Curto Circuito do Módulo de pSi");
        mapa.put("DesvCorrCC_moSi", "Desvio Padrão da Corrente de Curto Circuito do Módulo de moSi");
        mapa.put("DesvCorrCC_GaAs", "Desvio Padrão da Corrente de Curto Circuito do Módulo de GaAs");
        mapa.put("DesvCorrCC_TJ", "Desvio Padrão da Corrente de Curto Circuito do Módulo de TJ");
        
        mapa.put("TempMod_aSi", "Temperatura do Módulo de aSi");
        mapa.put("TempMod_micSi", "Temperatura do Módulo de micSi");
        mapa.put("TempMod_pSi", "Temperatura do Módulo de pSi");
        mapa.put("TempMod_moSi", "Temperatura do Módulo de moSi");
        mapa.put("TempMod_GaAs", "Temperatura do Módulo de GaAs");
        mapa.put("TempMod_TJ", "Temperatura do Módulo de TJ");
        
        mapa.put("DesvTempMod_aSi", "Desvio Padrão da Temperatura do Módulo de aSi");
        mapa.put("DesvTempMod_micSi", "Desvio Padrão da Temperatura do Módulo de micSi");
        mapa.put("DesvTempMod_pSi", "Desvio Padrão da Temperatura do Módulo de pSi");
        mapa.put("DesvTempMod_moSi", "Desvio Padrão da Temperatura do Módulo de moSi");
        mapa.put("DesvTempMod_GaAs", "Desvio Padrão da Temperatura do Módulo de GaAs");
        mapa.put("DesvTempMod_TJ", "Desvio Padrão da Temperatura do Módulo de TJ");
        
        mapa.put("IrrPiranômetro", "Irradiância do Piranômetro");
        mapa.put("DesvIrrPiranômetro", "Desvio Padrão da Irradiância do Piranômetro");
        mapa.put("IrrFotodiodo", "Irradiância do Fotodiodo");
        mapa.put("DesvIrrFotodiodo", "Desvio Padrão da Irradiância do Fotodiodo");
        mapa.put("Validacao", "Validação");
        mapa.put("Cidade", "Cidade");
    }
    
    public HashMap<String, String> getMapa() {
        return mapa;
    }
    public void setMapa(HashMap<String, String> mapa) {
        this.mapa = mapa;
    }
    
    public static String getComparador(String comparacao)
    {
        switch(comparacao)
        {
            case "Igual":
                comparacao = "=";
                break;
            case "Maior":
                comparacao = ">";
                break;
            case "Menor":
                comparacao = "<";
                break;
                case "Maior ou Igual":
                comparacao = ">=";
                break;
            case "Menor ou Igual":
                comparacao = "<=";
                break;
        }
        return comparacao;      
    }
    
    public String getCampo(String campo)
    {
        return  mapa.get(campo);    
    }
}
