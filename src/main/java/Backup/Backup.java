/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Backup;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Flávia Yumi
 */
public class Backup {
    
    public static void backup(String pathFile, String dump){
        
        List<String> commands = new ArrayList<String>();
        if(dump == null|| dump.compareTo("")==0 || pathFile == null || pathFile.compareTo("")==0)
            return;
        File file = new File(pathFile.toString().replace("\\\\", "/"));
        
        commands.add(dump);
        //commands.add("-i");      
        commands.add("-h");      
        commands.add("localhost");     
        commands.add("-p");      
        commands.add("5432");      
        commands.add("-U");      
        commands.add("postgres");      
        commands.add("-F");      
        commands.add("c");      
        commands.add("-b");      
        commands.add("-v");      
        commands.add("-f");      
        commands.add(pathFile);
        commands.add("dadosatmosfericos");
        
        ProcessBuilder pb = new ProcessBuilder(commands);
        pb.environment().put("PGPASSWORD", "userflavia");
        
        try
        {
            final Process process = pb.start();
            
            final BufferedReader r = new BufferedReader(
                new InputStreamReader(process.getErrorStream()));
            String line;
            line = r.readLine();
            
            while(line != null)
            {
                System.err.println(line);
                line = r.readLine();
            }
            r.close();
            process.waitFor();
            
            process.destroy();
            JOptionPane.showMessageDialog(null, "Backup realizado com sucesso");
        }
        catch (InterruptedException ex) {
                Logger.getLogger(Backup.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (IOException ex) {
                Logger.getLogger(Backup.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void restore(String pathFile, String restore){
        /*
        pg_restore -i -h localhost -p 5432 -U postgres -d old_db -v 
        "/usr/local/backup/10.70.0.61.backup"
        
        pg_restore [ -a ] [ -c ] [ -C ] [ -d nome_bd ] [ -f arquivo_de_saída ] 
        [ -F formato ] [ -i índice ] [ -l ] [ -L arquivo_da_listagem ] [ -N | -o | -r ]
        [ -O ] [ -P nome_da_função ] [ -R ] [ -s ] [ -S ] [ -t tabela ] [ -T gatilho ]
        [ -v ] [ -x ] [ -X palavra_chave] [ -h hospedeiro ] [ -p porta ] [ -U nome_do_usuário ]
        [ -W ] [ arquivo_de_exportação ]
        */
        List<String> commands = new ArrayList<String>();
        if(restore == null|| restore.compareTo("")==0 || pathFile == null || pathFile.compareTo("")==0)
            return;
        File file = new File(pathFile.toString().replace("\\\\", "/"));
        
        commands.add(restore);
        //commands.add("-i");      
        commands.add("-h");      
        commands.add("localhost");     
        commands.add("-p");      
        commands.add("5432");      
        commands.add("-U");      
        commands.add("postgres");
        commands.add("-c");
        commands.add("-d");      
        commands.add("dadosatmosfericos");       
        commands.add("-v");      
        commands.add(pathFile);
        
        ProcessBuilder pb = new ProcessBuilder(commands);
        pb.environment().put("PGPASSWORD", "userflavia");
        
        try
        {
            final Process process = pb.start();
            
            final BufferedReader r = new BufferedReader(
                new InputStreamReader(process.getErrorStream()));
            String line;
            line = r.readLine();
            
            while(line != null)
            {
                System.err.println(line);
                line = r.readLine();
            }
            r.close();
            process.waitFor();
            
            process.destroy();
            JOptionPane.showMessageDialog(null, "Restore realizado com sucesso");
        }
        catch (InterruptedException ex) {
                Logger.getLogger(Backup.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (IOException ex) {
                Logger.getLogger(Backup.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
