/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

import common.Tit;
import common.ArqvEspectro;
import common.ponto;
import common.txtEspectro;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.apache.commons.io.FilenameUtils;

public class Espectro {
    
    private Timestamp DataHora;
    private String cidade;
    private List<ponto> curva;
    
    public Espectro()
    {
        this.curva = new ArrayList<ponto>();
    }

    public Timestamp getDataHora() {
        return DataHora;
    }

    public void setDataHora(Timestamp DataHora) {
        this.DataHora = DataHora;
    }
    
    public String getCidade() {
        return cidade;
    }

    public void setCidade(String Cidade) {
        this.cidade = Cidade;
    }

    /**
     * @return Lista(ponto)
     */
    public List<ponto> getCurva() {
        return curva;
    }
    
    public void setCurva(String file) {
        
        ArqvEspectro arquivo = null;
        switch(FilenameUtils.getExtension(file))
        {
            case "tit":
                arquivo = new Tit(file);
                break;
            case "txt":  
                arquivo = new txtEspectro(file);
                break; 
        }
        if(arquivo != null)
            this.curva = arquivo.ler();
    }
    
    public void setCurva(List<ponto> curva) {

        this.curva = curva;
    }
}
