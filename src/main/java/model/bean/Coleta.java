/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

import common.ConversorData;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Coleta {
    
    private double IrradGlobHorizontal, RadiacaoDifusa, TempAr, UmidAr, PressAr;
    private double DesvGlobHorizontal, IrradDirNormal, DesvDirNormal, DesvDifusa;
    private double CorrCC_aSi, CorrCC_micSi, CorrCC_pSi, CorrCC_moSi, CorrCC_GaAs, CorrCC_TJ;
    private double DesvCorrCC_aSi, DesvCorrCC_micSi, DesvCorrCC_pSi, DesvCorrCC_moSi, DesvCorrCC_GaAs, DesvCorrCC_TJ;
    private double TempMod_aSi, TempMod_micSi, TempMod_pSi, TempMod_moSi, TempMod_GaAs, TempMod_TJ;
    private double DesvTempMod_aSi, DesvTempMod_micSi, DesvTempMod_pSi, DesvTempMod_moSi, DesvTempMod_GaAs, DesvTempMod_TJ;
    private double IrrPiranômetro, DesvIrrPiranômetro, IrrFotodiodo, DesvIrrFotodiodo;
    private boolean Validacao, InsercaoManual;
    private String Cidade;
    private Timestamp DataHora_col;    
    
    public Coleta(){};
    
    public Coleta(Timestamp dtHr, String Cidade)
    {
        this.DataHora_col = dtHr;
        this.Cidade = Cidade;
    }
    
    public Timestamp getDataHora_col() {
        return DataHora_col;
    }

    /** Para dados do Projeto SONDA
     * Entrada: "aaaa-MM-dd HH:mm:ss"
     * Armazena: Timestamp - aaaa-MM-dd HH:mm:ss
     */
    public void setDataHora_col(String DataHora_col) {
        ConversorData dtConv = null;
        try {
            this.DataHora_col = dtConv.FormatarDataConvertidaHoraFinal(DataHora_col.substring(0, 10), DataHora_col.substring(11,19));
        } catch (ParseException ex) {
            Logger.getLogger(Coleta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     */
    public void setDataHora_col(Timestamp dataHora) {
        this.DataHora_col = dataHora;
    }

    public double getRadiacaoDifusa() {
        return RadiacaoDifusa;
    }

    public boolean isInsercaoManual() {
        return InsercaoManual;
    }

    public void setInsercaoManual(boolean InsercaoManual) {
        this.InsercaoManual = InsercaoManual;
    }

    public String getCidade() {
        return Cidade;
    }

    public void setCidade(String Cidade) {
        this.Cidade = Cidade;
    }

    public void setRadiacaoDifusa(double RadiacaoDifusa) {
        this.RadiacaoDifusa = RadiacaoDifusa;
    }

    public double getTempAr() {
        return TempAr;
    }

    public void setTempAr(double TempAr) {
        this.TempAr = TempAr;
    }

    public double getUmidAr() {
        return UmidAr;
    }

    public void setUmidAr(double UmidAr) {
        this.UmidAr = UmidAr;
    }

    public double getPressAr() {
        return PressAr;
    }

    public void setPressAr(double PressAr) {
        this.PressAr = PressAr;
    }

    /**
     * @return the CorrCC_aSi
     */
    public double getCorrCC_aSi() {
        return CorrCC_aSi;
    }

    /**
     * @param CorrCC_aSi the CorrCC_aSi to set
     */
    public void setCorrCC_aSi(double CorrCC_aSi) {
        this.CorrCC_aSi = CorrCC_aSi;
    }

    /**
     * @return the CorrCC_micSi
     */
    public double getCorrCC_micSi() {
        return CorrCC_micSi;
    }

    /**
     * @param CorrCC_micSi the CorrCC_micSi to set
     */
    public void setCorrCC_micSi(double CorrCC_micSi) {
        this.CorrCC_micSi = CorrCC_micSi;
    }

    /**
     * @return the CorrCC_pSi
     */
    public double getCorrCC_pSi() {
        return CorrCC_pSi;
    }

    /**
     * @param CorrCC_pSi the CorrCC_pSi to set
     */
    public void setCorrCC_pSi(double CorrCC_pSi) {
        this.CorrCC_pSi = CorrCC_pSi;
    }

    /**
     * @return the CorrCC_moSi
     */
    public double getCorrCC_moSi() {
        return CorrCC_moSi;
    }

    /**
     * @param CorrCC_moSi the CorrCC_moSi to set
     */
    public void setCorrCC_moSi(double CorrCC_moSi) {
        this.CorrCC_moSi = CorrCC_moSi;
    }

    /**
     * @return the CorrCC_GaAs
     */
    public double getCorrCC_GaAs() {
        return CorrCC_GaAs;
    }

    /**
     * @param CorrCC_GaAs the CorrCC_GaAs to set
     */
    public void setCorrCC_GaAs(double CorrCC_GaAs) {
        this.CorrCC_GaAs = CorrCC_GaAs;
    }

    /**
     * @return the CorrCC_TJ
     */
    public double getCorrCC_TJ() {
        return CorrCC_TJ;
    }

    /**
     * @param CorrCC_TJ the CorrCC_TJ to set
     */
    public void setCorrCC_TJ(double CorrCC_TJ) {
        this.CorrCC_TJ = CorrCC_TJ;
    }

    /**
     * @return the DesvCorrCC_aSi
     */
    public double getDesvCorrCC_aSi() {
        return DesvCorrCC_aSi;
    }

    /**
     * @param DesvCorrCC_aSi the DesvCorrCC_aSi to set
     */
    public void setDesvCorrCC_aSi(double DesvCorrCC_aSi) {
        this.DesvCorrCC_aSi = DesvCorrCC_aSi;
    }

    /**
     * @return the DesvCorrCC_micSi
     */
    public double getDesvCorrCC_micSi() {
        return DesvCorrCC_micSi;
    }

    /**
     * @param DesvCorrCC_micSi the DesvCorrCC_micSi to set
     */
    public void setDesvCorrCC_micSi(double DesvCorrCC_micSi) {
        this.DesvCorrCC_micSi = DesvCorrCC_micSi;
    }

    /**
     * @return the DesvCorrCC_pSi
     */
    public double getDesvCorrCC_pSi() {
        return DesvCorrCC_pSi;
    }

    /**
     * @param DesvCorrCC_pSi the DesvCorrCC_pSi to set
     */
    public void setDesvCorrCC_pSi(double DesvCorrCC_pSi) {
        this.DesvCorrCC_pSi = DesvCorrCC_pSi;
    }

    /**
     * @return the DesvCorrCC_moSi
     */
    public double getDesvCorrCC_moSi() {
        return DesvCorrCC_moSi;
    }

    /**
     * @param DesvCorrCC_moSi the DesvCorrCC_moSi to set
     */
    public void setDesvCorrCC_moSi(double DesvCorrCC_moSi) {
        this.DesvCorrCC_moSi = DesvCorrCC_moSi;
    }

    /**
     * @return the DesvCorrCC_GaAs
     */
    public double getDesvCorrCC_GaAs() {
        return DesvCorrCC_GaAs;
    }

    /**
     * @param DesvCorrCC_GaAs the DesvCorrCC_GaAs to set
     */
    public void setDesvCorrCC_GaAs(double DesvCorrCC_GaAs) {
        this.DesvCorrCC_GaAs = DesvCorrCC_GaAs;
    }

    /**
     * @return the DesvCorrCC_TJ
     */
    public double getDesvCorrCC_TJ() {
        return DesvCorrCC_TJ;
    }

    /**
     * @param DesvCorrCC_TJ the DesvCorrCC_TJ to set
     */
    public void setDesvCorrCC_TJ(double DesvCorrCC_TJ) {
        this.DesvCorrCC_TJ = DesvCorrCC_TJ;
    }

    /**
     * @return the TempMod_aSi
     */
    public double getTempMod_aSi() {
        return TempMod_aSi;
    }

    /**
     * @param TempMod_aSi the TempMod_aSi to set
     */
    public void setTempMod_aSi(double TempMod_aSi) {
        this.TempMod_aSi = TempMod_aSi;
    }

    /**
     * @return the TempMod_micSi
     */
    public double getTempMod_micSi() {
        return TempMod_micSi;
    }

    /**
     * @param TempMod_micSi the TempMod_micSi to set
     */
    public void setTempMod_micSi(double TempMod_micSi) {
        this.TempMod_micSi = TempMod_micSi;
    }

    /**
     * @return the TempMod_pSi
     */
    public double getTempMod_pSi() {
        return TempMod_pSi;
    }

    /**
     * @param TempMod_pSi the TempMod_pSi to set
     */
    public void setTempMod_pSi(double TempMod_pSi) {
        this.TempMod_pSi = TempMod_pSi;
    }

    /**
     * @return the TempMod_moSi
     */
    public double getTempMod_moSi() {
        return TempMod_moSi;
    }

    /**
     * @param TempMod_moSi the TempMod_moSi to set
     */
    public void setTempMod_moSi(double TempMod_moSi) {
        this.TempMod_moSi = TempMod_moSi;
    }

    public double getIrradGlobHorizontal() {
        return IrradGlobHorizontal;
    }

    public void setIrradGlobHorizontal(double IrradGlobHorizontal) {
        this.IrradGlobHorizontal = IrradGlobHorizontal;
    }

    public double getDesvGlobHorizontal() {
        return DesvGlobHorizontal;
    }

    public void setDesvGlobHorizontal(double DesvGlobHorizontal) {
        this.DesvGlobHorizontal = DesvGlobHorizontal;
    }

    public double getIrradDirNormal() {
        return IrradDirNormal;
    }

    public void setIrradDirNormal(double IrradDirNormal) {
        this.IrradDirNormal = IrradDirNormal;
    }

    public double getDesvDirNormal() {
        return DesvDirNormal;
    }

    public void setDesvDirNormal(double DesvDirNormal) {
        this.DesvDirNormal = DesvDirNormal;
    }

    public double getDesvDifusa() {
        return DesvDifusa;
    }

    public void setDesvDifusa(double DesvDifusa) {
        this.DesvDifusa = DesvDifusa;
    }

    /**
     * @return the TempMod_GaAs
     */
    public double getTempMod_GaAs() {
        return TempMod_GaAs;
    }

    /**
     * @param TempMod_GaAs the TempMod_GaAs to set
     */
    public void setTempMod_GaAs(double TempMod_GaAs) {
        this.TempMod_GaAs = TempMod_GaAs;
    }

    /**
     * @return the TempMod_TJ
     */
    public double getTempMod_TJ() {
        return TempMod_TJ;
    }

    /**
     * @param TempMod_TJ the TempMod_TJ to set
     */
    public void setTempMod_TJ(double TempMod_TJ) {
        this.TempMod_TJ = TempMod_TJ;
    }

    /**
     * @return the DesvTempMod_aSi
     */
    public double getDesvTempMod_aSi() {
        return DesvTempMod_aSi;
    }

    /**
     * @param DesvTempMod_aSi the DesvTempMod_aSi to set
     */
    public void setDesvTempMod_aSi(double DesvTempMod_aSi) {
        this.DesvTempMod_aSi = DesvTempMod_aSi;
    }

    /**
     * @return the DesvTempMod_micSi
     */
    public double getDesvTempMod_micSi() {
        return DesvTempMod_micSi;
    }

    /**
     * @param DesvTempMod_micSi the DesvTempMod_micSi to set
     */
    public void setDesvTempMod_micSi(double DesvTempMod_micSi) {
        this.DesvTempMod_micSi = DesvTempMod_micSi;
    }

    /**
     * @return the DesvTempMod_pSi
     */
    public double getDesvTempMod_pSi() {
        return DesvTempMod_pSi;
    }

    /**
     * @param DesvTempMod_pSi the DesvTemoMod_pSi to set
     */
    public void setDesvTempMod_pSi(double DesvTempMod_pSi) {
        this.DesvTempMod_pSi = DesvTempMod_pSi;
    }

    /**
     * @return the DesvTempMod_moSi
     */
    public double getDesvTempMod_moSi() {
        return DesvTempMod_moSi;
    }

    /**
     * @param DesvTempMod_moSi the DesvTempMod_moSi to set
     */
    public void setDesvTempMod_moSi(double DesvTempMod_moSi) {
        this.DesvTempMod_moSi = DesvTempMod_moSi;
    }

    /**
     * @return the DesvTempMod_GaAs
     */
    public double getDesvTempMod_GaAs() {
        return DesvTempMod_GaAs;
    }

    /**
     * @param DesvTempMod_GaAs the DesvTempMod_GaAs to set
     */
    public void setDesvTempMod_GaAs(double DesvTempMod_GaAs) {
        this.DesvTempMod_GaAs = DesvTempMod_GaAs;
    }

    /**
     * @return the DesvTempMos_TJ
     */
    public double getDesvTempMod_TJ() {
        return DesvTempMod_TJ;
    }

    /**
     * @param DesvTempMod_TJ the DesvTempMos_TJ to set
     */
    public void setDesvTempMod_TJ(double DesvTempMod_TJ) {
        this.DesvTempMod_TJ = DesvTempMod_TJ;
    }

    /**
     * @return the IrrPiranômetro
     */
    public double getIrrPiranômetro() {
        return IrrPiranômetro;
    }

    /**
     * @param IrrPiranômetro the IrrPiranômetro to set
     */
    public void setIrrPiranômetro(double IrrPiranômetro) {
        this.IrrPiranômetro = IrrPiranômetro;
    }

    /**
     * @return the DesvIrrFotodiodo
     */
    public double getDesvIrrFotodiodo() {
        return DesvIrrFotodiodo;
    }

    /**
     * @param DesvIrrFotodiodo the DesvIrrFotodiodo to set
     */
    public void setDesvIrrFotodiodo(double DesvIrrFotodiodo) {
        this.DesvIrrFotodiodo = DesvIrrFotodiodo;
    }

    /**
     * @return the Validacao
     */
    public boolean isValidacao() {
        return Validacao;
    }

    /**
     * @param Validacao the Validacao to set
     */
    public void setValidacao(boolean Validacao) {
        this.Validacao = Validacao;
    }

    /**
     * @return the DesvIrrPiranômetro
     */
    public double getDesvIrrPiranômetro() {
        return DesvIrrPiranômetro;
    }

    /**
     * @param DesvIrrPiranômetro the DesvIrrPiranômetro to set
     */
    public void setDesvIrrPiranômetro(double DesvIrrPiranômetro) {
        this.DesvIrrPiranômetro = DesvIrrPiranômetro;
    }

    /**
     * @return the IrrFotodiodo
     */
    public double getIrrFotodiodo() {
        return IrrFotodiodo;
    }

    /**
     * @param IrrFotodiodo the IrrFotodiodo to set
     */
    public void setIrrFotodiodo(double IrrFotodiodo) {
        this.IrrFotodiodo = IrrFotodiodo;
    }
}
