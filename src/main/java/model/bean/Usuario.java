/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

/**
 *
 * @author Valdete
 */
public class Usuario {
    
    private String Usuario, Senha, Nome;
    
    public Usuario(String Usuario, String Senha, String Nome)
    {
        this.Usuario = Usuario;
        this.Senha = Senha;
        this.Nome = Nome;
    }
    public Usuario()
    {}

    public String getUsuario() {
        return Usuario;
    }

    public void setUsuario(String Usuario) {
        this.Usuario = Usuario;
    }

    public String getSenha() {
        return Senha;
    }

    public void setSenha(String Senha) {
        this.Senha = Senha;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }
    
}
