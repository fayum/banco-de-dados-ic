/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import common.ponto;
import connection.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.bean.Espectro;

public class EspectroDAO {
    
    public void delete(Espectro espec)
    {
        
        Iterator<ponto> it = espec.getCurva().iterator();
        int i=0;
        Connection conexao = ConnectionFactory.getConnection();
        
        try{
            PreparedStatement stmt = conexao.prepareStatement("DELETE FROM Espectro WHERE DataHora_espec = ? AND CompOnda = ? AND Irradiancia = ? AND Cidade = ?;");
            while(it.hasNext())
            {
                ponto pt = it.next();
                stmt.setTimestamp(1, espec.getDataHora());
                stmt.setDouble(2, pt.getX());
                stmt.setDouble(3, pt.getY());
                stmt.setString(4, espec.getCidade());
                stmt.addBatch();
                i++;
                
                if(i%1000 == 0)
                {
                    stmt.executeBatch();
                    stmt.clearBatch();
                }
            }
            stmt.executeBatch();
            stmt.close();
        }
        catch(SQLException ex){
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            ConnectionFactory.CloseConnection(conexao);   
        }
    }
    
    public void create(Espectro espec)
    {
        
        Iterator<ponto> it = espec.getCurva().iterator();
        int i=0;
        Connection conexao = ConnectionFactory.getConnection();
        
        try{
            PreparedStatement stmt = conexao.prepareStatement("INSERT INTO Espectro(DataHora_espec, CompOnda, Irradiancia, Cidade) VALUES(?,?,?,?);");
            while(it.hasNext())
            {
                ponto pt = it.next();
                stmt.setTimestamp(1, espec.getDataHora());
                stmt.setDouble(2, pt.getX());
                stmt.setDouble(3, pt.getY());
                stmt.setString(4, espec.getCidade());
                stmt.addBatch();
                i++;
                
                if(i%1000 == 0)
                {
                    stmt.executeBatch();
                    stmt.clearBatch();
                }
            }
            stmt.executeBatch();
            stmt.close();
        }
        catch(SQLException ex){
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            ConnectionFactory.CloseConnection(conexao);   
        }
    }
    
    public void updatePK(Timestamp dtOld, Timestamp dtNew, String ctOld, String ctNew)
    {
        System.out.println("Aletrar a PK do Espectro");
        Connection conexao = ConnectionFactory.getConnection();
        try
        {
            PreparedStatement stmt = conexao.prepareStatement("UPDATE espectro SET dataHora_espec = ?, cidade = ? where dataHora_espec = ? and cidade = ?;");
            stmt.setTimestamp(1, dtNew);
            stmt.setString(2, ctNew);
            stmt.setTimestamp(3, dtOld);
            stmt.setString(4, ctOld);
            
            System.out.println(stmt);
            stmt.executeUpdate();
            stmt.close();
        }
        catch(SQLException e)
        {   
            Logger.getLogger(ColetaDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        finally{
            ConnectionFactory.CloseConnection(conexao);   
        }
    }
    
    /**
     * Método retorna o espectro cadastrado na data do parâmetro
    */
    public Espectro select(Timestamp dataHora)
    {
        Espectro retorno = null;
        PreparedStatement stmt = null;
        ResultSet resultado = null;
        
        System.out.println("Data de Busca: "+dataHora);
        Connection conexao = ConnectionFactory.getConnection();
        try{
            stmt = conexao.prepareStatement("SELECT * FROM espectro WHERE datahora_espec = ?;");
            stmt.setTimestamp(1, dataHora);
            resultado = stmt.executeQuery();
            
            //Se há resultado de Busca
            ponto p;
            if(resultado.next())
            {
                retorno = new Espectro();
                p = new ponto(resultado.getDouble("componda"),resultado.getDouble("irradiancia"));
                int i=1;
                System.out.println("Tem resultado\nInstrução "+stmt);
                retorno.setDataHora(dataHora);
                System.out.println("Antes de adc");
                retorno.getCurva().add(p);
                System.out.println("Depois de adc");
                
                while(resultado.next())
                {
                    System.out.println(i++);
                    p = new ponto(resultado.getDouble("componda"),resultado.getDouble("irradiancia"));
                    retorno.getCurva().add(p);
                }
            }
            stmt.close();
        }
        catch(SQLException ex){
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            ConnectionFactory.CloseConnection(conexao);   
        }
        System.out.println("Antes retorno");
        return retorno;
    }
    
    /**
     * Método retorna espectro com curva de pontos ainda não cadastrados
    */
    public Espectro getNaoRepetidos(Espectro espec)
    {
        Espectro retorno = null;
        PreparedStatement stmt = null;
        ResultSet resultado = null;
        ponto p, pAux;
        Espectro especNaoCad = new Espectro();
        especNaoCad.setDataHora(espec.getDataHora());        
        especNaoCad.setCidade(espec.getCidade());
        Connection conexao = ConnectionFactory.getConnection();
        
        try{
            for(int i=0; i<espec.getCurva().size();i++)
            {
                pAux = (ponto) espec.getCurva().get(i);
                p = new ponto(pAux.getX(), pAux.getY());
                stmt = conexao.prepareStatement("SELECT * FROM espectro WHERE datahora_espec = ? and componda = ?;");
                
                stmt.setTimestamp(1, espec.getDataHora());
                stmt.setDouble(2, p.getX());
                
                resultado = stmt.executeQuery();
                
                //Se não há resultado de Busca, adiciona na lista
                if(!resultado.next())
                    especNaoCad.getCurva().add(p);
            }
            stmt.close();
        }
        catch(SQLException ex){
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            ConnectionFactory.CloseConnection(conexao);   
        }
        System.out.println("Antes retorno");
        return especNaoCad;
    }
}
