/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;
import java.sql.Connection;
import connection.ConnectionFactory;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.bean.Usuario;

public class UsuarioDAO {

    public void create(Usuario usu)
    {
        Connection conexao = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        try{
            stmt = conexao.prepareStatement("INSERT INTO Usuario(usuario, senha, nome) VALUES(?,?,?)");
            stmt.setString(1, usu.getUsuario());
            stmt.setString(2, usu.getSenha());
            stmt.setString(3, usu.getNome());
            stmt.executeUpdate();
            stmt.close();
        }
        catch(SQLException ex){
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            ConnectionFactory.CloseConnection(conexao, stmt);
        }
    }
    
    public Usuario select(String usu)
    {
        Connection conexao = ConnectionFactory.getConnection();
        Usuario retorno = null;
        PreparedStatement stmt = null;
        ResultSet resultado = null;
        
        try{
            stmt = conexao.prepareStatement("SELECT * FROM Usuario WHERE usuario = ?;");
            stmt.setString(1, usu);
            resultado = stmt.executeQuery();
            
            //Se há resultado de Busca
            if(resultado.next())
            {
                retorno =  new Usuario(resultado.getString("usuario"),resultado.getString("senha"),resultado.getString("nome"));
            }
            stmt.close();
        }
        catch(SQLException ex){
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            ConnectionFactory.CloseConnection(conexao, stmt);
        }
        return retorno;
    }
    public Usuario login(String user, String password)
    {
        Connection conexao = ConnectionFactory.getConnection();
        Usuario retorno = null;
        PreparedStatement stmt = null;
        ResultSet resultado = null;
        
        try{
            stmt = conexao.prepareStatement("SELECT * FROM Usuario WHERE usuario = ? and senha = ?;");
            stmt.setString(1, user);
            stmt.setString(2, password);
            resultado = stmt.executeQuery();
            //Se há resultado de Busca
            if(resultado.next())
            {
                retorno =  new Usuario(resultado.getString("usuario"),resultado.getString("senha"),resultado.getString("nome"));
            }
            stmt.close();
        }
        catch(SQLException ex){
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            ConnectionFactory.CloseConnection(conexao, stmt);
        }
        return retorno;
    }
}
