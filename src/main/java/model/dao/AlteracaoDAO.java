/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import connection.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.Pair;

/**
 *
 * @author Flávia Yumi
 */
public class AlteracaoDAO {
   
    /**
     * Busca se campo foi ou nao alterado
     */
    public Boolean buscaAlteracao(Timestamp dataHora, String campo)
    {
        Boolean retorno = false;
        Connection conexao = ConnectionFactory.getConnection();
        try{
            PreparedStatement stmt = conexao.prepareStatement("SELECT campo FROM alteracaoColeta where DataHora_col = ? AND campo = ?;");
            stmt.setTimestamp(1, dataHora);
            stmt.setString(2, campo);
            
            ResultSet resultado = stmt.executeQuery();
            if(resultado.next())
                retorno = true;
            
            stmt.close();
            
        }catch(SQLException e){
            Logger.getLogger(ColetaDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        finally{
            ConnectionFactory.CloseConnection(conexao);   
        }
        return retorno;
    }
    
    /**
     * Busca campos alterados
     */
    public List<String> buscaAlteracoes(Timestamp dataHora)
    {
        List<String> retorno = new ArrayList<String>();
        Connection conexao = ConnectionFactory.getConnection();
        try{
            PreparedStatement stmt = conexao.prepareStatement("SELECT campo FROM alteracaoColeta where DataHora_alt = ?;");
            stmt.setTimestamp(1, dataHora);
            
            ResultSet resultado = stmt.executeQuery();
                        
            while(resultado.next())
                retorno.add(resultado.getString("campo"));
            
            stmt.close();
            
        }catch(SQLException e){
            Logger.getLogger(ColetaDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        finally{
            ConnectionFactory.CloseConnection(conexao);   
        }
        return retorno;
    }
    
    /**
     * Cadastra campos alterados
     */
    public void cadAlteracoes(ArrayList<Pair<String,Object>> alteracoes, Timestamp dataHora, String cidade)
    {
        Connection conexao = ConnectionFactory.getConnection();
        try {
            
            PreparedStatement stmt = conexao.prepareStatement("INSERT INTO alteracaoColeta(DataHora_alt, campo, Cidade, ValorOriginal) VALUES (?,?,?,?);");
            
            for(int i=0; alteracoes.size()>i;i++)
            {
                stmt.setTimestamp(1, dataHora);
                stmt.setString(2, (String)alteracoes.get(i).getKey());                
                stmt.setString(3, cidade);
                stmt.setString(4, alteracoes.get(i).getValue().toString());
                stmt.addBatch();
                System.out.println(stmt);
                if(i%1000 == 0)
                {
                    stmt.executeBatch();
                    stmt.clearBatch();
                }
            }
            stmt.executeBatch();
            stmt.close();
            
        }
        catch (SQLException ex) {
            Logger.getLogger(ColetaDAO.class.getName()).log(Level.SEVERE, null, ex);
            ex.getNextException();
        }
        finally{
            ConnectionFactory.CloseConnection(conexao);   
        }
    }
    
    /**
     * Alterar a dataHora das alterações já cadastradas
     */
    public void updatePK(Timestamp dtOld, Timestamp dtNew, String ctOld, String ctNew)
    {
        Connection conexao = ConnectionFactory.getConnection();
        try
        {
            PreparedStatement stmt = conexao.prepareStatement("UPDATE alteracaoColeta SET dataHora_alt = ?, cidade = ? where dataHora_alt = ? and cidade = ?;");
            stmt.setTimestamp(1, dtNew);
            stmt.setString(2, ctNew);
            stmt.setTimestamp(3, dtOld);
            stmt.setString(4, ctOld);
            
            System.out.println(stmt);
            stmt.executeUpdate();
            stmt.close();
        }
        catch(SQLException e)
        {   
            Logger.getLogger(ColetaDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        finally{
            ConnectionFactory.CloseConnection(conexao);   
        }
    }
    
}
