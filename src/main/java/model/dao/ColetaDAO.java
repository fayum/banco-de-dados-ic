/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import model.bean.Coleta;
import common.HashMapHeaderDataBase;
import connection.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.Pair;
import javax.swing.JOptionPane;

public class ColetaDAO {
    
    /**
     * Converte o resultado do ResultSet da Query em Double
     * Trata de casos com valor nulo
     */
    private static Double getResultSetDouble(ResultSet resultset, String colName) throws SQLException {
        double v = resultset.getDouble(colName);

        if (resultset.wasNull()) {
            return 0.0;
        }
            return v;
    }
    /**
     * Cadastro de somente uma coleta
     * @param col
     */
    
    public PreparedStatement preparedStatementExistentsFields(Coleta col, HashMap<String, Integer> posicaoSQL,PreparedStatement stmt)
    {                
        try {
            
            if(posicaoSQL.containsKey("DataHora_col"))
                stmt.setTimestamp(posicaoSQL.get("DataHora_col"), col.getDataHora_col());
            if(posicaoSQL.containsKey("IrradGlobHorizontal"))
                stmt.setDouble(posicaoSQL.get("IrradGlobHorizontal"), col.getIrradGlobHorizontal());                
            if(posicaoSQL.containsKey("DesvGlobHorizontal"))
                stmt.setDouble(posicaoSQL.get("DesvGlobHorizontal"), col.getDesvGlobHorizontal());
            if(posicaoSQL.containsKey("IrradDirNormal"))
                stmt.setDouble(posicaoSQL.get("IrradDirNormal"), col.getIrradDirNormal());
            if(posicaoSQL.containsKey("DesvDirNormal"))
                stmt.setDouble(posicaoSQL.get("DesvDirNormal"), col.getIrradDirNormal());
            if(posicaoSQL.containsKey("RadiacaoDifusa"))
                stmt.setDouble(posicaoSQL.get("RadiacaoDifusa"), col.getRadiacaoDifusa());
            if(posicaoSQL.containsKey("DesvDifusa"))
                stmt.setDouble(posicaoSQL.get("DesvDifusa"), col.getRadiacaoDifusa());
            if(posicaoSQL.containsKey("TempAr"))
                stmt.setDouble(posicaoSQL.get("TempAr"), col.getTempAr());
            if(posicaoSQL.containsKey("UmidAr"))
                stmt.setDouble(posicaoSQL.get("UmidAr"), col.getUmidAr());
            if(posicaoSQL.containsKey("PressAr"))
                stmt.setDouble(posicaoSQL.get("PressAr"), col.getPressAr());
            if(posicaoSQL.containsKey("TempMod_aSi"))
                stmt.setDouble(posicaoSQL.get("TempMod_aSi"), col.getTempMod_aSi());
            if(posicaoSQL.containsKey("TempMod_micSi"))
                stmt.setDouble(posicaoSQL.get("TempMod_micSi"), col.getTempMod_micSi());
            if(posicaoSQL.containsKey("TempMod_pSi"))
                stmt.setDouble(posicaoSQL.get("TempMod_pSi"), col.getTempMod_pSi());
            if(posicaoSQL.containsKey("TempMod_moSi"))
                stmt.setDouble(posicaoSQL.get("TempMod_moSi"), col.getTempMod_moSi());
            if(posicaoSQL.containsKey("TempMod_GaAs"))
                stmt.setDouble(posicaoSQL.get("TempMod_GaAs"), col.getTempMod_GaAs());
            if(posicaoSQL.containsKey("TempMod_TJ"))
                stmt.setDouble(posicaoSQL.get("TempMod_TJ"), col.getTempMod_TJ());
            if(posicaoSQL.containsKey("DesvTempMod_aSi"))
                stmt.setDouble(posicaoSQL.get("DesvTempMod_aSi"), col.getDesvTempMod_aSi());
            if(posicaoSQL.containsKey("DesvTempMod_micSi"))
                stmt.setDouble(posicaoSQL.get("DesvTempMod_micSi"), col.getDesvTempMod_micSi());
            if(posicaoSQL.containsKey("DesvTempMod_pSi"))
                stmt.setDouble(posicaoSQL.get("DesvTempMod_pSi"), col.getDesvTempMod_pSi());
            if(posicaoSQL.containsKey("DesvTempMod_moSi"))
                stmt.setDouble(posicaoSQL.get("DesvTempMod_moSi"), col.getDesvTempMod_moSi());
            if(posicaoSQL.containsKey("DesvTempMod_GaAs"))
                stmt.setDouble(posicaoSQL.get("DesvTempMod_GaAs"), col.getDesvTempMod_GaAs());
            if(posicaoSQL.containsKey("DesvTempMod_TJ"))
                stmt.setDouble(posicaoSQL.get("DesvTempMod_TJ"), col.getDesvTempMod_TJ());
            if(posicaoSQL.containsKey("CorrCC_aSi"))
                stmt.setDouble(posicaoSQL.get("CorrCC_aSi"), col.getCorrCC_aSi());
            if(posicaoSQL.containsKey("CorrCC_micSi"))
                stmt.setDouble(posicaoSQL.get("CorrCC_micSi"), col.getCorrCC_micSi());
            if(posicaoSQL.containsKey("CorrCC_pSi"))
                stmt.setDouble(posicaoSQL.get("CorrCC_pSi"), col.getCorrCC_pSi());
            if(posicaoSQL.containsKey("CorrCC_moSi"))
                stmt.setDouble(posicaoSQL.get("CorrCC_moSi"), col.getCorrCC_moSi());
            if(posicaoSQL.containsKey("CorrCC_GaAs"))
                stmt.setDouble(posicaoSQL.get("CorrCC_GaAs"), col.getCorrCC_GaAs());
            if(posicaoSQL.containsKey("CorrCC_TJ"))
                stmt.setDouble(posicaoSQL.get("CorrCC_TJ"), col.getCorrCC_TJ());
            if(posicaoSQL.containsKey("DesvCorrCC_aSi"))
                stmt.setDouble(posicaoSQL.get("DesvCorrCC_aSi"), col.getDesvCorrCC_aSi());
            if(posicaoSQL.containsKey("DesvCorrCC_micSi"))
                stmt.setDouble(posicaoSQL.get("DesvCorrCC_micSi"), col.getDesvCorrCC_micSi());
            if(posicaoSQL.containsKey("DesvCorrCC_pSi"))
                stmt.setDouble(posicaoSQL.get("DesvCorrCC_pSi"), col.getDesvCorrCC_pSi());
            if(posicaoSQL.containsKey("DesvCorrCC_moSi"))
                stmt.setDouble(posicaoSQL.get("DesvCorrCC_moSi"), col.getDesvCorrCC_moSi());
            if(posicaoSQL.containsKey("DesvCorrCC_GaAs"))
                stmt.setDouble(posicaoSQL.get("DesvCorrCC_GaAs"), col.getDesvCorrCC_GaAs());
            if(posicaoSQL.containsKey("DesvCorrCC_TJ"))
                stmt.setDouble(posicaoSQL.get("DesvCorrCC_TJ"), col.getDesvCorrCC_TJ());
            if(posicaoSQL.containsKey("IrrPiranômetro"))
                stmt.setDouble(posicaoSQL.get("IrrPiranômetro"), col.getDesvCorrCC_pSi());
            if(posicaoSQL.containsKey("DesvIrrPiranômetro"))
                stmt.setDouble(posicaoSQL.get("DesvIrrPiranômetro"), col.getDesvCorrCC_moSi());
            if(posicaoSQL.containsKey("IrrFotodiodo"))
                stmt.setDouble(posicaoSQL.get("IrrFotodiodo"), col.getDesvCorrCC_GaAs());
            if(posicaoSQL.containsKey("DesvIrrFotodiodo"))
                stmt.setDouble(posicaoSQL.get("DesvIrrFotodiodo"), col.getDesvCorrCC_TJ());
            if(posicaoSQL.containsKey("Validacao"))
                stmt.setBoolean(posicaoSQL.get("Validacao"), col.isValidacao());
            if(posicaoSQL.containsKey("InsercaoManual"))
                stmt.setBoolean(posicaoSQL.get("InsercaoManual"), col.isInsercaoManual());
            
            stmt.setTimestamp(posicaoSQL.get("DataHora_col"), col.getDataHora_col());
            stmt.setString(posicaoSQL.get("Cidade"), col.getCidade());
                        
        } catch (SQLException ex) {
            Logger.getLogger(ColetaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return stmt;
    }
    
    public void createIndiv(Coleta col, String[] colExistente)
    {
        Connection conexao = ConnectionFactory.getConnection();
        HashMapHeaderDataBase tradutor = new HashMapHeaderDataBase();
        HashMap<String, Integer> posicaoSQL = new HashMap<>();
        
        try{            
            String insert = "INSERT INTO coleta(";
            Integer posicao =1;
            
            for(String s: colExistente)
            {
                insert = insert.concat(tradutor.getCampo(s)+", ");
                posicaoSQL.put(tradutor.getCampo(s), posicao++);
            }
            insert = insert.concat("InsercaoManual) VALUES (");
            posicaoSQL.put("InsercaoManual", posicao++);
            for(String s: colExistente)
                    insert += " ?,";
            insert = insert.concat(" ?);");
            
            PreparedStatement stmt = conexao.prepareStatement(insert);
            
            stmt = this.preparedStatementExistentsFields(col, posicaoSQL, stmt);
            
            System.out.println("Stmt: "+stmt);
            stmt.executeUpdate();
            stmt.close();
            JOptionPane.showMessageDialog(null,"Cadastro feito com sucesso!");
        }
        catch(SQLException ex){
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            ConnectionFactory.CloseConnection(conexao);   
        }
    }
    
    /**
     * Cadastro de de várias coletas
     * @param arq
     */
    public void create(List arq, List<String> colExistente)
    {
        System.out.println("Cadastro");
        Iterator<Coleta> it = arq.iterator();
        HashMapHeaderDataBase tradutor = new HashMapHeaderDataBase();
        HashMap<String, Integer> posicaoSQL = new HashMap<>();
        
        Connection conexao = ConnectionFactory.getConnection();
        
        try{
            
            String insert = "INSERT INTO coleta(";
            Integer posicao =1;
            
            for(String s: colExistente)
            {
                insert = insert.concat(tradutor.getCampo(s)+", ");
                posicaoSQL.put(tradutor.getCampo(s), posicao++);
            }
            insert = insert.concat("InsercaoManual) VALUES (");
            posicaoSQL.put("InsercaoManual", posicao++);
            for(String s: colExistente)
                    insert += " ?,";
            insert = insert.concat(" ?);");
            
            PreparedStatement stmt = conexao.prepareStatement(insert);
             System.out.println(stmt);
            Coleta col;
            int i=0;
            while(it.hasNext())
            {
                col = it.next();
                
                stmt = this.preparedStatementExistentsFields(col, posicaoSQL, stmt);
                stmt.addBatch();
                i++;
                
                if(i%1000 == 0)
                {
                    stmt.executeBatch();
                    stmt.clearBatch();
                }
                
            }
            stmt.executeBatch();
            stmt.close();
        }
        catch(SQLException ex){
            ex.getNextException().printStackTrace();
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            ConnectionFactory.CloseConnection(conexao);   
        }
        
    }
    
    /**
     * Cadastro de arquivos gerados pelo BD 
    */
    public void createBD(List arq)
    {
        Iterator<Coleta> it = arq.iterator();
        Connection conexao = ConnectionFactory.getConnection();
        
        try{
            String insert = "INSERT INTO coleta("
                    +"DataHora_col," +
                    "IrradGlobHorizontal,DesvGlobHorizontal," +
                    "IrradDirNormal,DesvDirNormal," +
                    "RadiacaoDifusa,DesvDifusa," +
                    "TempAr,UmidAr,PressAr," +
                    
                    "CorrCC_aSi, DesvCorrCC_aSi," +
                    "CorrCC_micSi, DesvCorrCC_micSi," +
                    "CorrCC_pSi, DesvCorrCC_pSi," +
                    "CorrCC_moSi, DesvCorrCC_moSi," +
                    "CorrCC_GaAs, DesvCorrCC_GaAs," +
                    "CorrCC_TJ, DesvCorrCC_TJ," +
                    
                    "TempMod_aSi, DesvTempMod_aSi," +
                    "TempMod_micSi, DesvTempMod_micSi," +
                    "TempMod_pSi, DesvTempMod_pSi," +
                    "TempMod_moSi, DesvTempMod_moSi," +
                    "TempMod_GaAs, DesvTempMod_GaAs," +
                    "TempMod_TJ, DesvTempMod_TJ," +
                    
                    "IrrPiranômetro, DesvIrrPiranômetro," +
                    "IrrFotodiodo,DesvIrrFotodiodo,Validacao,"
                    + "InsercaoManual, Cidade"
                    + ") VALUES (?,?,?,?,?,"
                    + "?,?,?,?,?,"
                    + "?,?,?,?,?,"
                    + "?,?,?,?,?,"
                    + "?,?,?,?,?,"
                    + "?,?,?,?,?,"
                    + "?,?,?,?,?,"
                    + "?,?,?,?, ?, ?);";
            
            PreparedStatement stmt = conexao.prepareStatement(insert);
            
            Coleta col;
            int i=0;
            while(it.hasNext())
            {
                col = it.next();
                
                stmt.setTimestamp(1, col.getDataHora_col());
                stmt.setDouble(2, col.getIrradGlobHorizontal());
                stmt.setDouble(3, col.getDesvGlobHorizontal());
                stmt.setDouble(4, col.getIrradDirNormal());
                stmt.setDouble(5, col.getDesvDirNormal());

                stmt.setDouble(6, col.getRadiacaoDifusa());
                stmt.setDouble(7, col.getDesvDifusa());
                stmt.setDouble(8, col.getTempAr());
                stmt.setDouble(9, col.getUmidAr());
                stmt.setDouble(10, col.getPressAr());
                
                stmt.setDouble(11, col.getCorrCC_aSi());
                stmt.setDouble(12, col.getDesvCorrCC_aSi());
                stmt.setDouble(13, col.getCorrCC_micSi());
                stmt.setDouble(14, col.getDesvCorrCC_micSi());                
                stmt.setDouble(15, col.getCorrCC_pSi());
                stmt.setDouble(16, col.getDesvCorrCC_pSi());                
                stmt.setDouble(17, col.getCorrCC_moSi());                
                stmt.setDouble(18, col.getDesvCorrCC_moSi());
                stmt.setDouble(19, col.getCorrCC_GaAs());
                stmt.setDouble(20, col.getDesvCorrCC_GaAs());
                stmt.setDouble(21, col.getCorrCC_TJ());
                stmt.setDouble(22, col.getDesvCorrCC_TJ());
                
                stmt.setDouble(23, col.getTempMod_aSi());
                stmt.setDouble(29, col.getDesvTempMod_aSi());
                stmt.setDouble(24, col.getTempMod_micSi());
                stmt.setDouble(30, col.getDesvTempMod_micSi());
                stmt.setDouble(25, col.getTempMod_pSi());
                stmt.setDouble(31, col.getDesvTempMod_pSi());
                stmt.setDouble(26, col.getTempMod_moSi());
                stmt.setDouble(32, col.getDesvTempMod_moSi());                
                stmt.setDouble(27, col.getTempMod_GaAs());
                stmt.setDouble(33, col.getDesvTempMod_GaAs());
                stmt.setDouble(28, col.getTempMod_TJ());
                stmt.setDouble(34, col.getDesvTempMod_TJ());

                stmt.setDouble(35, col.getIrrPiranômetro());
                stmt.setDouble(36, col.getDesvIrrPiranômetro());
                stmt.setDouble(37, col.getIrrFotodiodo());
                stmt.setDouble(38, col.getDesvIrrFotodiodo());
                stmt.setBoolean(39, col.isValidacao());
                stmt.setBoolean(40, col.isInsercaoManual());
                stmt.setString(41, col.getCidade());
                
                stmt.addBatch();
                i++;
                
                if(i%1000 == 0)
                {
                    stmt.executeBatch();
                    stmt.clearBatch();
                }
                
            }
            stmt.executeBatch();
            stmt.close();
        }
        catch(SQLException ex){
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            ConnectionFactory.CloseConnection(conexao);   
        }
        
    }
    
    /**
     * Completar cadastro da coleta
    */
    public void update(List<Coleta> coletas, List<String> camposExistentes)
    {
        Connection conexao = ConnectionFactory.getConnection();
        PreparedStatement stmt;
        HashMapHeaderDataBase tradutor = new HashMapHeaderDataBase();
        HashMap<String,Integer> posicaoSQL = new HashMap<>();
        Integer posicao=1;
        Coleta col;
        
        String sql = "UPDATE coleta SET";
        for(String s:camposExistentes)
        {
            if(s.compareTo("TIMESTAMP")!=0)
            {
                sql = sql.concat(" "+tradutor.getCampo(s)+" = ?,");
                posicaoSQL.put(tradutor.getCampo(s), posicao++);
            }
        }
        sql = sql.substring(0, sql.length()-1).concat("WHERE DataHora_col = ? AND Cidade = ? ;");
        posicaoSQL.put(tradutor.getCampo("TIMESTAMP"), posicao++);
        posicaoSQL.put("Cidade", posicao++);
        
        try {
            
            stmt = conexao.prepareStatement(sql);                
            Iterator<Coleta> it = coletas.iterator();
            int i=0;
            while(it.hasNext())
            {
                col = it.next();
                
                stmt = this.preparedStatementExistentsFields(col, posicaoSQL, stmt);
                
                stmt.addBatch();
                i++;
                
                if(i%1000 == 0)
                {
                    stmt.executeBatch();
                    stmt.clearBatch();
                }
                
            }
            System.out.println("Stmt: "+ stmt);
            stmt.executeBatch();
            stmt.close();
        }catch (SQLException ex1) {
            Logger.getLogger(ColetaDAO.class.getName()).log(Level.SEVERE, null, ex1);
        }
        finally
        {
            ConnectionFactory.CloseConnection(conexao);
        }
    }
    
    /**
     * Busca por dataHora ou só data
     * intervalo == true  -> Data e Hora
     * intervalo == false -> Só Data 
     * @param dtInicio
     * @param dtFim
     * @param intervalo
     * @return 
     */
    public List<Coleta> selectDtHr(Timestamp dtInicio, Timestamp dtFim, boolean intervalo)
    {
        Connection conexao = ConnectionFactory.getConnection();
        try {
            PreparedStatement stmt;
            ResultSet resultado;
            
            if(dtInicio != null)
            {
                if(intervalo == false)
                {
                    stmt = conexao.prepareStatement("SELECT * FROM COLETA WHERE DataHora_col = ?;");
                    stmt.setTimestamp(1, dtInicio);
                }
                else
                {
                    stmt = conexao.prepareStatement("SELECT * FROM COLETA WHERE DataHora_col >= ? and DataHora_col <= ? ORDER BY DataHora_Col;");
                    stmt.setTimestamp(1, dtInicio);
                    stmt.setTimestamp(2, dtFim);
                }
            }
            else
            {
                stmt = conexao.prepareStatement("SELECT * FROM COLETA ORDER BY DataHora_Col;");
            }
            resultado = stmt.executeQuery();
            List<Coleta> retornar = resultSetToListColeta(resultado);
            stmt.close();
            return retornar;
            
        } catch (SQLException ex) {
            Logger.getLogger(ColetaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }        
        finally{
            ConnectionFactory.CloseConnection(conexao);   
        }
        return null;        
    }
    
    /**
     * Busca por dataHora e Cidade
     */
    public List<Coleta> selectPK(Timestamp dtHr, String Cidade)
    {
        Connection conexao = ConnectionFactory.getConnection();
        try {
            PreparedStatement stmt = null;
            ResultSet resultado;
            
            if(dtHr == null || Cidade.compareTo("")==0 || Cidade==null)
            {
                System.out.println("Parametro(s) não fornecido de maneira correta");
                return null;
            }
            
            stmt = conexao.prepareStatement("SELECT * FROM COLETA WHERE DataHora_col = ? and Cidade = ?;");
            stmt.setTimestamp(1, dtHr);
            stmt.setString(2, Cidade);

            System.out.println(stmt);
            resultado = stmt.executeQuery();
            List<Coleta> retornar = resultSetToListColeta(resultado);
            stmt.close();
            return retornar;
            
        } catch (SQLException ex) {
            Logger.getLogger(ColetaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }        
        finally{
            ConnectionFactory.CloseConnection(conexao);   
        }
        return null;        
    }
    
    /**
     * Datas devem estar previamente convertidas se forem ser utilizadas
     */
    public List<Coleta> selectValidacao(boolean valor, int modoBusca, Timestamp dtInicio, Timestamp dtFim)
    {
        Connection conexao = ConnectionFactory.getConnection();
        try {
            
            PreparedStatement stmt;
            ResultSet resultado;
            
            switch(modoBusca)
            {
                case 0:
                    stmt = conexao.prepareStatement("SELECT * FROM COLETA WHERE Validacao = ? ORDER BY DataHora_Col, Cidade;");
                    stmt.setBoolean(1, valor);
                    break;
                case 2:
                    stmt = conexao.prepareStatement("SELECT * FROM COLETA WHERE Validacao = ? and DataHora_col = ? ORDER BY Cidade;;");
                    stmt.setBoolean(1, valor);
                    stmt.setTimestamp(2, dtInicio);
                    break;
                case 1: case 5: case 6: case 7: case 8:
                    stmt = conexao.prepareStatement("SELECT * FROM COLETA WHERE Validacao = ? and DataHora_col>= ? and DataHora_col<= ?  ORDER BY DataHora_Col, Cidade;;");
                    stmt.setBoolean(1, valor);
                    stmt.setTimestamp(2, dtInicio);
                    stmt.setTimestamp(3, dtFim);
                    break;
                default:
                    stmt = conexao.prepareStatement("");
                    break;
            }
            resultado = stmt.executeQuery();
            List<Coleta> retornar = resultSetToListColeta(resultado);
            stmt.close();
            
            return retornar;
            
        } catch (SQLException ex) {
            Logger.getLogger(ColetaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            ConnectionFactory.CloseConnection(conexao);   
        }
        return null;        
    }
    
    /**
     * Datas devem estar previamente convertidas
     */
    public List<Coleta> selectCampo(String campo, String comparador, Double valor, int modoBusca, Timestamp dtInicio, Timestamp dtFim)
    {
        Connection conexao = ConnectionFactory.getConnection();
        try {
            PreparedStatement stmt;
            ResultSet resultado;
            
            switch(modoBusca)
            {
                case 0:
                    stmt = conexao.prepareStatement("SELECT * FROM COLETA WHERE "+campo+" "+comparador+" ? ORDER BY DataHora_Col, Cidade;");
                    stmt.setDouble(1, valor);
                    break;
                case 2:
                    stmt = conexao.prepareStatement("SELECT * FROM COLETA WHERE "+campo+" "+comparador+" ? ORDER BY DataHora_Col, Cidade;");
                    stmt.setDouble(1, valor);
                    stmt.setTimestamp(2, dtInicio);
                    break;
                case 1: case 5: case 6: case 7: case 8:
                    stmt = conexao.prepareStatement("SELECT * FROM COLETA WHERE "+campo+" "+comparador+" ? and DataHora_col>= ? and DataHora_col<= ?  ORDER BY DataHora_Col, Cidade;");
                    stmt.setDouble(1,valor);
                    stmt.setTimestamp(2, dtInicio);
                    stmt.setTimestamp(3, dtFim);
                    break;
                default:
                    stmt = conexao.prepareStatement("");
                    break;
            }
            
            resultado = stmt.executeQuery();
            List<Coleta> retornar = resultSetToListColeta(resultado);
            stmt.close();
            
            return retornar;
            
        } catch (SQLException ex) {
            Logger.getLogger(ColetaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        finally{
            ConnectionFactory.CloseConnection(conexao);   
        }
        return null;        
    }
    
    public List<Coleta> selectCidade(String cidade, int modoBusca, Timestamp dtInicio, Timestamp dtFim)
    {
        Connection conexao = ConnectionFactory.getConnection();
        try {
            PreparedStatement stmt;
            ResultSet resultado;
            
            switch(modoBusca)
            {
                case 0:
                    stmt = conexao.prepareStatement("SELECT * FROM COLETA WHERE Cidade = ? ORDER BY DataHora_Col, Cidade;");
                    stmt.setString(1, cidade);
                    break;
                case 2:
                    stmt = conexao.prepareStatement("SELECT * FROM COLETA WHERE Cidade = ? and DataHora_col = ?, Cidade;");
                    stmt.setString(1, cidade);
                    stmt.setTimestamp(2, dtInicio);
                    break;
                case 1: case 5: case 6: case 7: case 8:
                    stmt = conexao.prepareStatement("SELECT * FROM COLETA WHERE Cidade = ? and DataHora_col>= ? and DataHora_col<= ? ORDER BY DataHora_Col, Cidade;");
                    stmt.setString(1, cidade);
                    stmt.setTimestamp(2, dtInicio);
                    stmt.setTimestamp(3, dtFim);
                    break;
                default:
                    stmt = conexao.prepareStatement("");
                    break;
            }
            
            resultado = stmt.executeQuery();
            List<Coleta> retornar = resultSetToListColeta(resultado);
            stmt.close();
            
            return retornar;
            
        } catch (SQLException ex) {
            Logger.getLogger(ColetaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        finally{
            ConnectionFactory.CloseConnection(conexao);   
        }
        return null; 
    }
    
    /**
     * O ResultSet deve ter o resultado de um select de coletas com todos os campos
     * O método retorna uma lista dessas coletas
    */
    private List<Coleta> resultSetToListColeta(ResultSet resultado)
    {
        List<Coleta> coletas = new ArrayList<>();
        Coleta col;
        try {
            while(resultado.next())
            {
                col = new Coleta();
                col.setDataHora_col(resultado.getTimestamp("DataHora_col"));
                col.setIrradGlobHorizontal(getResultSetDouble(resultado,"IrradGlobHorizontal"));
                col.setDesvGlobHorizontal(getResultSetDouble(resultado,"DesvGlobHorizontal"));
                col.setIrradDirNormal(getResultSetDouble(resultado,"IrradDirNormal"));
                col.setDesvDirNormal(getResultSetDouble(resultado,"DesvDirNormal"));
                col.setRadiacaoDifusa(getResultSetDouble(resultado,"RadiacaoDifusa"));
                col.setDesvDifusa(getResultSetDouble(resultado,"DesvDifusa"));
                col.setTempAr(getResultSetDouble(resultado,"TempAr"));
                col.setUmidAr(getResultSetDouble(resultado,"UmidAr"));
                col.setPressAr(getResultSetDouble(resultado,"PressAr"));
                col.setCorrCC_aSi(getResultSetDouble(resultado,"CorrCC_aSi"));
                col.setCorrCC_micSi(getResultSetDouble(resultado,"CorrCC_micSi"));
                col.setCorrCC_pSi(getResultSetDouble(resultado,"CorrCC_pSi"));
                col.setCorrCC_moSi(getResultSetDouble(resultado,"CorrCC_moSi"));
                col.setCorrCC_GaAs(getResultSetDouble(resultado,"CorrCC_GaAs"));
                col.setCorrCC_TJ(getResultSetDouble(resultado,"CorrCC_TJ"));
                col.setDesvCorrCC_aSi(getResultSetDouble(resultado,"DesvCorrCC_aSi"));
                col.setDesvCorrCC_micSi(getResultSetDouble(resultado,"DesvCorrCC_micSi"));
                col.setDesvCorrCC_pSi(getResultSetDouble(resultado,"DesvCorrCC_pSi"));
                col.setDesvCorrCC_moSi(getResultSetDouble(resultado,"DesvCorrCC_moSi"));
                col.setDesvCorrCC_GaAs(getResultSetDouble(resultado,"DesvCorrCC_GaAs"));
                col.setDesvCorrCC_TJ(getResultSetDouble(resultado,"DesvCorrCC_TJ"));
                col.setTempMod_aSi(getResultSetDouble(resultado,"TempMod_aSi"));
                col.setTempMod_micSi(getResultSetDouble(resultado,"TempMod_micSi"));
                col.setTempMod_pSi(getResultSetDouble(resultado,"TempMod_pSi"));
                col.setTempMod_moSi(getResultSetDouble(resultado,"TempMod_moSi"));
                col.setTempMod_GaAs(getResultSetDouble(resultado,"TempMod_GaAs"));
                col.setTempMod_TJ(getResultSetDouble(resultado,"TempMod_TJ"));
                col.setDesvTempMod_aSi(getResultSetDouble(resultado,"DesvTempMod_aSi"));
                col.setDesvTempMod_micSi(getResultSetDouble(resultado,"DesvTempMod_micSi"));
                col.setDesvTempMod_pSi(getResultSetDouble(resultado,"DesvTempMod_pSi"));
                col.setDesvTempMod_moSi(getResultSetDouble(resultado,"DesvTempMod_moSi"));
                col.setDesvTempMod_GaAs(getResultSetDouble(resultado,"DesvTempMod_GaAs"));
                col.setDesvTempMod_TJ(getResultSetDouble(resultado,"DesvTempMod_TJ"));
                col.setIrrPiranômetro(getResultSetDouble(resultado,"IrrPiranômetro"));
                col.setDesvIrrPiranômetro(getResultSetDouble(resultado,"DesvIrrPiranômetro"));
                col.setIrrFotodiodo(getResultSetDouble(resultado,"IrrFotodiodo"));
                col.setDesvIrrFotodiodo(getResultSetDouble(resultado,"DesvIrrFotodiodo"));
                col.setValidacao(Boolean.parseBoolean(resultado.getString("Validacao")));
                col.setCidade(resultado.getString("Cidade"));
                coletas.add(col);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ColetaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return coletas;
    }
    /**
     * Busca por dataHora
     * Para verificação antes de cadastro do arq do projeto SONDA
     * Retorna Lista de Coletas ainda nao cadastradas
     */
    public List<List<Coleta>> buscaRepetido(List<Coleta> lista, List<String> CampoExistente)
    {
        System.out.println("Busca de repetidos");
        Connection conexao = ConnectionFactory.getConnection();
        PreparedStatement stmt;
        ResultSet resultado;
        List<Coleta> ColetasNaoCadastradas = new ArrayList<>();
        List<Coleta> ColetasParaUpdate = new ArrayList<>();
        HashMapHeaderDataBase tradutor = new HashMapHeaderDataBase();
        
        String sql = "SELECT *";
        if(CampoExistente.isEmpty()==false)
        {
            /*---------------*/
            if(CampoExistente.isEmpty())
                System.out.println("Não há campos existentes");
            else
            {
                System.out.println("Campos: ");
                for(String s:CampoExistente)
                    if(s.compareTo("TIMESTAMP")!=0 && s.compareTo("Cidade")!=0)
                        System.out.println(" "+tradutor.getCampo(s)+" is null and");
            }
            /*---------------*/
            sql = sql.concat(", CASE WHEN");
            for(String s:CampoExistente)
            {    
                if(s.compareTo("TIMESTAMP")!=0 && s.compareTo("Cidade")!=0)
                {   
                    sql = sql.concat(" "+tradutor.getCampo(s)+" is null and");
                }
            }
            sql = sql.substring(0, sql.length()-3);
            sql = sql.concat("THEN 'true' ELSE 'false' END AS Update FROM coleta WHERE DataHora_col = ? and Cidade = ?;");
            System.out.println(sql);
        }
        
        try {
            stmt = conexao.prepareStatement(sql);
            for(int i=0; lista.size()>i;i++)
            {
                stmt.setTimestamp(1, lista.get(i).getDataHora_col());
                stmt.setString(2, lista.get(i).getCidade());

                resultado = stmt.executeQuery();
                if(!resultado.next())
                {
                    //Não está cadastrado => pode ser cadastrado
                    ColetasNaoCadastradas.add(lista.get(i));
                }
                
                else
                {
                    //Verifico se é update        
                    if("true".compareTo(resultado.getString("Update"))==0)
                    {
                        ColetasParaUpdate.add(lista.get(i));
                    }
                }
            }    
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(ColetaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            ConnectionFactory.CloseConnection(conexao);
        }
        System.out.println("ParaCadastro "+ColetasNaoCadastradas.size()+"\nParaUpdate "+ColetasParaUpdate.size());
        
        List<List<Coleta>> retorno = new ArrayList<>(2);
        retorno.add(ColetasNaoCadastradas);
        retorno.add(ColetasParaUpdate);
        
        return retorno;
    }
    
    /**
     * Deleta por dataHora
     * Parametro: lista de timestap
     */
    public void deleta(List<Pair<Timestamp, String>> lista)
    {
        Connection conexao = ConnectionFactory.getConnection();
        try {
            PreparedStatement stmtCol = conexao.prepareStatement("DELETE FROM COLETA WHERE DataHora_col = ? AND Cidade = ?;");
            PreparedStatement stmtAlt = conexao.prepareStatement("DELETE FROM COLETA WHERE DataHora_alt = ? AND Cidade = ?;");
            PreparedStatement stmtEspec = conexao.prepareStatement("DELETE FROM Espectro WHERE DataHora_espec = ? AND Cidade = ?;");
            
            for(int i=0; lista.size()>i;i++)
            {
                stmtCol.setTimestamp(1, lista.get(i).getKey());
                stmtCol.setString(2, lista.get(i).getValue());
                stmtCol.addBatch();

                stmtAlt.setTimestamp(1, lista.get(i).getKey());
                stmtAlt.setString(2, lista.get(i).getValue());
                stmtAlt.addBatch();
                
                stmtEspec.setTimestamp(1, lista.get(i).getKey());
                stmtEspec.setString(2, lista.get(i).getValue());
                stmtEspec.addBatch();
                
                if(i%1000 == 0)
                {                    
                    stmtAlt.executeBatch();
                    stmtAlt.clearBatch();
                    
                    stmtEspec.executeBatch();
                    stmtEspec.clearBatch();
                    
                    stmtCol.executeBatch();
                    stmtCol.clearBatch();
                }
                
            }
            
            stmtAlt.executeBatch();
            stmtAlt.close();
            
            stmtEspec.executeBatch();
            stmtEspec.close();
            
            stmtCol.executeBatch();
            stmtCol.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(ColetaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }        
        finally
        {            
            ConnectionFactory.CloseConnection(conexao);
        }
    }
    
    /**
     * Quando altera especificamente os campos 
     * @param alteracoes
     * @param valores
     * @param val
     * @param validacao
     * @param data
     * @param dataOriginal
     * @param cidade
     * @param cidadeOriginal
     */
    public void alterarColeta(List<Pair<String,Object>> alteracoes, List<Object> novosValores,Timestamp dt, String ct)
    {        
        Connection conexao = ConnectionFactory.getConnection();
        try{            
            //Monta a instrução
            String sql = "UPDATE coleta set";
            int i = 0;
            for(Pair par:alteracoes)
            {   
                sql+= " "+par.getKey()+" = ?";
                i++;
                if(alteracoes.size()>i)
                    sql += ",";
            }
            sql += " WHERE DataHora_col = '"+ dt +"' AND Cidade = '"+ ct +"';";
            
            PreparedStatement stmt = conexao.prepareStatement(sql);
            i=1;
            System.out.println("Qtd de itens em novosValores: "+ novosValores.size());
            for(Object b: novosValores)
            {
                if(b instanceof Double)
                {
                    stmt.setDouble(i, (Double)b);
                }
                else if(b instanceof Timestamp)
                {
                    stmt.setTimestamp(i, (Timestamp)b);
                }
                else if(b instanceof String)
                {
                    stmt.setString(i, (String)b);
                }else if(b instanceof Boolean)
                {
                    stmt.setBoolean(i, (Boolean)b);
                }
                else
                    System.out.println("Não encontrou o tipo de " + b);
                i++;
            }
            System.out.println(stmt);
            stmt.executeUpdate();
            stmt.close();
            
        }
        catch(SQLException e)
        {   
            Logger.getLogger(ColetaDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        finally
        {            
            ConnectionFactory.CloseConnection(conexao);
        }
    }
    
    public boolean colJaCad(Coleta col)
    {
        Connection conexao = ConnectionFactory.getConnection();
        try{            
            //Monta a instrução
            String sql = "SELECT * FROM coleta WHERE DataHora_col = ? AND Cidade = ?;";
            PreparedStatement stmt = conexao.prepareStatement(sql);
            
            //Determina valores
            stmt.setTimestamp(1, col.getDataHora_col());
            stmt.setString(2, col.getCidade());
            
            ResultSet resultado = stmt.executeQuery();
            if(resultado.next())
            {
                conexao.close();
                stmt.close();
                return true;
            }
            stmt.close();
            
        }
        catch(SQLException e)
        {   
            Logger.getLogger(ColetaDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        finally
        {            
            ConnectionFactory.CloseConnection(conexao);
        }
        return false;
    }
}
