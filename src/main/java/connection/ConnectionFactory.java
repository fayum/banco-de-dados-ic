/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectionFactory {
    
    private static final String DRIVE = "org.postgresql.Driver";
    private static final String URL = "jdbc:postgresql://localhost:5432/dadosatmosfericos";
    private static final String USER = "postgres";
    private static final String PASS = "userflavia";
    
    public static Connection getConnection()
    {
        try {
            Class.forName(DRIVE);
            return DriverManager.getConnection(URL, USER, PASS);
        } catch (ClassNotFoundException | SQLException ex) {
            throw new RuntimeException("Erro na conexão: ", ex);
        }
    }
    
    public static void CloseConnection(Connection conexao)
    {
        try{
            if(conexao != null)
            {
                   conexao.close();
            }
        }
        catch(SQLException ex){
            Logger.getLogger(ConnectionFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void CloseConnection (Connection conexao, PreparedStatement sttmnt)
    {
        CloseConnection(conexao);
        try{
            if(sttmnt != null)
            {
                sttmnt.close();
            }
        }
        catch(SQLException ex){
            Logger.getLogger(ConnectionFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        public static void CloseConnection (Connection conexao, PreparedStatement sttmnt, ResultSet ra)
    {
        CloseConnection(conexao);
        try{
            if(ra != null)
            {
                ra.close();
            }
        }
        catch(SQLException ex){
            Logger.getLogger(ConnectionFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
}
